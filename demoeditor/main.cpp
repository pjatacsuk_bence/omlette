#include "DXUT.h"
//#include "EntityFactory.h"
#include <Omlette\Framework\Utility\ResourceManager.h>
#include "d3dx11effect.h"
#include <Omlette\Framework\App\App.h>

Oml::App* app = NULL;
	extern "C" void __declspec(dllexport)  init();
	extern "C" void __declspec(dllexport)  setHWND(HWND hWnd);
	extern "C" void __declspec(dllexport)  createDevice();
	extern "C" void __declspec(dllexport)  run();
	extern "C" void __declspec(dllexport)  update();
	extern "C" void __declspec(dllexport)  render();
	extern "C" void __declspec(dllexport)  proc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);



//--------------------------------------------------------------------------------------
// Reject any D3D11 devices that aren't acceptable by returning false
//--------------------------------------------------------------------------------------
bool CALLBACK IsD3D11DeviceAcceptable( const CD3D11EnumAdapterInfo *AdapterInfo, UINT Output, const CD3D11EnumDeviceInfo *DeviceInfo,
                                       DXGI_FORMAT BackBufferFormat, bool bWindowed, void* pUserContext )
{
    return true;
}


//--------------------------------------------------------------------------------------
// Called right before creating a D3D9 or D3D11 device, allowing the app to modify the device settings as needed
//--------------------------------------------------------------------------------------
bool CALLBACK ModifyDeviceSettings( DXUTDeviceSettings* pDeviceSettings, void* pUserContext )
{
	pDeviceSettings->d3d11.CreateFlags |= D3D10_CREATE_DEVICE_BGRA_SUPPORT;
    return true;
}


//--------------------------------------------------------------------------------------
// Create any	D3D11 resources that aren't dependant on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11CreateDevice( ID3D11Device* pd3dDevice, const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc,
                                      void* pUserContext )
{
	app = new Oml::App(pd3dDevice);
	return app->createResources(pBackBufferSurfaceDesc);
    return S_OK;
}


//--------------------------------------------------------------------------------------
// Create any D3D11 resources that depend on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11ResizedSwapChain( ID3D11Device* pd3dDevice, IDXGISwapChain* pSwapChain,
                                          const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext )
{
	app->setSwapChain(pSwapChain, pBackBufferSurfaceDesc);
	app->createSwapChainResources();
    return S_OK;
}


//--------------------------------------------------------------------------------------
// Handle updates to the scene.  This is called regardless of which D3D API is used
//--------------------------------------------------------------------------------------
void CALLBACK OnFrameMove( double fTime, float fElapsedTime, void* pUserContext )
{
	if(app)
		app->animate(fElapsedTime, fTime);
}

void update()
{
	if (app)
		app->animate(DXUTGetElapsedTime(), DXUTGetTime());
}
void proc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if(app)
		app->processMessage(hWnd, uMsg, wParam, lParam);
}
void render()
{
	
	ID3D11Device* device = DXUTGetD3D11Device();
	ID3D11DeviceContext* context;
	device->GetImmediateContext(&context);
	if (app)
		app->render(context);
}
//--------------------------------------------------------------------------------------
// Render the scene using the D3D11 device
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11FrameRender( ID3D11Device* pd3dDevice, ID3D11DeviceContext* pd3dImmediateContext,
                                  double fTime, float fElapsedTime, void* pUserContext )
{
	if(app)
		app->render(pd3dImmediateContext);
}


//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11ResizedSwapChain 
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11ReleasingSwapChain( void* pUserContext )
{
	app->releaseSwapChainResources();
}


//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11CreateDevice 
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11DestroyDevice( void* pUserContext )
{
	app->releaseResources();
	delete app;
	app = NULL;
}


//--------------------------------------------------------------------------------------
// Handle messages to the application
//--------------------------------------------------------------------------------------
LRESULT CALLBACK MsgProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam,
                          bool* pbNoFurtherProcessing, void* pUserContext )
{
	if(app)
		app->processMessage(hWnd, uMsg, wParam, lParam);
    return 0;
}


//--------------------------------------------------------------------------------------
// Handle key presses
//--------------------------------------------------------------------------------------
void CALLBACK OnKeyboard( UINT nChar, bool bKeyDown, bool bAltDown, void* pUserContext )
{
}


//--------------------------------------------------------------------------------------
// Handle mouse button presses
//--------------------------------------------------------------------------------------
void CALLBACK OnMouse( bool bLeftButtonDown, bool bRightButtonDown, bool bMiddleButtonDown,
                       bool bSideButton1Down, bool bSideButton2Down, int nMouseWheelDelta,
                       int xPos, int yPos, void* pUserContext )
{
}


//--------------------------------------------------------------------------------------
// Call if device was removed.  Return true to find a new device, false to quit
//--------------------------------------------------------------------------------------
bool CALLBACK OnDeviceRemoved( void* pUserContext )
{
    return true;
}

void init()
{
    DXUTSetCallbackFrameMove( OnFrameMove );
    DXUTSetCallbackKeyboard( OnKeyboard );
    DXUTSetCallbackMouse( OnMouse );
    DXUTSetCallbackMsgProc( MsgProc );
    DXUTSetCallbackDeviceChanging( ModifyDeviceSettings );
    DXUTSetCallbackDeviceRemoved( OnDeviceRemoved );

    // Set the D3D11 DXUT callbacks. Remove these sets if the app doesn't need to support D3D11
    DXUTSetCallbackD3D11DeviceAcceptable( IsD3D11DeviceAcceptable );
    DXUTSetCallbackD3D11DeviceCreated( OnD3D11CreateDevice );
    DXUTSetCallbackD3D11SwapChainResized( OnD3D11ResizedSwapChain );
    DXUTSetCallbackD3D11FrameRender( OnD3D11FrameRender );
    DXUTSetCallbackD3D11SwapChainReleasing( OnD3D11ReleasingSwapChain );
    DXUTSetCallbackD3D11DeviceDestroyed( OnD3D11DestroyDevice );

    // Perform any application-level initialization here

    DXUTInit( true, true, NULL ); // Parse the command line, show msgboxes on error, no extra command line params
    DXUTSetCursorSettings( true, true ); // Show the cursor and clip it when in full screen

}

void setHWND(HWND hWnd)
{
	DXUTSetWindow(DXUTGetHWND(), DXUTGetHWND(), DXUTGetHWND(), true);
}

void createDevice()
{
    DXUTCreateDevice( D3D_FEATURE_LEVEL_10_0, true, 1440,800 );
	ID3D11Device* device = DXUTGetD3D11Device();
	app = new Oml::App(device);
	const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc = DXUTGetDXGIBackBufferSurfaceDesc();
	 app->createResources(pBackBufferSurfaceDesc);
	MessageBox(NULL, (LPCWSTR)"INIT", (LPCWSTR) "CREATED", MB_ICONWARNING);
}
void run()
{
    DXUTMainLoop(); // Enter into the DXUT ren  der loop
}

//---------------------------------k-----------------------------------------------------
// Initialize everything and go into a render loop
//--------------------------------------------------------------------------------------
int WINAPI wWinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow )
{
    // Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
    _CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif
	

    // DXUT will create and use the best device (either D3D9 or D3D11) 
    // that is available on the system depending on which D3D callbacks are set below

    // Set general DXUT callbacks
    DXUTSetCallbackFrameMove( OnFrameMove );
    DXUTSetCallbackKeyboard( OnKeyboard );
    DXUTSetCallbackMouse( OnMouse );
    DXUTSetCallbackMsgProc( MsgProc );
    DXUTSetCallbackDeviceChanging( ModifyDeviceSettings );
    DXUTSetCallbackDeviceRemoved( OnDeviceRemoved );

    // Set the D3D11 DXUT callbacks. Remove these sets if the app doesn't need to support D3D11
    DXUTSetCallbackD3D11DeviceAcceptable( IsD3D11DeviceAcceptable );
    DXUTSetCallbackD3D11DeviceCreated( OnD3D11CreateDevice );
    DXUTSetCallbackD3D11SwapChainResized( OnD3D11ResizedSwapChain );
    DXUTSetCallbackD3D11FrameRender( OnD3D11FrameRender );
    DXUTSetCallbackD3D11SwapChainReleasing( OnD3D11ReleasingSwapChain );
    DXUTSetCallbackD3D11DeviceDestroyed( OnD3D11DestroyDevice );

    // Perform any application-level initialization here

    DXUTInit( true, true, NULL ); // Parse the command line, show msgboxes on error, no extra command line params
    DXUTSetCursorSettings( true, true ); // Show the cursor and clip it when in full screen
    DXUTCreateWindow( L"Egg" );
	DXUTSetWindow(DXUTGetHWND(), DXUTGetHWND(), DXUTGetHWND(), true);
    // Only require 10-level hardware
	
    DXUTCreateDevice( D3D_FEATURE_LEVEL_10_0, true, 1440,800 );

    DXUTMainLoop(); // Enter into the DXUT ren  der loop

    // Perform any application-level cleanup here

    return DXUTGetExitCode();
}




#ifdef TESTING_OMLETTE_SERIALIZATION
int main(int argc,char** argv)
{
	using namespace Oml;

	EntityManager entityManager;
	entityManager.deserialize("output_component_prop_segment_input.xml");

	Entity::P	hello = entityManager.getEntity("Hello");
	Component::P helloPos = hello->getComponentManager()->getComponent("transform");
	Component::PropertyMap	pMap = helloPos->getPropertyMap();
	IPropertyAncestor::P	posProp = pMap["position"];
	Property<Egg::Math::float4>::P posPropFloat4 = Caster::getInstance().getPropertyCasted<Property<Egg::Math::float4>::P>(posProp);

	SegmentManager<Egg::Math::float4>::P sm =  posPropFloat4->getSegmentManager();
	for(float i=0;i <= 6000.0;i+=100.0)
	{
		std::cout << i << " " 
				  << sm->getValue(i).x << " " 
				  << sm->getValue(i).y << " "  
				  << sm->getValue(i).z << " "
				  << sm->getValue(i).w << " "
				  << std::endl;

	}
	entityManager.serialize("output_component_prop_segment_compare.xml");
	return 0;
}
#endif