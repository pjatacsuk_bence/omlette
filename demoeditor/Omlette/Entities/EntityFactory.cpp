#include "DXUT.h"
#include "EntityFactory.h"

using namespace Oml;

Entity::P	EntityFactory::createEntity(std::string name)
{
	return boost::shared_ptr<Entity>(new Entity(name));
}

Entity::P	EntityFactory::createEntity(pugi::xml_node& node)
{
	boost::shared_ptr<Entity> ret = boost::shared_ptr<Entity>(new Entity());
	ret->deserialize(node);
	return ret;
}