#ifndef INC_ENTITY_H
#define INC_ENTITY_H

#include "Omlette\Interfaces\IComponentManager.h"
#include "Omlette\Interfaces\IDrawable.h"
#include "Omlette\Interfaces\IUpdateable.h"
#include "Omlette\Interfaces\IGuiDrawable.h"
#include "Omlette\Interfaces\ISerializable.h"
#include "Omlette\Interfaces\ITimelineDrawable.h"
#include <boost\shared_ptr.hpp>


namespace Oml {
class Entity : public IDrawable,IUpdateable,IGuiDrawable,ISerializable,ITimelineDrawable
{
friend class EntityFactory;
public:
	typedef boost::shared_ptr<Entity> P;
	inline	IComponentManager*	getComponentManager(){return componentManager;}
	inline	std::string	getName(){return name;}
	std::vector<IPropertyAncestor::P>	getPropertiesFromPoint(Egg::Math::int2 point, D2D1_RECT_F rect);
	std::vector<std::string>			getComponentNames();
	std::vector<Component::P>			getComponents();
	Component::P						getComponentFromPoint(Egg::Math::int2 point, D2D1_RECT_F rect);
	virtual				~Entity();
	virtual	void		draw(RenderParameters renderParameters);
	virtual	void		update(float t);
	virtual	void		guiDraw();
	virtual	void		serialize(pugi::xml_node&	node);
	virtual	void		deserialize(pugi::xml_node&	node);
	virtual	void		timelineDraw(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget, ID2D1SolidColorBrush* brush, Egg::Math::float1 timelineMinTime, Egg::Math::float1 timelineMaxTime);
	virtual	int			getTimelineHeight();
	virtual	void		addNewKeyFrame(Egg::Math::int2 point, D2D1_RECT_F rect);
	virtual	void		deleteKeyFramesByPoint(Egg::Math::int2 point, D2D1_RECT_F rect);
	virtual void		buildComponentDependencies();
	virtual void		swapComponent(std::string componentName, Component::P component);
protected:
						Entity(std::string name);
						Entity();
	IComponentManager*	componentManager;
	std::string			name;

//***********************************************************/
	float x,y,z;
//***********************************************************/
};
}

#endif