 #ifndef INC_ENTITYFACTORY_H
 #define INC_ENTITYFACTORY_H

#include "Entity.h"

namespace Oml	{
class EntityFactory
{
public:
	static	EntityFactory&	getInstance()
	{
		static EntityFactory instance;
		return instance;

	}


	Entity::P				createEntity(std::string name);	
	Entity::P				createEntity(pugi::xml_node& node);	


private:
	EntityFactory(){}
	EntityFactory(EntityFactory const&);
	void operator=(EntityFactory const&);
};

}
#endif