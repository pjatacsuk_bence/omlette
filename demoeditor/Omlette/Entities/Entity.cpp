#include "DXUT.h"
#include "Entity.h"
#include <Omlette\Framework\Utility\Logger.h>
#include <Omlette\Components\ComponentMap.h>

#include <Omlette\Components\TransformComponent.h>
#include <Omlette\Components\ComponentFactory.h>
using namespace Oml;



Entity::Entity(std::string name):
name(name)
{
	componentManager = new ComponentMap(name);
	componentManager->addComponent(ComponentFactory::getInstance().createTransformComponent("transform"));
	Logger::getInstance().consoleLog("Entity constructor");	
}

Entity::Entity()
{
	
	componentManager = NULL;
}

int Entity::getTimelineHeight()
{
	return componentManager->getTimelineHeight();	
}

void Entity::deleteKeyFramesByPoint(Egg::Math::int2 point, D2D1_RECT_F rect)
{
	componentManager->deleteKeyFramesByPoint(point,rect);
}
void Entity::addNewKeyFrame(Egg::Math::int2 point, D2D1_RECT_F rect)
{
	componentManager->addNewKeyFrame(point, rect);
}

void Entity::buildComponentDependencies()
{
	componentManager->buildDependencies();
}

std::vector<std::string> Entity::getComponentNames()
{
	return	componentManager->getNames();
}

std::vector<Oml::Component::P> Entity::getComponents()
{
	return componentManager->getComponents();
}

Entity::~Entity()
{

	delete componentManager;
	Logger::getInstance().consoleLog("Entity->deconstructor()");	
	
}

void	Entity::draw(RenderParameters renderParameters)
{
	Logger::getInstance().consoleLog("Entity->draw()");	
	componentManager->draw(renderParameters);
}

void	Entity::update(float t)
{
	Logger::getInstance().consoleLog("Entity->update()");	
	componentManager->update(t);
}

void	Entity::guiDraw()
{
	Logger::getInstance().consoleLog("Entity->guiDraw()");	
	componentManager->guiDraw();
}

void	Entity::serialize(pugi::xml_node& node)
{
	Logger::getInstance().consoleLog("Entity->serialize()");	
    //[code_modify_add
    // add node with some name
    pugi::xml_node sub_node = node.append_child("Entity");

    // add description node with text child
    pugi::xml_node node_name = sub_node.append_child("name");
	node_name.append_child(pugi::node_pcdata).set_value(name.c_str());


	componentManager->serialize(sub_node);

}

void	Entity::deserialize(pugi::xml_node& node)
{
	for (pugi::xml_node param = node.first_child(); param; param = param.next_sibling())
	{
		if(std::string("name").compare(param.name())==0)
		{
			name = param.first_child().value();
			if(componentManager != NULL)
			{
				delete componentManager;
			}
			componentManager = new ComponentMap(name);

		}
		if(std::string("Components").compare(param.name())==0)
		{
			componentManager->deserialize(param);
		}
	}

	Logger::getInstance().consoleLog("Entity->deserialize()");	

}

void	Entity::timelineDraw(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget, ID2D1SolidColorBrush* brush, Egg::Math::float1 timelineMinTime, Egg::Math::float1 timelineMaxTime)
{
	componentManager->timelineDraw(rect,renderTarget,brush, timelineMinTime, timelineMaxTime);
}

std::vector<IPropertyAncestor::P>	Entity::getPropertiesFromPoint(Egg::Math::int2 point, D2D1_RECT_F rect)
{
	return componentManager->getPropertiesFromPoint(point, rect);
}

void Entity::swapComponent(std::string componentName, Oml::Component::P component)
{
//	component->setEntityName(name);
	componentManager->swapComponent(componentName, component);
}
Oml::Component::P Entity::getComponentFromPoint(Egg::Math::int2 point, D2D1_RECT_F rect)
{
	return componentManager->getComponentFromPoint(point, rect);
}