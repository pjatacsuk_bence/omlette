#include "DXUT.h"
#include "EntityManager.h"
#include <Omlette\Framework\Utility\Logger.h>
#include <Omlette\Framework\Gui\RightClickManager.h>
#include <Omlette\Components\TransformComponent.h>
#include <Omlette\Components\ComponentFactory.h>
#include <Omlette\Components\SolidColorMaterial.h>
#include <Omlette\Components\Component.h>
#include <Omlette\Components\LightComponent.h>

using namespace Oml;





EntityManager::~EntityManager()
{
	entities.clear();
}


Entity::P	EntityManager::getEntity(std::string name)
{
	auto it = entities.find(name);
	if (it != entities.end())
		return it->second;

	it = inactiveEntities.find(name);
	if (it != inactiveEntities.end())
		return it->second;
}

void		EntityManager::addEntity(Entity::P entity)
{
	entities[entity->getName()] = entity;
}
void		EntityManager::addInactiveEntity(Entity::P entity)
{
	inactiveEntities[entity->getName()] = entity;
}

void		EntityManager::serialize(std::string fileName)
{
	pugi::xml_document doc;
	pugi::xml_node node = doc.append_child("Entities");
	for(iterator it = entities.begin(); it != entities.end();it++)
	{
		it->second->serialize(node);
	}
	doc.save_file(fileName.c_str());
}

void		EntityManager::deserialize(std::string fileName)
{
	pugi::xml_document doc;
	pugi::xml_parse_result result = doc.load_file(fileName.c_str());
	if(result == NULL)
	{
		Logger::getInstance().consoleLog("Not found: " + fileName);
		return;
	}
	
	for (pugi::xml_node entities = doc.first_child().first_child(); entities;entities = entities.next_sibling())
	{
		addEntity(EntityFactory::getInstance().createEntity(entities));
	}
	for(iterator it = entities.begin(); it != entities.end();it++)
	{
		it->second->buildComponentDependencies();
	}


}

void		EntityManager::draw(ID3D11DeviceContext* context,ID3DX11Effect* effect, Oml::Mesh::Mien* mien)
{
	IDrawable::RenderParameters renderParameters;
	renderParameters.context = context;
	renderParameters.effect = effect;
	renderParameters.mien = mien;
	for(iterator it = entities.begin(); it != entities.end();it++)
	{
		it->second->draw(renderParameters);
	}
}
void		EntityManager::guiDraw()
{
	for(iterator it = entities.begin(); it != entities.end();it++)
	{
		it->second->guiDraw();
	}
}

void		EntityManager::update(float t)
{
	for(iterator it = entities.begin(); it != entities.end();it++)
	{
		it->second->update(t);
	}
}

Oml::Component::P EntityManager::getComponentFromEntity(std::string entityName, std::string componentName)
{
	auto it = entities.find(entityName);
	if (it != entities.end())
	{
		return it->second->getComponentManager()->getComponent(componentName);
	}
	it = inactiveEntities.find(entityName);
	if (it != inactiveEntities.end())
	{
		return it->second->getComponentManager()->getComponent(componentName);
	}
	return NULL;
}

void EntityManager::processMessage(HWND hWnd, unsigned uMsg, WPARAM wParam, LPARAM lParam)
{
	if(uMsg == WM_COMMAND)
	{
		switch(wParam)
		{
		case ID_COMMAND_ADD_ENTITY:
			addNewEmptyEntity();
			break;
		case ID_COMMAND_ADD_LIGHT:
			createNewEntityWithLight();
			break;
		}
	}
}

void EntityManager::addNewEmptyEntity()
{
	std::stringstream ss;
	ss << entities.size();
	Entity::P entity = EntityFactory::getInstance().createEntity("Default" +ss.str() );
	TransformComponent::P transformComponent = 
		boost::dynamic_pointer_cast<TransformComponent,Component>(
					ComponentFactory::getInstance().createTransformComponent("transform"));
	entity->getComponentManager()->addComponent(transformComponent);

	MaterialComponent::P materialComponent = SolidColorMaterial::create("scm");
	entity->getComponentManager()->addComponent(materialComponent);

	Component::P meshComponent = ComponentFactory::getInstance().createMeshRendererComponent("mesh",
																							 "./Resources/Meshes/giraffe.obj",
																							 materialComponent,
																							 transformComponent);
	entity->getComponentManager()->addComponent(meshComponent);

	addEntity(entity);
}

void EntityManager::createNewEntityWithLight()
{

	std::stringstream ss;
	ss << entities.size();
	Entity::P entity = EntityFactory::getInstance().createEntity("Default" +ss.str() );
	TransformComponent::P transformComponent = 
		boost::dynamic_pointer_cast<TransformComponent,Component>(
					ComponentFactory::getInstance().createTransformComponent("transform"));

	entity->getComponentManager()->addComponent(transformComponent);

	LightComponent::P lc = LightComponent::create("light", transformComponent);
	lc->setEntityName(entity->getName());
	entity->getComponentManager()->addComponent(lc);
	addEntity(entity);


}