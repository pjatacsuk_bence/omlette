#ifndef INC_ENTITYMANAGER_H
#define INC_ENTITYMANAGER_H

#include <Omlette\Entities\EntityFactory.h>
#include <Omlette\Interfaces\IDrawable.h>
#include <map>
namespace Oml{
class EntityManager 
{
public:
	static	EntityManager& getInstance()
	{
		static EntityManager instance;
		return instance;
	}
	
							~EntityManager();
	Entity::P				getEntity(std::string name);
	void					addEntity(Entity::P	entity);
	void					addInactiveEntity(Entity::P entity);

	void					serialize(std::string fileName);
	void					deserialize(std::string fileName);

	void					draw(ID3D11DeviceContext* context,ID3DX11Effect* effect, Mesh::Mien* mien);
	void					guiDraw();
	void					update(float t);
	Component::P			getComponentFromEntity(std::string entityName, std::string componentName);
	void					processMessage(HWND hWnd, unsigned uMsg, WPARAM wParam, LPARAM lParam);
	void					addNewEmptyEntity();
	void					createNewEntityWithLight();
	
private:
							EntityManager(){}

	std::map<std::string,Entity::P>	entities;
	std::map<std::string,Entity::P>	inactiveEntities;
	typedef std::map<std::string,Entity::P>::iterator iterator;

};


}
#endif