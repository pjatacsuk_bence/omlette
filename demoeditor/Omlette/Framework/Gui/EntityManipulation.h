#ifndef INC_ENTITYMANIPULATION_H
#define INC_ENTITYMANIPULATION_H

#include <Omlette\Framework\Utility\PickableManager.h>

class EntityManipulation
{
public:
	static EntityManipulation& getInstance()
	{
		static EntityManipulation instance;
		return instance;
	}

	enum Manipulation
	{
		TransformX,
		TransformY,
		TransformZ
	};

	void	processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	Egg::Math::float3	getPositionFromMouseCoord(Egg::Math::float2 mouseCoord, Manipulation manipulation);
	

private:
			EntityManipulation(){}
};

#endif