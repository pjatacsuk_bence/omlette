#include "DXUT.h"
#include <Omlette\Framework\Gui\ColorConverter.h>
#include <string>
#include <Math\math.h>

using namespace Oml;

template<class T>
T ColorConverter<T>::convert(T value){
	return value / 255.0;
}

template<>
Egg::Math::float1 ColorConverter<Egg::Math::float1>::convert(Egg::Math::float1 value)
{
	return Egg::Math::float1((float)value / (float)255.0);
}

template<>
std::string ColorConverter<std::string>::convert(std::string value){
	return value;
}

template class ColorConverter<int>;
template class ColorConverter<float>;
template class ColorConverter<class Egg::Math::float1>;
template class ColorConverter<class Egg::Math::float3>;
template class ColorConverter<class Egg::Math::float4>;
template class ColorConverter<std::string>;