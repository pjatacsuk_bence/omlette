#ifndef INC_GUITEXTBOXKEYFRAME_H
#define INC_GUITEXTBOXKEYFRAME_H
#include <Omlette\Interfaces\IGuiElement.h>
#include <Omlette\Framework\Gui\GuiTextBoxKeyFrameValue.hpp>
#include <string>
#include <Omlette\Framework\Segments\Segment.h>

namespace Oml
{
template<class T>
class GuiTextBoxKeyFrame : public GuiContainer 
{
public:
	typedef boost::shared_ptr<GuiTextBoxKeyFrame<T>> P;
	static	P	create(D2D1_RECT_F rect, KeyFrame<T>* keyFrame);

protected:
									GuiTextBoxKeyFrame(D2D1_RECT_F rect, KeyFrame<T>* keyFrame);
	KeyFrame<T>*					keyFrame;	
	
};	

template<class T>
typename GuiTextBoxKeyFrame<T>::P GuiTextBoxKeyFrame<T>::create(D2D1_RECT_F rect, KeyFrame<T>* keyFrame)
{
	return P(new GuiTextBoxKeyFrame<T>(rect,keyFrame));
}

template<class T>
GuiTextBoxKeyFrame<T>::GuiTextBoxKeyFrame(D2D1_RECT_F rect, KeyFrame<T>* keyFrame):
keyFrame(keyFrame)
{
}
template<>
inline
GuiTextBoxKeyFrame<Egg::Math::float1>::GuiTextBoxKeyFrame(D2D1_RECT_F rect, KeyFrame<Egg::Math::float1>* keyFrame):
GuiContainer(rect),
keyFrame(keyFrame)
{
	Egg::Math::float1* value = &(keyFrame->key);
		GuiTextBoxKeyFrameValue<Egg::Math::float1>::P textBox = 
			GuiTextBoxKeyFrameValue<Egg::Math::float1>::create(
											hitRect,
											GuiTextBox::InputType::NUMBER_ONLY,
											value);
		addGuiElement(textBox);
}
template<>
inline
GuiTextBoxKeyFrame<std::string>::GuiTextBoxKeyFrame(D2D1_RECT_F rect, KeyFrame<std::string>* keyFrame):
GuiContainer(rect),
keyFrame(keyFrame)
{
	std::string* value = &(keyFrame->key);
		GuiTextBoxKeyFrameValue<std::string>::P textBox = 
			GuiTextBoxKeyFrameValue<std::string>::create(
											hitRect,
											GuiTextBox::InputType::CHAR_ONLY,
											value);
		addGuiElement(textBox);
}
template<>
inline
GuiTextBoxKeyFrame<Egg::Math::float4>::GuiTextBoxKeyFrame(D2D1_RECT_F rect, KeyFrame<Egg::Math::float4>* keyFrame):
GuiContainer(rect),
keyFrame(keyFrame)
{
	float heightPerTextBox = fabs(hitRect.bottom - hitRect.top) / 4.0;
	
	D2D1_RECT_F currentRect = hitRect;
	for(int i=0;i<4;i++)
	{
		currentRect.top = hitRect.top + heightPerTextBox * i;
		currentRect.bottom = hitRect.top + heightPerTextBox * (i+1);
		float* value;
		if(i == 0)
		{
			value = &(keyFrame->key.x);
		}
		if(i == 1)
		{
			value = &(keyFrame->key.y);
		}
		if(i == 2)
		{
			value = &(keyFrame->key.z);
		}
		if(i == 3)
		{
			value = &(keyFrame->key.w);
		}
		GuiTextBoxKeyFrameValue<float>::P textBox = 
			GuiTextBoxKeyFrameValue<float>::create(
											currentRect,
											GuiTextBox::InputType::NUMBER_ONLY,
											value);
		addGuiElement(textBox);
	}
	
}

template<>
inline
GuiTextBoxKeyFrame<Egg::Math::float3>::GuiTextBoxKeyFrame(D2D1_RECT_F rect, KeyFrame<Egg::Math::float3>* keyFrame):
GuiContainer(rect),
keyFrame(keyFrame)
{
	float heightPerTextBox = fabs(hitRect.bottom - hitRect.top) / 4.0;
	
	D2D1_RECT_F currentRect = hitRect;
	for(int i=0;i<3;i++)
	{
		currentRect.top = hitRect.top + heightPerTextBox * i;
		currentRect.bottom = hitRect.top + heightPerTextBox * (i+1);
		float* value;
		if(i == 0)
		{
			value = &(keyFrame->key.x);
		}
		if(i == 1)
		{
			value = &(keyFrame->key.y);
		}
		if(i == 2)
		{
			value = &(keyFrame->key.z);
		}
		GuiTextBoxKeyFrameValue<float>::P textBox = 
			GuiTextBoxKeyFrameValue<float>::create(
											currentRect,
											GuiTextBox::InputType::NUMBER_ONLY,
											value);
		addGuiElement(textBox);
	}
	
}

}

#endif