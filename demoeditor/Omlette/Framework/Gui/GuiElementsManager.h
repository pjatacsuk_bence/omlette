#ifndef INC_GUIELEMENTMANAGER_H
#define INC_GUIELEMENTMANAGER_H
#include <Omlette\Interfaces\IGuiElement.h>
#include <vector>
#include <Omlette\Framework\Gui\GuiContainer.h>
#include <Omlette\Framework\Gui\GuiTimeline.h>
namespace Oml
{
class GuiElementsManager
{
public:
  typedef boost::shared_ptr<GuiElementsManager> P;
  inline static P create() { return P(new GuiElementsManager()); }

  ~GuiElementsManager();
  void addGuiContainer(GuiContainer::P guiContainer);
  void remove(GuiContainer::P guiContainer);
  void draw(ID2D1RenderTarget *renderTarget);
  void processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
  void setKeyFrameEditors(GuiContainer::P first, GuiContainer::P second);

  IGuiElement::P getFocusedGuiElement();
  IGuiElement::P getGuiElementUnderPoint(Egg::Math::int2 point);
  GuiTimeline::P getGuiTimeline();

  inline void setGuiInfoKeyFrame(GuiContainer::P g) {
    guiInfoKeyFrame = g;
    guiContainers[2] = guiInfoKeyFrame;
  }

  inline std::vector<GuiContainer::P>::iterator begin() {
    return (guiContainers.begin() + 2);
  }
  inline std::vector<GuiContainer::P>::iterator end() {
    return guiContainers.end();
  }

private:
  GuiElementsManager();

  IGuiElement::P focusedGuiElement;
  std::vector<GuiContainer::P> guiContainers;
  GuiContainer::P guiKeyFrameEditors[2];
  GuiContainer::P guiInfoKeyFrame;
  GuiTimeline::P guiTimeline;
  typedef std::vector<GuiContainer::P>::iterator iterator;
};
}

#endif