#ifndef INC_GUIMANAGER_H
#define INC_GUIMANAGER_H
#include <D2D1.h>
#include "d3dx11effect.h"
#include <DWrite.h>
#include <Omlette\Mesh\UnpickableShadedMesh.h>
#include <Omlette\Framework\Gui\EntityInfo.h>
#include <Omlette\Framework\Gui\GuiTimeline.h>
#include <Omlette\Framework\Gui\GuiTimelineBuilder.h>
#include <Omlette\Framework\Gui\GuiElementsManager.h>
#include <Omlette\Framework\Gui\GuiLabel.h>
#include <Omlette\Framework\Gui\GuiTextBoxKeyFrame.h>
#include <Omlette\Framework\Gui\EntityManipulation.h>
#include <Omlette\Framework\Gui\TransformMesh.h>
#include <Omlette\Framework\Gui\GuiInfoComponent.h>
#include <Omlette\Framework\Gui\GuiInfoKeyFrame.h>

namespace Oml {
class GuiManager {
public:
  static const int ZOOM_FACTOR_TIME_ZOOM = 500;
  static GuiManager &getInstance() {
    static GuiManager instance;
    return instance;
  }
  bool static isPointInsideRect(POINT p, D2D1_RECT_F rect) {
    return p.x > rect.left && p.x < rect.right && p.y < rect.bottom &&
           p.y > rect.top;
  }

  inline ID2D1SolidColorBrush *getBlackBrush() { return solidBlackBrush; }
  inline ID2D1SolidColorBrush *getSolidSofterGrayBrush() {
    return solidSofterGrayBrush;
  }
  inline ID2D1SolidColorBrush *getSolidGrayBrush() { return solidGrayBrush; }
  inline ID2D1SolidColorBrush *getSolidDarkGrayBrush() {
    return solidDarkGrayBrush;
  }
  inline ID2D1SolidColorBrush *getSolidDarkerGrayBrush() {
    return solidDarkerGrayBrush;
  }
  inline ID2D1SolidColorBrush *getSolidWhiteBrush() { return solidWhiteBrush; }
  inline ID2D1SolidColorBrush *getSolidRedBrush() { return solidRedBrush; }
  inline ID2D1SolidColorBrush *getSolidOrangeBrush() {
    return solidOrangeBrush;
  }
  inline ID2D1SolidColorBrush *getSolidBluishBrush() {
    return solidBluishBrush;
  }
  inline IDWriteTextFormat *getTextFormat() { return textFormat; }
  inline IGuiElement::P getFocusedGuiElement() {
    return guiElementsManager->getFocusedGuiElement();
  }
  inline IGuiElement::P getGuiElementUnderPoint(Egg::Math::int2 point) {
    return guiElementsManager->getGuiElementUnderPoint(point);
  }
  inline Component::P getCurrentComponent() {
    return guiInfoComponent->getComponent();
  }
  inline std::vector<IPropertyAncestor::P> getCurrentProperties() {
      return currentProperties;
  }
  inline IPropertyAncestor::P getCurrentProperty(){
      return curProp;
  }
  inline void setCurrentProperty(IPropertyAncestor::P p){
      curProp = p;
  }
  inline GuiTimeline::P getGuiTimeline() {
    return guiElementsManager->getGuiTimeline();
  }
  inline void setGuiInfoComponent(Component::P comp) {
    guiInfoComponent->setComponent(comp);
  }

  ~GuiManager();
  void processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
  void init(ID3D11Device *device, ID3DX11Effect *effect);
  void draw(ID3D11DeviceContext *context, ID3DX11Effect *effect);
  Egg::Math::float1 getTimeFromTimelinePoint(Egg::Math::int2 p);
  void update();
  void createSwapChainResources();
  void releaseSwapChainResources();
  void BindMethods();
  inline void resetKeyFrameEditors() {
    guiElementsManager->setGuiInfoKeyFrame(NULL);
  }
  inline bool isReadyToCleanKeyFrameInfo() { return readyToCleanKeyFrameInfo; }
  inline void setReadyToCleanKeyFrameInfo(bool b) {
    readyToCleanKeyFrameInfo = b;
  }

  inline bool isOverKeyFrame() { return overKeyFrame; }
  inline void setOverKeyFrame(bool b) { overKeyFrame = b; }

  inline bool isOverProperty() { return overProperty; }
  inline void setOverProperty(bool b) { overProperty = b; }

  template <typename T>
  void setTextBoxKeyFrames(IPropertyAncestor::P i, D2D1_RECT_F rect);

  template <typename T>
  void setGuiKeyFrameEditors(D2D1_RECT_F rect, KeyFrame<T> *keyFrame,
                             Oml::Converter conv = NO_CONVERT);

private:
  Egg::Math::int2 mousep;
  ID3D11Texture2D *guiTexture;
  ID3D11RenderTargetView *guiRtv;
  ID3D11ShaderResourceView *guiSrv;
  ID2D1RenderTarget *renderTarget;
  IDWriteFactory *writeFactory;
  IDWriteTextFormat *textFormat;
  ID2D1Factory *D2DFactory;
  ID2D1SolidColorBrush *solidRedBrush;
  ID2D1SolidColorBrush *solidSofterGrayBrush;
  ID2D1SolidColorBrush *solidGrayBrush;
  ID2D1SolidColorBrush *solidDarkGrayBrush;
  ID2D1SolidColorBrush *solidDarkerGrayBrush;
  ID2D1SolidColorBrush *solidBlackBrush;
  ID2D1SolidColorBrush *solidBluishBrush;
  ID2D1SolidColorBrush *solidWhiteBrush;
  ID2D1SolidColorBrush *solidOrangeBrush;
  ID2D1SolidColorBrush *transparentGreenBrush;

  ID3D11Texture2D *awsTexture;
  ID3D11ShaderResourceView *awsSrv;
  EntityInfo::P entityInfo;
  GuiInfoComponent::P guiInfoComponent;
  IGuiElement::P guiInfoKeyFrame;

  Mesh::UnpickableShadedMesh::P guiMesh;
  GuiElementsManager::P guiElementsManager;
  GuiContainer::P guiKeyFrameEditors[2];
  TransformMesh::P transformMesh;
  std::vector<IPropertyAncestor::P> currentProperties;
  IPropertyAncestor::P curProp;
  bool readyToCleanKeyFrameInfo;
  bool overKeyFrame;
  bool overProperty;

  void drawInfoFromPickedEntity(ID3D11DeviceContext *context,
                                ID3DX11Effect *effect);
  void updateTimeline();

  GuiManager();
};

template <typename T>
void GuiManager::setGuiKeyFrameEditors(D2D1_RECT_F rect, KeyFrame<T> *keyFrame,
                                       Oml::Converter conv) {
  GuiInfoKeyFrame<T>::P guiKF = GuiInfoKeyFrame<T>::create();
  guiKF->init(rect);
  guiKF->setKeyFrame(keyFrame, conv);
  guiInfoKeyFrame = guiKF;
  GuiContainer::P guic = GuiContainer::create(rect);
  guic->addGuiElement(guiKF);
  guiElementsManager->setGuiInfoKeyFrame(guic);
  /*
  guiKeyFrameEditors[0] = guic;
  guiElementsManager->setKeyFrameEditors(guic, NULL);
  */
}
template <typename T>
void GuiManager::setTextBoxKeyFrames(IPropertyAncestor::P i, D2D1_RECT_F rect) {

  Property<T>::P prop =
      Caster::getInstance().getPropertyCasted<Property<T>::P>(i);
  GuiLabel::draw(renderTarget, rect, (prop)->getName());

  static std::vector<KeyFrame<T> *> old_keyFrames;
  std::vector<KeyFrame<T> *> keyFrames;
  keyFrames = prop->getKeyFramesFromTime(TimeManager::getInstance().getTime());
  if (keyFrames.size() == 0) {
    guiKeyFrameEditors[0] = NULL;
    guiKeyFrameEditors[1] = NULL;
  } else if (old_keyFrames.size() == 0) {
    if (keyFrames.size() >= 1) {
      GuiContainer::P guic =
          (GuiTextBoxKeyFrame<T>::create(rect, (keyFrames[0])));
      guiKeyFrameEditors[0] = guic;
      rect.top = rect.bottom;
      rect.bottom = rect.bottom + 100;
    }
    if (keyFrames.size() >= 2) {
      GuiContainer::P guic =
          (GuiTextBoxKeyFrame<T>::create(rect, (keyFrames[1])));
      guiKeyFrameEditors[1] = guic;
      rect.top = rect.bottom + 100;
      rect.bottom = rect.bottom + 200;
    }
  }
  if (old_keyFrames.size() >= 1 && keyFrames.size() >= 1) {
    if (old_keyFrames[0] != keyFrames[0]) {
      GuiContainer::P guic =
          (GuiTextBoxKeyFrame<T>::create(rect, (keyFrames[0])));
      guiKeyFrameEditors[0] = guic;
      rect.top = rect.bottom;
      rect.bottom = rect.bottom + 100;
    }
    if (old_keyFrames.size() >= 2 && keyFrames.size() >= 2) {
      if (old_keyFrames[1] != keyFrames[1]) {
        GuiContainer::P guic =
            (GuiTextBoxKeyFrame<T>::create(rect, (keyFrames[1])));
        guiKeyFrameEditors[1] = guic;
        rect.top = rect.bottom + 100;
        rect.bottom = rect.bottom + 200;
      }
    }
  }

  old_keyFrames = keyFrames;
}
}

#endif