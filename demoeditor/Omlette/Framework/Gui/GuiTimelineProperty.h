#ifndef INC_GUITIMELINEPROPERTY_H
#define INC_GUITIMELINEPROPERTY_H
#include <Omlette\Interfaces\IGuiElement.h>
#include <Omlette\Framework\Properties\IPropertyAncestor.h>
#include <Omlette\Framework\Gui\GuiTimelineSegment.h>
#include <vector>
namespace Oml
{
class GuiTimelineProperty : public IGuiElement
{
public:
	typedef boost::shared_ptr<GuiTimelineProperty> P;
	static P create(Oml::IPropertyAncestor::P property) { return P(new GuiTimelineProperty(property)); }

	void				processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	void				draw(ID2D1RenderTarget* renderTarget){}
	void				draw(ID2D1RenderTarget* renderTarget, D2D1_RECT_F& rect, ID2D1SolidColorBrush* bgBrush);
    inline IPropertyAncestor::P getProperty(){ return property; }
private:
	GuiTimelineProperty(Oml::IPropertyAncestor::P property);
	Oml::IPropertyAncestor::P property;
	std::vector<IGuiElement::P> guiSegments;
};
}

#endif

