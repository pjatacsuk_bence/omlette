#ifndef INC_TIMELINEBILDER_H
#define INC_TIMELINEBILDER_H
#include <string>
#include <Omlette\Components\Component.h>


namespace Oml
{

class GuiTimelineBuilder
{
public:
	struct Properties
	{
		Property<Egg::Math::float4>::P pfloat4;
		Property<Egg::Math::float3>::P pfloat3;
		Property<Egg::Math::float1>::P pfloat1;
		Property<float>::P pfloat;
		Property<std::string>::P pstring;
		Property<unsigned int>::P pint;
		Properties()
		{
			pfloat4 = NULL;
			pfloat3 = NULL;
			pfloat1 = NULL;
			pfloat = NULL;
			pstring = NULL;
			pint = NULL;
		}
	};
					GuiTimelineBuilder(int width);
	std::string		buildTimeline();
	std::string		getComponentDiv(Oml::Component::P comp);
	std::string		getPropertyDiv(Oml::IPropertyAncestor::P comp);
	std::string		getFloat4PropertyDiv(Property<Egg::Math::float4>::P p);
	std::string		getFloat3PropertyDiv(Property<Egg::Math::float3>::P p);
	std::string		getFloat1PropertyDiv(Property<Egg::Math::float1>::P p);
	std::string		getFloatPropertyDiv(Property<float>::P p);
	std::string		getStringPropertyDiv(Property<std::string>::P p);
	std::string		getIntPropertyDiv(Property<unsigned int>::P p);
	inline int		getWidth(){ return width; }
private:

	int width;
	int maxTime;
	std::string		float4Color;
	std::string		float3Color;
	std::string		float1Color;
	std::string		floatColor;
	std::string		stringColor;
	std::string		intColor;
};
}

#endif