#ifndef INC_GUICONTAINER_H
#define INC_GUICONTAINER_H
#include <boost\smart_ptr\shared_ptr.hpp>
#include <D2D1.h>
#include <Omlette\Interfaces\IGuiElement.h>
#include <vector>

namespace Oml
{
class GuiContainer
{
public:
	typedef boost::shared_ptr<GuiContainer> P;
	static P create(D2D1_RECT_F rect);

	void						draw(ID2D1RenderTarget* renderTarget);
	void						addGuiElement(IGuiElement::P guiElement);
	IGuiElement::P				getGuiElement(POINT p);
	virtual						~GuiContainer();

	inline D2D1_RECT_F					getRect(){ return hitRect; }
protected:
								GuiContainer(D2D1_RECT_F rect);

	D2D1_RECT_F					hitRect;		

	std::vector<IGuiElement::P>	guiElements;
	typedef std::vector<IGuiElement::P>::iterator iterator;
};
}


#endif