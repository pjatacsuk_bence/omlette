#include "DXUT.h"
#include <Omlette\Framework\Gui\GuiTextBoxKeyFrame.h>

#include <Omlette\Framework\Gui\GuiContainer.h>

using namespace Oml;

template<class T>
typename GuiTextBoxKeyFrame<T>::P GuiTextBoxKeyFrame<T>::create(D2D1_RECT_F rect, KeyFrame<T>* keyFrame, Oml::Converter conv)
{
	return P(new GuiTextBoxKeyFrame<T>(rect,keyFrame,conv));
}

template<> GuiTextBoxKeyFrame<int>::GuiTextBoxKeyFrame(D2D1_RECT_F rect, KeyFrame<int>* keyFrame, Oml::Converter conv):
GuiContainer(rect),
keyFrame(keyFrame)
{
	int* value = &(keyFrame->key);
		GuiTextBoxKeyFrameValue<int>::P textBox = 
			GuiTextBoxKeyFrameValue<int>::create(
											hitRect,
											GuiTextBox::InputType::NUMBER_ONLY,
											value);
		textBox->setConverter(conv);
		addGuiElement(textBox);
}
template<> GuiTextBoxKeyFrame<float>::GuiTextBoxKeyFrame(D2D1_RECT_F rect, KeyFrame<float>* keyFrame, Oml::Converter conv):
GuiContainer(rect),
keyFrame(keyFrame)
{
	float* value = &(keyFrame->key);
		GuiTextBoxKeyFrameValue<float>::P textBox = 
			GuiTextBoxKeyFrameValue<float>::create(
											hitRect,
											GuiTextBox::InputType::NUMBER_ONLY,
											value);
		textBox->setConverter(conv);
		addGuiElement(textBox);
}
template<> GuiTextBoxKeyFrame<Egg::Math::float1>::GuiTextBoxKeyFrame(D2D1_RECT_F rect, KeyFrame<Egg::Math::float1>* keyFrame, Oml::Converter conv):
GuiContainer(rect),
keyFrame(keyFrame)
{
	Egg::Math::float1* value = &(keyFrame->key);
		GuiTextBoxKeyFrameValue<Egg::Math::float1>::P textBox = 
			GuiTextBoxKeyFrameValue<Egg::Math::float1>::create(
											hitRect,
											GuiTextBox::InputType::NUMBER_ONLY,
											value);
		textBox->setConverter(conv);
		addGuiElement(textBox);
}
template<>
GuiTextBoxKeyFrame<std::string>::GuiTextBoxKeyFrame(D2D1_RECT_F rect, KeyFrame<std::string>* keyFrame, Oml::Converter conv):
GuiContainer(rect),
keyFrame(keyFrame)
{
	std::string* value = &(keyFrame->key);
		GuiTextBoxKeyFrameValue<std::string>::P textBox = 
			GuiTextBoxKeyFrameValue<std::string>::create(
											hitRect,
											GuiTextBox::InputType::CHAR_ONLY,
											value);
		textBox->setConverter(conv);
		addGuiElement(textBox);
}
template<>
GuiTextBoxKeyFrame<Egg::Math::float4>::GuiTextBoxKeyFrame(D2D1_RECT_F rect, KeyFrame<Egg::Math::float4>* keyFrame, Oml::Converter conv):
GuiContainer(rect),
keyFrame(keyFrame)
{
	float heightPerTextBox = fabs(hitRect.bottom - hitRect.top) / 4.0;
	
	D2D1_RECT_F currentRect = hitRect;
	for(int i=0;i<4;i++)
	{
		currentRect.top = hitRect.top + heightPerTextBox * i;
		currentRect.bottom = hitRect.top + heightPerTextBox * (i+1);
		float* value;
		if(i == 0)
		{
			value = &(keyFrame->key.x);
		}
		if(i == 1)
		{
			value = &(keyFrame->key.y);
		}
		if(i == 2)
		{
			value = &(keyFrame->key.z);
		}
		if(i == 3)
		{
			value = &(keyFrame->key.w);
		}
		GuiTextBoxKeyFrameValue<float>::P textBox = 
			GuiTextBoxKeyFrameValue<float>::create(
											currentRect,
											GuiTextBox::InputType::NUMBER_ONLY,
											value);
		textBox->setConverter(conv);
		addGuiElement(textBox);
	}
	
}

template<>
GuiTextBoxKeyFrame<Egg::Math::float3>::GuiTextBoxKeyFrame(D2D1_RECT_F rect, KeyFrame<Egg::Math::float3>* keyFrame, Oml::Converter conv):
GuiContainer(rect),
keyFrame(keyFrame)
{
	float heightPerTextBox = fabs(hitRect.bottom - hitRect.top) / 4.0;
	
	D2D1_RECT_F currentRect = hitRect;
	for(int i=0;i<3;i++)
	{
		currentRect.top = hitRect.top + heightPerTextBox * i;
		currentRect.bottom = hitRect.top + heightPerTextBox * (i+1);
		float* value;
		if(i == 0)
		{
			value = &(keyFrame->key.x);
		}
		if(i == 1)
		{
			value = &(keyFrame->key.y);
		}
		if(i == 2)
		{
			value = &(keyFrame->key.z);
		}
		GuiTextBoxKeyFrameValue<float>::P textBox = 
			GuiTextBoxKeyFrameValue<float>::create(
											currentRect,
											GuiTextBox::InputType::NUMBER_ONLY,
											value);
		textBox->setConverter(conv);
		addGuiElement(textBox);
	}
	
}



template class GuiTextBoxKeyFrame<int>;
template class GuiTextBoxKeyFrame<float>;
template class GuiTextBoxKeyFrame<class Egg::Math::float1>;
template class GuiTextBoxKeyFrame<class Egg::Math::float3>;
template class GuiTextBoxKeyFrame<class Egg::Math::float4>;
template class GuiTextBoxKeyFrame<std::string>;