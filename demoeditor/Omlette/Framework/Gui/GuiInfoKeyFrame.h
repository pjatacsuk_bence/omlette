#ifndef INC_GUIINFOKEYFRAME_H
#define INC_GUIINFOKEYFRAME_H
#include <Omlette\Framework\Segments\Segment.h>
#include <Omlette\Interfaces\IGuiElement.h>
#include <Omlette\Framework\Gui\GuiTextBoxKeyFrame.h>

namespace Oml{

template<class T>
class GuiInfoKeyFrame : public IGuiElement{
public:

	typedef boost::shared_ptr<GuiInfoKeyFrame> P;
	static P create(){ return P(new GuiInfoKeyFrame()); }
	void	init(D2D1_RECT_F rect);
	void	processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	void	draw(ID2D1RenderTarget* renderTarget);
	void	execute();

	inline void setKeyFrame(KeyFrame<T>* kf, Oml::Converter conv = NO_CONVERT)
	{ 
		keyFrame = kf; 
		guiTextBoxKeyFrame = (typename GuiTextBoxKeyFrame<T>::create( hitRect, keyFrame, conv ));
	}
private:
	typename KeyFrame<T>* keyFrame;
	typename GuiTextBoxKeyFrame<T>::P guiTextBoxKeyFrame;
	IGuiElement::P focusedGuiElement;
	GuiInfoKeyFrame();
};
}
#endif