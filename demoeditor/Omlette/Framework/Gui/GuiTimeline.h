#ifndef INC_GUITIMELINE_H
#define INC_GUITIMELINE_H
#include <Omlette\Entities\Entity.h>
#include <Omlette\Interfaces\IGuiElement.h>
#include <Omlette\Framework\Gui\GuiTimelineComponent.h>
#include <Omlette\Framework\Gui\GuiScrollable.h>

namespace Oml
{
class GuiTimeline : public IGuiElement
{
 public:
	static const int HEIGHT_FOR_COMPONENT = 10;
	static const int HEIGHT_FOR_PROPERTY = 10;
	static const int ZOOM_FACTOR_TIME_ZOOM = 500;

	typedef boost::shared_ptr<GuiTimeline> P;
	static inline	P		create(){return P(new GuiTimeline());}

	inline Egg::Math::float1		getMinTime() { return minTime; }
	inline Egg::Math::float1		getMaxTime() { return maxTime; }

	Egg::Math::float1		getTimeFromTimelinePoint(Egg::Math::int2 p);
	std::vector<IPropertyAncestor::P>	getPropertiesFromPoint(Egg::Math::int2 point, D2D1_RECT_F rect);
	std::vector<IPropertyAncestor::P>	getCurrentProperties();
	Component::P						getComponentFromPoint(Egg::Math::int2 point, D2D1_RECT_F rect);
	Component::P						getCurrentComponent();

	void					init();
	void					update();
	void					draw(ID2D1RenderTarget* renderTarget);
	void					drawProto(ID2D1RenderTarget* renderTarget);
	void					drawTimeStepLines(D2D1_RECT_F rect, int step, ID2D1RenderTarget* renderTarget, ID2D1SolidColorBrush* color);
	void					processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	D2D1_RECT_F				getTimelineRect();
	D2D1_RECT_F				getRect();
	void					moveTimelineByTime(Egg::Math::float1 timeMove);
	D2D1_RECT_F				getScrollBoxRect();
	void					addZoomFactor(int zoomFactor);
	void					addZoomFactorRelativeToMouse(int zoomFactor,Egg::Math::int2 mousePos);
	inline	void			setRectBottomRight(Egg::Math::int2 br){ rectBottomRight = br; }
	inline	int				getScrollVerticalPos(){ return guiScrollable.getScrollAmountY(); }
private:
							GuiTimeline();
	Entity::P				entity;
	Egg::Math::float1		minTime;
	Egg::Math::float1		maxTime;
	Egg::Math::int2			rectTopLeft;
	Egg::Math::int2			rectBottomRight;
	Egg::Math::float1		scrollBoxHeight;

	int						lButtonOldXPos;	

	int						scrollVerticalPos;
	int						scrollDistance;

	D2D1_RECT_F				timelineRect;

	GuiScrollable			guiScrollable;

	Component::P						currentComponent;
	std::vector<IPropertyAncestor::P>	currentProperties;
	std::vector<GuiTimelineComponent::P> guiTimelineComponents;
};

}






#endif