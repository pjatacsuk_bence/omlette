#ifndef INC_GUILABEL_H
#define INC_GUILABEL_H
#include <Omlette\Interfaces\IGuiElement.h>
#include <string>

namespace Oml
{
class GuiLabel : public IGuiElement
{
public:
	enum ALIGN
	{
		LEFT,RIGHT,CENTER	
	};

	typedef boost::shared_ptr<GuiLabel> P;
	static GuiLabel::P	create(D2D1_RECT_F rect, std::string text);

	void			processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	void			draw(ID2D1RenderTarget* renderTarget);
	static	void	draw(ID2D1RenderTarget* renderTarget, D2D1_RECT_F rect, std::string text, ID2D1SolidColorBrush* textBrush = NULL, ID2D1SolidColorBrush* brackgroundBrush = NULL, ALIGN align = CENTER);
	std::string		getText();
	void			setText(std::string text);

	static	void			incTabCount(){ tabCount++; }
	static	void			decTabCount(){ tabCount--; }

	static	std::string		getTabs(){ std::string ret = ""; for (int i = 0; i < tabCount; i++) ret += "    "; return ret; }

protected:
					GuiLabel(D2D1_RECT_F rect,std::string text);
	std::string		text;
	static int		tabCount;

	
};	


}

#endif