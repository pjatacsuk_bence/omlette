#include "DXUT.h"
#include <Omlette\Framework\Gui\GuiInfoComponent.h>
#include <Omlette\Framework\Gui\GuiManager.h>
#include <Omlette\Framework\Gui\GuiLabel.h>

using namespace Oml;

GuiInfoComponent::GuiInfoComponent()
{

}
void GuiInfoComponent::init(D2D1_RECT_F rect)
{
	hitRect = rect;
}
void GuiInfoComponent::processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{

}
void GuiInfoComponent::draw(ID2D1RenderTarget* renderTarget)
{
	if (component != NULL)
	{
		renderTarget->FillRectangle(hitRect, GuiManager::getInstance().getSolidGrayBrush());
		GuiLabel::draw(renderTarget, hitRect, component->getEntityName() + "::" + component->getName());

	}
}