#include "DXUT.h"
#include <Omlette\Framework\Gui\EntityInfo.h>
#include <Omlette\Framework\Gui\GuiManager.h>
#include <Omlette\Framework\Gui\RightClickManager.h>
#include <Omlette\Components\ComponentCompabilityManager.h>
#include <Omlette\Components\ComponentFactory.h>
#include <Windows.h>
#include <Omlette\Framework\Utility\Logger.h>
#include <Omlette\Framework\Utility\Caster.h>
#include <Omlette\Components\MultiMeshRendererComponent.h>
using namespace Oml;

EntityInfo::EntityInfo()
{

}

void EntityInfo::setEntity(Entity::P entity)
{
	this->entity = entity;
	componentRects.clear();
	if (entity != NULL)
	{

		std::vector<Component::P> components = entity->getComponents();
		float heightPerString = 20;
		hitRect.bottom = (components.size() + 1) * heightPerString;
		hitRect.left = 0;
		hitRect.top = 0;
		hitRect.right = 300;

		D2D1_RECT_F rect;
		rect.left = 0;
		rect.top = 0;
		rect.right = 300;
		rect.bottom = heightPerString;

		D2D1_RECT_F crect = rect;
		int n = 1;
		for (auto i = components.begin();
			i != components.end();
			i++)
		{
			crect.top = rect.top + n * heightPerString;
			crect.bottom = rect.top + (n + 1) * heightPerString;
			ComponentRect componentRect;
			componentRect.comp = *i;
			componentRect.rect = crect;
			componentRects.push_back(componentRect);
			n++;
		}
	}
}

void EntityInfo::draw(ID2D1RenderTarget* renderTarget)
{
	if (entity != NULL)
	{
		float heightPerString = 20;
		D2D1_RECT_F rect = hitRect;
		renderTarget->FillRectangle(&rect, GuiManager::getInstance().getSolidGrayBrush());

		D2D1_RECT_F entityNameRect = rect;
		entityNameRect.bottom = rect.top + heightPerString;
		rect.bottom = heightPerString;
		GuiLabel::draw(renderTarget, entityNameRect, entity->getName());
		D2D1_POINT_2F pointLeft;
		pointLeft.x = entityNameRect.left;
		pointLeft.y = entityNameRect.bottom;
		D2D1_POINT_2F pointRight;
		pointRight.x = entityNameRect.right;
		pointRight.y = entityNameRect.bottom;
		renderTarget->DrawLine(pointLeft, pointRight, GuiManager::getInstance().getBlackBrush());
		for (auto i = componentRects.begin();
			i != componentRects.end();
			i++)
		{
			std::string componentName = (*i).comp->getName();
			std::string componentType = (*i).comp->getComponentTypeName();
			std::string text = componentType + "::" + componentName;
			GuiLabel::draw(renderTarget, (*i).rect, text);
		}

	}
}

Oml::Component::P	EntityInfo::getComponentUnderPoint(Egg::Math::int2 point)
{
	for (auto it = componentRects.begin();
		it != componentRects.end();
		it++)
	{
		POINT p;
		p.x = point.x;
		p.y = point.y;
		if (GuiManager::isPointInsideRect(p, it->rect))
		{
			return it->comp;
		}
	}
	return NULL;
}
void EntityInfo::createComponentSpecificRightClickMenus(HWND hWnd, Oml::Component::P component, HMENU hmenu)
{
	int pos = 2;
	if (component->getComponentTypeName().compare("MeshRendererComponent") == 0 || 
		component->getComponentTypeName().compare("MultiMeshRendererComponent") == 0)
	{
		std::wstring stemp(L"Replace Mesh");
		LPWSTR sw = const_cast<LPWSTR>(stemp.c_str());

		MENUITEMINFO lpmii;
		lpmii.cbSize = sizeof(MENUITEMINFO);
		lpmii.fType = MFT_STRING;
		lpmii.cch = 256;
		lpmii.fMask = MIIM_STRING | MIIM_ID;
		lpmii.wID = ID_COMMAND_REPLACE_MESH;

		lpmii.dwTypeData = sw;
		InsertMenuItem(hmenu, pos++, true, &lpmii);
	}
}
void EntityInfo::createComponentRightClickMenus(HWND hWnd, Oml::Component::P component, Egg::Math::int2 pos)
{
	HMENU hPopupMenu = CreatePopupMenu();
	HMENU replaceMenu = CreatePopupMenu();
	
	InsertMenu(hPopupMenu, 0, MF_BYPOSITION | MF_STRING, ID_COMMAND_DELETE_COMPONENT, L"Delete Component");
	InsertMenu(hPopupMenu, 1, MF_BYPOSITION | MF_STRING | MF_POPUP, (UINT_PTR)replaceMenu, L"Replace Component to");

	MENUINFO minfo = { sizeof(MENUINFO), 0 };
	minfo.fMask = MIM_STYLE | MIM_APPLYTOSUBMENUS;
	
	GetMenuInfo(hPopupMenu,&minfo);
	minfo.dwStyle |= MNS_NOTIFYBYPOS;
	SetMenuInfo(hPopupMenu,&minfo);

	std::vector<std::string> compatibleComponents = 
		ComponentCompabilityManager::getInstance().getCompatibleComponents(component->getComponentTypeName());
	int menuPos = 0;
	for (auto it = compatibleComponents.begin();
		it != compatibleComponents.end();
		it++)
	{
		if (it ->compare(component->getComponentTypeName()) != 0)
		{
			std::wstring stemp = std::wstring(it->begin(), it->end());
			LPWSTR sw = const_cast<LPWSTR>(stemp.c_str());

			MENUITEMINFO lpmii;
			lpmii.cbSize = sizeof(MENUITEMINFO);
			lpmii.fType = MFT_STRING;
			lpmii.cch = 256;
			lpmii.fMask = MIIM_STRING | MIIM_ID;
			lpmii.wID = ID_COMMAND_REPLACE_COMPONENT_TO_COMP;

			lpmii.dwTypeData = sw;
			InsertMenuItem(replaceMenu, menuPos++, true, &lpmii);
		}
	}
	createComponentSpecificRightClickMenus(hWnd, component, hPopupMenu);
	RECT rect;
	GetWindowRect(hWnd, &rect);
	SetForegroundWindow(hWnd);
	TrackPopupMenu(hPopupMenu, TPM_TOPALIGN | TPM_LEFTALIGN, rect.left + pos.x, rect.top + pos.y, 0, hWnd, NULL);
}
void EntityInfo::processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if (uMsg == WM_RBUTTONDOWN)
	{
		int x  = LOWORD(lParam);
		int y  = HIWORD(lParam);
		Component::P component = getComponentUnderPoint(Egg::Math::int2(x, y));
		rightClickedComponent = component;
		if (component != NULL)
		{
			createComponentRightClickMenus(hWnd, component, Egg::Math::int2(x, y));
		}
	}
	if (uMsg == WM_MENUCOMMAND)
	{
		HMENU hmenu = (HMENU)lParam;
		int item = wParam;
		MENUITEMINFO mii;
		mii.cbSize = sizeof(MENUITEMINFO);
		mii.fMask = MIIM_STRING |MIIM_ID;
		mii.fType = MFT_STRING;
		char* szString = new char[256];
		mii.cch = 256;
		mii.dwTypeData = (LPWSTR)szString;
		if (!GetMenuItemInfo(hmenu, item, true, &mii))
		{
			DWORD word = GetLastError();
			std::stringstream ss;
			ss << word;
			Logger::getInstance().fileLog(ss.str());
		}
		std::wstring componentName((LPWSTR)mii.dwTypeData);
		switch (mii.wID)
		{
			case ID_COMMAND_REPLACE_COMPONENT_TO_COMP:
				replaceComponent(rightClickedComponent, componentName);
				break;
			case ID_COMMAND_REPLACE_MESH:
				replaceMesh(rightClickedComponent);
				break;
		}
	}
}

void EntityInfo::replaceMesh(Oml::Component::P component)
{

	LPWSTR str = new WCHAR[512];
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = NULL;
	ofn.lpstrFile = str;
	ofn.lpstrFilter = L"All\0*.*\0Obj\0*.obj\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = 512;
	ofn.lpstrFileTitle = NULL;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	if (GetOpenFileName(&ofn))
	{
		std::wstring wfilepath(ofn.lpstrFile);
		std::string filepath(wfilepath.begin(), wfilepath.end());
		int meshCount = ResourceManager::getInstance().getGeometryCount(filepath);
		if (meshCount == 1)
		{
			MeshRendererComponent::P mrc = Caster::getInstance().getComponentCasted<MeshRendererComponent::P>(component);
			if (ResourceManager::getInstance().getGeometry(filepath).size() == 0)
			{
				ResourceManager::getInstance().loadGeometry(filepath);
			}
			mrc->replaceMesh(ResourceManager::getInstance().getGeometry(filepath)[0]);
		}
		else
		{
			Oml::Component::P compTfc = entity->getComponentManager()->getComponentsByType("TransformComponent")[0];
			Oml::Component::P compMat = entity->getComponentManager()->getComponentsByType("MaterialComponent")[0];
			TransformComponent::P tfc = Caster::getInstance().getComponentCasted<TransformComponent::P>(compTfc);
			MaterialComponent::P mtc = Caster::getInstance().getComponentCasted<MaterialComponent::P>(compMat);
			MultiMeshRendererComponent::P mmrc = MultiMeshRendererComponent::create("mmrc",filepath,tfc, mtc);
			MeshRendererComponent::P mrc = Caster::getInstance().getComponentCasted<MeshRendererComponent::P>(component);
			PickableManager::getInstance().removePickable(mrc.get());
			entity->swapComponent(component->getName(), mmrc);
		}
	}
	delete[] str;
}
void EntityInfo::replaceComponent(Oml::Component::P component, std::wstring componentTypeName)
{
	Component::P newComponent = ComponentFactory::getInstance().createComponent(componentTypeName);
	entity->swapComponent(component->getName(), newComponent);
}