#ifndef INC_COMMANDMANAGER_H
#define INC_COMMANDMANAGER_H
namespace Oml
{
class CommandManager
{
public:
	enum Command
	{
		DraggingTimeline,
		DraggingScrollbar,
		HandlingFocusedGuiElement,
		DraggingEntityPositionX,
		DraggingEntityPositionY,
		DraggingEntityPositionZ,
		None
	};
	
	static CommandManager& getInstance()
	{
		static CommandManager instance;
		return instance;
	}

	void	setCommand(Command command);
	Command	getCommand();

private:
	Command	command;
	CommandManager()
	{
		command = Command::None;	
	}
};

}
#endif