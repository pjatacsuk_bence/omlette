#include "DXUT.h"
#include <Omlette\Framework\Gui\GuiInfoKeyFrame.h>
#include <Omlette\Framework\Utility\ResourceManager.h>

using namespace Oml;

template<class T>
GuiInfoKeyFrame<T>::GuiInfoKeyFrame(){

}

template<class T>
void GuiInfoKeyFrame<T>::init(D2D1_RECT_F rect)
{
	hitRect = rect;
}

template<class T>
void GuiInfoKeyFrame<T>::execute()
{
	if (focusedGuiElement != NULL)
	{
		focusedGuiElement->execute();
		focusedGuiElement = NULL;
	}
	
}
template<class T>
void GuiInfoKeyFrame<T>::processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{

	if (uMsg == WM_LBUTTONDOWN){
		POINT p = ResourceManager::getInstance().getMouseCoords(lParam);
		auto e = guiTextBoxKeyFrame->getGuiElement(p);
		if (e != focusedGuiElement)
		{
			focusedGuiElement = e;
		}
	}
	if (focusedGuiElement != NULL){
		focusedGuiElement->processMessage(hWnd, uMsg, wParam, lParam);
	}

}
template<class T>
void GuiInfoKeyFrame<T>::draw(ID2D1RenderTarget* renderTarget)
{
	guiTextBoxKeyFrame->draw(renderTarget);
}

template class GuiInfoKeyFrame<int>;
template class GuiInfoKeyFrame<float>;
template class GuiInfoKeyFrame<class Egg::Math::float1>;
template class GuiInfoKeyFrame<class Egg::Math::float3>;
template class GuiInfoKeyFrame<class Egg::Math::float4>;
template class GuiInfoKeyFrame<std::string>;