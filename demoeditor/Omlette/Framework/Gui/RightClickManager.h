#ifndef INC_RIGHTCLICKMANAGER_H
#define INC_RIGHTCLICKMANAGER_H
#include <comcat.h>

namespace Oml
{

static const int ID_COMMAND_ADD_ENTITY = 0x9001;
static const int ID_COMMAND_DELETE_COMPONENT = 0x9002;
static const int ID_COMMAND_REPLACE_COMPONENT = 0x9003;
static const int ID_COMMAND_REPLACE_COMPONENT_TO_COMP = 0x9004;
static const int ID_COMMAND_REPLACE_MESH = 0x9005;
static const int ID_COMMAND_ADD_LIGHT = 0x9006;



class RightClickManager
{
public:	
	static RightClickManager&	getInstance()
	{
		static RightClickManager instance;
		return instance;
	}

	void	processMessage(HWND hWnd, unsigned uMsg, WPARAM wParam, LPARAM lParam);

private:

};



}
#endif