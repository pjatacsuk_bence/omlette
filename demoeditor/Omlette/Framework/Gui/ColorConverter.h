#ifndef INC_COLORCONVERTER_H
#define INC_COLORCONVERTER_H

#include <Omlette\Interfaces\IConverter.h>
namespace Oml{

template <class T>
class ColorConverter : public IConverter<T>{
public:
	typedef boost::shared_ptr<ColorConverter> P;
	static P create(){ return P(new ColorConverter()); }
	T convert(T value);
private:
};
}

#endif