#include "DXUT.h"
#include <Omlette\Framework\Gui\RightClickManager.h>
#include <Omlette\Entities\Entity.h>
#include <Omlette\Entities\EntityFactory.h>
#include <Omlette\Framework\Gui\GuiManager.h>

using namespace Oml;

void RightClickManager::processMessage(HWND hWnd, unsigned uMsg, WPARAM wParam, LPARAM lParam)
{
	if (uMsg == WM_RBUTTONDOWN)
	{
		int x = LOWORD(lParam);
		int y = HIWORD(lParam);

		Egg::Math::int2 mouseCoord(x, y);
		IGuiElement::P guiElement = GuiManager::getInstance().getGuiElementUnderPoint(mouseCoord);
		if (guiElement != NULL)
		{
			guiElement->processMessage(hWnd, uMsg, wParam, lParam);
		}
		else
		{
			HMENU hPopupMenu = CreatePopupMenu();
			InsertMenu(hPopupMenu, 0, MF_BYPOSITION | MF_STRING, ID_COMMAND_ADD_ENTITY, L"Add Entity");
			InsertMenu(hPopupMenu, 1, MF_BYPOSITION | MF_STRING, ID_COMMAND_ADD_LIGHT, L"Add Light");
			RECT rect;
			GetWindowRect(hWnd, &rect);
			SetForegroundWindow(hWnd);
			TrackPopupMenu(hPopupMenu, TPM_TOPALIGN | TPM_LEFTALIGN, rect.left + x, rect.top + y, 0, hWnd, NULL);

		}
	}
}