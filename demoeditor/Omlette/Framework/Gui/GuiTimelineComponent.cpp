#include "DXUT.h"
#include <Omlette\Components\ComponentMap.h>
#include <Omlette\Framework\Gui\GuiTimeline.h>
#include <Omlette\Framework\Gui\GuiTimelineComponent.h>
#include <Omlette\Framework\Gui\GuiManager.h>
using namespace Oml;

GuiTimelineComponent::GuiTimelineComponent(Oml::Component::P component):
component(component)
{
	auto components = component->getComponentManager()->getComponents();
	auto it = components.begin();

	while (it != components.end())
	{
		GuiTimelineComponent::P child = create(*it);
		childs.push_back(child);
		it++;
	}
	auto props = component->getPropertyMap();
	auto props_it = props.begin();
	while (props_it != props.end())
	{
		GuiTimelineProperty::P prop = GuiTimelineProperty::create(props_it->second);
		properties.push_back(prop);
		props_it++;
	}

}

void GuiTimelineComponent::draw(ID2D1RenderTarget* renderTarget)
{

}

void GuiTimelineComponent::draw(ID2D1RenderTarget* renderTarget, D2D1_RECT_F& rect, ID2D1SolidColorBrush* bgBrush)
{
	D2D1_RECT_F compRect = rect;
	compRect.bottom = rect.top + GuiTimeline::HEIGHT_FOR_COMPONENT;
	renderTarget->FillRectangle(compRect, bgBrush);

	D2D1_RECT_F nameRect = compRect;
	GuiLabel::incTabCount();
	std::string text = GuiLabel::getTabs() + component->getName() + "::" + component->getComponentTypeName();
	GuiLabel::draw(renderTarget,
		nameRect,
		text,
		GuiManager::getInstance().getBlackBrush(),
		bgBrush,GuiLabel::ALIGN::LEFT);

	rect.top = compRect.bottom;

	for (auto it = properties.begin(); it != properties.end(); it++)
	{
		(*it)->draw(renderTarget, rect, GuiManager::getInstance().getSolidBluishBrush());
	}

	int count = 0;
	ID2D1SolidColorBrush* scb = GuiManager::getInstance().getSolidDarkerGrayBrush();
	if (bgBrush == GuiManager::getInstance().getSolidDarkerGrayBrush())
	{
		scb = GuiManager::getInstance().getSolidDarkGrayBrush();
	}
	else if (bgBrush == GuiManager::getInstance().getSolidDarkGrayBrush())
	{
		scb = GuiManager::getInstance().getSolidGrayBrush();
	}
	else if (bgBrush == GuiManager::getInstance().getSolidGrayBrush()) 
	{
		scb = GuiManager::getInstance().getSolidSofterGrayBrush();
	}
	else if (bgBrush == GuiManager::getInstance().getSolidSofterGrayBrush())
	{
		scb = GuiManager::getInstance().getSolidWhiteBrush();
	}
	for (auto it = childs.begin(); it != childs.end(); it++){
		(*it)->draw(renderTarget, rect, scb);
		count++;
	}
	hitRect = compRect;
	hitRect.bottom = rect.top;
	rect.bottom = rect.top;
	GuiLabel::decTabCount();

}

void GuiTimelineComponent::processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	POINT p = ResourceManager::getInstance().getMouseCoords(lParam);
	if (uMsg == WM_LBUTTONDOWN) {
		GuiManager::getInstance().setGuiInfoComponent(component);
	}
	for (auto it = properties.begin(); it != properties.end(); it++)
	{
		auto rect = (*it)->getRect();
		if (GuiManager::isPointInsideRect(p, rect)){
			(*it)->processMessage(hWnd, uMsg, wParam, lParam);
            GuiManager::getInstance().setCurrentProperty((*it)->getProperty());
            GuiManager::getInstance().setOverProperty(true);
		}
	}
	for (auto it = childs.begin(); it != childs.end(); it++){
		if (GuiManager::isPointInsideRect(p, (*it)->getRect())){
			(*it)->processMessage(hWnd, uMsg, wParam, lParam);
		}
	}
}