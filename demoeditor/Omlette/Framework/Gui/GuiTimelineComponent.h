#ifndef INC_GUITIMELINECOMPONENT_H
#define INC_GUITIMELINECOMPONENT_H

#include <Omlette\Interfaces\IGuiElement.h>
#include <Omlette\Components\Component.h>
#include <Omlette\Framework\Gui\GuiTimelineProperty.h>

namespace Oml
{
class GuiTimelineComponent : public IGuiElement
{
public:
	typedef boost::shared_ptr<GuiTimelineComponent> P;
	static P create(Component::P component) { return P(new GuiTimelineComponent(component)); }

	void				processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	void				draw(ID2D1RenderTarget* renderTarget);
	void				draw(ID2D1RenderTarget* renderTarget, D2D1_RECT_F& rect, ID2D1SolidColorBrush* bgBrush);
private:
						GuiTimelineComponent(Component::P component);
	Component::P		component;

	std::vector<GuiTimelineComponent::P> childs;
	std::vector<GuiTimelineProperty::P> properties;
};
}

#endif