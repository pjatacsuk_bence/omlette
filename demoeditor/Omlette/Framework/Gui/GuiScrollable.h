#ifndef INC_GUISCROLLABLE_H
#define INC_GUISCROLLABLE_H

#include <D2D1.h>
#include "../../Math/math.h"
namespace Oml{
class GuiScrollable {
public:	
	static const int VERTICAL_SCROLLBOX_WIDTH = 10;
	static const int VERTICAL_SCROLLBOX_HEIGHT = 25;

			GuiScrollable(D2D1_RECT_F rect);
			GuiScrollable();

	void	beginScrollable(ID2D1RenderTarget* renderTarget);
	void	endScrollable(ID2D1RenderTarget* renderTarget);
	void	processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	inline	void	setScrollableRect(D2D1_RECT_F rect){ scrollableRect = rect; }
	inline	D2D1_RECT_F	getScrollableRect(){ return scrollableRect; }

	inline	void	setMaxScrollAmountY(float y){ maxScrollAmountY = y - (scrollableRect.bottom - scrollableRect.top) ; }
	
	inline	D2D1_RECT_F	getDrawTargetRect(){ 
		D2D1_RECT_F rect = scrollableRect; 
		rect.top = rect.top - scrollAmountY; 
		return rect; 
	}

	inline	Egg::Math::float1 getScrollAmountY(){ return scrollAmountY; }
private:
	D2D1_RECT_F scrollableRect;
	ID2D1Layer* layer;
		
	Egg::Math::float1	scrollAmountY;
	Egg::Math::float1	maxScrollAmountY;
			

};
}



#endif