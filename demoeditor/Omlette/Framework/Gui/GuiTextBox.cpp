#include "DXUT.h"
#include <Omlette\Framework\Gui\GuiTextBox.h>
#include <Omlette\Framework\Gui\GuiManager.h>

Oml::GuiTextBox::GuiTextBox(D2D1_RECT_F rect, InputType inputType)
{
	this->hitRect = rect;
	this->inputType = inputType;
	maxLength = 50;
	border = 2;
}

Oml::IGuiElement::P Oml::GuiTextBox::create(D2D1_RECT_F rect, InputType inputType)
{
	return boost::shared_ptr<GuiTextBox>(new GuiTextBox(rect,inputType));
}

void Oml::GuiTextBox::processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if(uMsg == WM_CHAR)
	{

		char c = (char)wParam;
		if(wParam == 0x08)
		{
			//BACKSPACE
			if(text.length() != 0)
			{
				text.pop_back();
			}
		}
		else if(text.length() <= maxLength)
		{
			if(inputType == InputType::CHAR_ONLY)
			{
				if((c >= 'a' && c <= 'z') ||
					(c >= 'A' && c <= 'Z') ||
					(c == '\\') || (c=='/') ||
					(c == '.') || (c==':'))
				{
					text += c;	
				}
			} else if(inputType == InputType::NUMBER_ONLY)
			{
				if((c>= '0' && c<='9') || c == '.' || c == '-')		
				{
					text += c;
				}
			} else
			{
				text += c;
			}
		}
	}

}

void Oml::GuiTextBox::draw(ID2D1RenderTarget* renderTarget, D2D1_RECT_F rect)
{
		D2D1_RECT_F whiteRect;
		whiteRect.left = rect.left+border;
		whiteRect.right = rect.right-border;
		whiteRect.top = rect.top+border;
		whiteRect.bottom = rect.bottom-border;
		renderTarget->FillRectangle(&rect,GuiManager::getInstance().getSolidGrayBrush());
		renderTarget->FillRectangle(&whiteRect,GuiManager::getInstance().getSolidWhiteBrush());

		std::wstring wText;
		wText.assign(text.begin(),
					 text.end());  
		renderTarget->DrawTextW(wText.c_str(),
								wText.length(),
								GuiManager::getInstance().getTextFormat(),
								rect,
								GuiManager::getInstance().getBlackBrush());
}
void Oml::GuiTextBox::draw(ID2D1RenderTarget* renderTarget)
{
	draw(renderTarget, hitRect);	
}
std::string Oml::GuiTextBox::getText()
{
	return text;
}