#include "DXUT.h"
#include <Omlette\Framework\Gui\GuiTimelineKeyFrame.h>
#include <Omlette\Framework\Gui\GuiManager.h>

using namespace Oml;

template<class T>
GuiTimelineKeyFrame<T>::GuiTimelineKeyFrame(typename KeyFrame<T>* keyFrame, typename Property<T>::P property)
{
	this->property = property;
	this->keyFrame = keyFrame;
}

template<class T>
void GuiTimelineKeyFrame<T>::draw(ID2D1RenderTarget* renderTarget)
{

}
template<class T>
void GuiTimelineKeyFrame<T>::draw(ID2D1RenderTarget* renderTarget, D2D1_RECT_F& rect)
{

	using namespace Egg::Math;
	GuiTimeline::P guiTimeline = GuiManager::getInstance().getGuiTimeline();

	float1 minTime = guiTimeline->getMinTime();
	float1 maxTime = guiTimeline->getMaxTime();

	float1 keyFrameTime = keyFrame->time;
	float1 difTime = maxTime - minTime;

	if (keyFrameTime > maxTime || keyFrameTime < minTime)
	{
		return;
	}
	float1 DTime = (keyFrameTime-minTime) / difTime;
	float1 posX = DTime.lerp(rect.left,rect.right);

	D2D1_RECT_F keyFrameRect = rect;
	keyFrameRect.left = (float)posX - (float)(SIZE_OF_KEYFRAME / 2.0f);
	keyFrameRect.right = (float)posX + (float)(SIZE_OF_KEYFRAME / 2.0f);
	hitRect = keyFrameRect;

	renderTarget->FillRectangle(keyFrameRect, getKeyFrameColorBrush());
}

template<class T>
ID2D1SolidColorBrush* GuiTimelineKeyFrame<T>::getKeyFrameColorBrush() {
	return GuiManager::getInstance().getSolidRedBrush();
}
template<class T>
void GuiTimelineKeyFrame<T>::processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    GuiManager::getInstance().setOverKeyFrame(true);
	if (uMsg == WM_LBUTTONDBLCLK)
	{
		if (wParam & MK_SHIFT)
		{
			property->deleteKeyFrame(keyFrame);
			dirty = true;
		}
	}
	else if (uMsg == WM_LBUTTONDOWN){
		D2D1_RECT_F rect;			
		rect.top = 0;
		rect.bottom = 100;
		rect.left = 300;
		rect.right = 400;
		Oml::Converter conv = NO_CONVERT;
		if (property->getName().compare("color") == 0){
			conv = FROM_RGB_TO_COLOR;
		}
		GuiManager::getInstance().setGuiKeyFrameEditors<T>(rect, keyFrame, conv);
	}
}


template class GuiTimelineKeyFrame<int>;
template class GuiTimelineKeyFrame<float>;
template class GuiTimelineKeyFrame<class Egg::Math::float1>;
template class GuiTimelineKeyFrame<class Egg::Math::float3>;
template class GuiTimelineKeyFrame<class Egg::Math::float4>;
template class GuiTimelineKeyFrame<std::string>;