#ifndef INC_TEXTURESURFACE_H
#define INC_TEXTURESURFACE_H
#include <D3D11.h>

namespace Oml
{
class TextureSurface : public Awesomium::Surface
{
public:
	class TextureSurfaceFactory : public Awesomium::SurfaceFactory
	{
		Awesomium::Surface * 	CreateSurface(Awesomium::WebView *view, int width, int height)
		{
			return new TextureSurface(width, height);
		}
		 
		void 	DestroySurface(Awesomium::Surface *surface)
		{
			delete surface;
		}
	};
	TextureSurface(int width, int height);
	~TextureSurface();
	virtual void 	Paint(unsigned char *src_buffer, int src_row_span, const Awesomium::Rect &src_rect, const Awesomium::Rect &dest_rect);
	virtual void 	Scroll(int dx, int dy, const Awesomium::Rect &clip_rect);

	bool	inline	isDirty(){ return dirty; }
	void	inline	setDirty(bool d){ dirty = d; }
	ID3D11ShaderResourceView* getSrv();
	
private:
	ID3D11Texture2D*		texture;
	ID3D11ShaderResourceView* srv;
	bool					dirty;
	char*			buffer;
};
}


#endif