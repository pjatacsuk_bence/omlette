#include "DXUT.h"
#include <Omlette\Framework\Gui\GuiManager.h>
#include <Omlette\Framework\Utility\ResourceManager.h>
#include <Omlette\Framework\Utility\PickableManager.h>
#include <Omlette\Framework\Utility\Caster.h>
#include <Omlette\Framework\Gui\GuiTextBox.h>
#include <omlette\Framework\Gui\GuiTextBoxKeyFrame.h>
#include <Omlette\Components\ComponentFactory.h>
#include <Omlette\Framework\Gui\GuiElementsManager.h>
#include <Omlette\Framework\Gui\GuiLabel.h>
#include <omp.h>
#include <Omlette\Interfaces\IDrawable.h>
#include <boost\make_shared.hpp>
using namespace Oml;

GuiManager::GuiManager()
{
	guiElementsManager = GuiElementsManager::create();
	D2D1_RECT_F containerRect;
	containerRect.top = 0;
	containerRect.bottom = 300;
	containerRect.left = 0;
	containerRect.right = 300;
	GuiContainer::P  container = GuiContainer::create(containerRect);
	entityInfo = EntityInfo::create();
	container->addGuiElement(entityInfo);
	guiElementsManager->addGuiContainer(container);

	D2D1_RECT_F rect;
	rect.left = 130;
	rect.right = ResourceManager::getInstance().getScreenWidth();
	rect.top = 0;
	rect.bottom = ResourceManager::getInstance().getScreenHeight();

	transformMesh = TransformMesh::create();


	D2D1_RECT_F guiInfoComponentRect = rect;
	guiInfoComponentRect.left = guiInfoComponentRect.right - 130;
	guiInfoComponentRect.bottom = guiInfoComponentRect.top + 100;
	guiInfoComponent = GuiInfoComponent::create();
	guiInfoComponent->init(guiInfoComponentRect);

	GuiContainer::P gicContainer = GuiContainer::create(guiInfoComponentRect);
	
	gicContainer->addGuiElement(guiInfoComponent);
	guiElementsManager->addGuiContainer(gicContainer);

	GuiContainer::P gcContainer = GuiContainer::create(D2D1::RectF(0,600,
														ResourceManager::getInstance().getScreenWidth(),
														ResourceManager::getInstance().getScreenHeight()));
	guiElementsManager->addGuiContainer(gcContainer);




}
GuiManager::~GuiManager()
{
	releaseSwapChainResources();
}

void GuiManager::processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	guiElementsManager->processMessage(hWnd, uMsg, wParam, lParam);
	EntityManipulation::getInstance().processMessage(hWnd, uMsg, wParam, lParam);
}

void GuiManager::init(ID3D11Device* device,ID3DX11Effect* effect)
{
	static const WCHAR msc_fontName[] = L"Verdana";
    static const float msc_fontSize = 10;
	D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED,&D2DFactory);
	DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED,__uuidof(writeFactory),reinterpret_cast<IUnknown**>(&writeFactory));
	writeFactory->CreateTextFormat(msc_fontName,
									NULL,
									DWRITE_FONT_WEIGHT_NORMAL,
									DWRITE_FONT_STYLE_NORMAL,
									DWRITE_FONT_STRETCH_NORMAL,
									msc_fontSize,
									L"",
									&textFormat);

	textFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);
	textFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);
	



	D3D11_TEXTURE2D_DESC desc;
	desc.ArraySize = 1;
	desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags = 0;

	desc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	desc.Height = ResourceManager::getInstance().getScreenHeight();
	desc.Width = ResourceManager::getInstance().getScreenWidth();
	desc.MipLevels = 1;
	desc.MiscFlags = 0;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Usage = D3D11_USAGE_DEFAULT;

	HRESULT hr ;
	hr = device->CreateTexture2D(&desc,NULL,&guiTexture);

	IDXGISurface* surface = NULL;
	hr = 	guiTexture->QueryInterface(&surface);
	
	D2D1_RENDER_TARGET_PROPERTIES props =
		D2D1::RenderTargetProperties(	D2D1_RENDER_TARGET_TYPE_DEFAULT,
										D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN,D2D1_ALPHA_MODE_PREMULTIPLIED),
										96,
										96);

	 hr = D2DFactory->CreateDxgiSurfaceRenderTarget(
				surface,
				&props,
				&renderTarget);
         hr = renderTarget->CreateSolidColorBrush(
             D2D1::ColorF(D2D1::ColorF::Red), &solidRedBrush);
         hr = renderTarget->CreateSolidColorBrush(
             D2D1::ColorF(D2D1::ColorF::Black), &solidBlackBrush);
        hr = renderTarget->CreateSolidColorBrush(
            D2D1::ColorF(0.8,0.8,0.1),
            &solidSofterGrayBrush
            );
	hr = renderTarget->CreateSolidColorBrush(
            D2D1::ColorF(D2D1::ColorF::Silver),
            &solidGrayBrush
            );
	hr = renderTarget->CreateSolidColorBrush(
            D2D1::ColorF(D2D1::ColorF::DimGray),
            &solidDarkGrayBrush
            );
	hr = renderTarget->CreateSolidColorBrush(
            D2D1::ColorF(0.08,0.08,0.08),
            &solidDarkerGrayBrush
            );
	hr = renderTarget->CreateSolidColorBrush(
            D2D1::ColorF(255.0/255.0,223.0/255.0,51.0/255.0),
            &solidOrangeBrush
            );

	hr = renderTarget->CreateSolidColorBrush(
            D2D1::ColorF(D2D1::ColorF::White),
            &solidWhiteBrush
            );

	hr = renderTarget->CreateSolidColorBrush(
            D2D1::ColorF(D2D1::ColorF::Green),
            &transparentGreenBrush
            );
	hr = renderTarget->CreateSolidColorBrush(
            D2D1::ColorF(D2D1::ColorF::RoyalBlue),
            &solidBluishBrush
            );
	
	ResourceManager::getInstance().setBrush(transparentGreenBrush, "transparentGreenBrush");


	//QuadMesh

	Mesh::IndexedMesh::P quad = Mesh::IndexedMesh::createQuad(device);
	ID3DX11EffectPass*	fullQuad = effect->GetTechniqueByName("fullQuad")->GetPassByName("fullQuad");
	Mesh::Material::P fullQuadMaterial = Mesh::Material::create(fullQuad,0);
	guiMesh = Mesh::UnpickableShadedMesh::create(ResourceManager::getInstance().getBinder()->bindMaterial(fullQuadMaterial,quad),
											     IDrawable::DrawableType::QUAD);

	D3D11_RENDER_TARGET_VIEW_DESC txtDesc;
	txtDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	txtDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	txtDesc.Texture2D.MipSlice = 0;
	device->CreateRenderTargetView(guiTexture,&txtDesc,&guiRtv);

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.Texture2D.MostDetailedMip = 0;
	device->CreateShaderResourceView(guiTexture,&srvDesc,&guiSrv);

	

}


void GuiManager::draw(ID3D11DeviceContext* context,ID3DX11Effect* effect)
{
	
	IDrawable::RenderParameters renderParameters;
	renderParameters.context = context;
	renderParameters.effect = effect;
	renderTarget->BeginDraw();
	/****** clipping ******/
	ID2D1PathGeometry* geometry = NULL;
	D2DFactory->CreatePathGeometry(&geometry);
	ID2D1GeometrySink* sink;
	geometry->Open(&sink);
	sink->SetFillMode(D2D1_FILL_MODE_WINDING);

	for (auto it = guiElementsManager->begin(); it != guiElementsManager->end(); it++)
	{
		if (*it == NULL) continue;
		auto rect = (*it)->getRect();
		sink->BeginFigure(D2D1::Point2F(rect.left, rect.top), D2D1_FIGURE_BEGIN_FILLED);
		D2D1_POINT_2F points[3] = {
			D2D1::Point2F(rect.right, rect.top),
			D2D1::Point2F(rect.right, rect.bottom),
			D2D1::Point2F(rect.left, rect.bottom)
		};
		sink->AddLines(points, 3);
		sink->EndFigure(D2D1_FIGURE_END_CLOSED);
	}
	
	sink->Close();
	sink->Release();
	/**********************/


	renderTarget->Clear();
	ID2D1Layer* layer = NULL;
	renderTarget->CreateLayer(NULL, &layer);
	renderTarget->PushLayer(D2D1::LayerParameters(D2D1::InfiniteRect(), geometry), layer);

	drawInfoFromPickedEntity(context, effect);
	guiElementsManager->draw(renderTarget);

	renderTarget->PopLayer();
	layer->Release();
	geometry->Release();
	renderTarget->EndDraw();

	effect->GetVariableByName("guiTexture")->AsShaderResource()->SetResource(guiSrv);
	guiMesh->draw(renderParameters);

}

Egg::Math::float1 GuiManager::getTimeFromTimelinePoint(Egg::Math::int2 p)
{
	return	guiElementsManager->getGuiTimeline()->getTimeFromTimelinePoint(p);
}

void GuiManager::BindMethods()
{
}
void GuiManager::updateTimeline()
{
}
void GuiManager::update()
{
	double start = omp_get_wtime();

	updateTimeline();
	
	double end = omp_get_wtime();
	double dif = end - start;

	guiElementsManager->getGuiTimeline()->update();
	Entity::P pickedEntity = PickableManager::getInstance().getPickedEntity();
	bool visibility = true;
	if (pickedEntity == NULL)
	{
		visibility = false;
	}
	Oml::Component::P mmsh = transformMesh->getComponentManager()->getComponentsByType(MultiMeshRendererComponent::getTypeName())[0];
	auto drawableComps = mmsh->getComponentManager()->getDrawableComponents();
	auto it = drawableComps.begin();
	while (it != drawableComps.end())
	{
		(*it)->setVisible(visibility);
		it++;
	}
	entityInfo->setEntity(pickedEntity);
	currentProperties = guiElementsManager->getGuiTimeline()->getCurrentProperties();
	if(currentProperties.size() == 0)
	{
		return;
	}
	auto i = currentProperties.begin();
	auto e = currentProperties.end();

	/*while(i != e)
	{
			D2D1_RECT_F rect;			
			rect.top = 0;
			rect.bottom = 100;
			rect.left = 300;
			rect.right = 400;
			if((*i)->getType() == Oml::Type::FLOAT)
			{
				setTextBoxKeyFrames<Egg::Math::float1>((*i),rect);
				guiElementsManager->setKeyFrameEditors(guiKeyFrameEditors[0],guiKeyFrameEditors[1]);
			}
			if((*i)->getType() == Oml::Type::FLOAT3)
			{
				setTextBoxKeyFrames<Egg::Math::float3>((*i),rect);
				guiElementsManager->setKeyFrameEditors(guiKeyFrameEditors[0],guiKeyFrameEditors[1]);
			}
			if((*i)->getType() == Oml::Type::FLOAT4)
			{
				setTextBoxKeyFrames<Egg::Math::float4>((*i),rect);
				guiElementsManager->setKeyFrameEditors(guiKeyFrameEditors[0],guiKeyFrameEditors[1]);
			}
			if((*i)->getType() == Oml::Type::STRING)
			{
				setTextBoxKeyFrames<std::string>((*i),rect);
				guiElementsManager->setKeyFrameEditors(guiKeyFrameEditors[0],guiKeyFrameEditors[1]);
			}
			i++;
	}*/
    Component::P currentComponent = guiInfoComponent->getComponent(); //guiElementsManager->getGuiTimeline()->getCurrentComponent();
	if (currentComponent != NULL)
	{
		if (currentComponent->getComponentTypeName().compare("TransformComponent") == 0)
		{
			Entity::P pickedEntity = PickableManager::getInstance().getPickedEntity();
			if (pickedEntity != NULL)
			{
				TransformComponent::P ptc = Caster::getInstance().getComponentCasted<TransformComponent::P>(currentComponent);
				if (ptc != transformMesh->getTransformComponent())
				{
					transformMesh->setTransformComponent(ptc);
				}
			}
		}
	}
}

void GuiManager::drawInfoFromPickedEntity(ID3D11DeviceContext* context, ID3DX11Effect* effect)
{
	Component::P currentComponent = guiInfoComponent->getComponent(); //guiElementsManager->getGuiTimeline()->getCurrentComponent();
	if (currentComponent != NULL)
	{
		if (currentComponent->getComponentTypeName().compare("TransformComponent") == 0)
		{
			Entity::P pickedEntity = PickableManager::getInstance().getPickedEntity();
			if (pickedEntity != NULL)
			{
				IDrawable::RenderParameters rp;
				rp.context = context;
				rp.effect = effect;
				rp.mien = &ResourceManager::getInstance().getMien("ambient");
				transformMesh->drawGizmo(rp);
			}
		}
	}
		
}



void	GuiManager::createSwapChainResources()
{
	static const WCHAR msc_fontName[] = L"Verdana";
    static const float msc_fontSize =10;
	D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED,&D2DFactory);
	DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED,__uuidof(writeFactory),reinterpret_cast<IUnknown**>(&writeFactory));
	
	writeFactory->CreateTextFormat(msc_fontName,
									NULL,
									DWRITE_FONT_WEIGHT_NORMAL,
									DWRITE_FONT_STYLE_NORMAL,
									DWRITE_FONT_STRETCH_NORMAL,
									msc_fontSize,
									L"",
									&textFormat);

	textFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_CENTER);
	textFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_CENTER);
	
	D3D11_TEXTURE2D_DESC desc;
	desc.ArraySize = 1;
	desc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags = 0;
	desc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	desc.Height = ResourceManager::getInstance().getScreenHeight();
	desc.Width = ResourceManager::getInstance().getScreenWidth();
	desc.MipLevels = 1;
	desc.MiscFlags = 0;
	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Usage = D3D11_USAGE_DEFAULT;

	HRESULT hr ;
	hr = ResourceManager::getInstance().getDevice()->CreateTexture2D(&desc,NULL,&guiTexture);

	IDXGISurface* surface = NULL;
	hr = 	guiTexture->QueryInterface(&surface);
	
	D2D1_RENDER_TARGET_PROPERTIES props =
		D2D1::RenderTargetProperties(	D2D1_RENDER_TARGET_TYPE_DEFAULT,
										D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN,D2D1_ALPHA_MODE_PREMULTIPLIED),
										96,
										96);

	 hr = D2DFactory->CreateDxgiSurfaceRenderTarget(
				surface,
				&props,
				&renderTarget);
	hr = renderTarget->CreateSolidColorBrush(
            D2D1::ColorF(D2D1::ColorF::Red),
            &solidRedBrush
            );
	hr = renderTarget->CreateSolidColorBrush(
            D2D1::ColorF(D2D1::ColorF::Black),
            &solidBlackBrush
            );
	hr = renderTarget->CreateSolidColorBrush(
            D2D1::ColorF(0.8,0.8,0.8),
            &solidSofterGrayBrush
            );
	hr = renderTarget->CreateSolidColorBrush(
            D2D1::ColorF(0.5,0.5,0.5),
            &solidGrayBrush
            );
	hr = renderTarget->CreateSolidColorBrush(
            D2D1::ColorF(0.2,0.2,0.2),
            &solidDarkGrayBrush
            );
	hr = renderTarget->CreateSolidColorBrush(
            D2D1::ColorF(0.08,0.08,0.08),
            &solidDarkerGrayBrush
            );
	hr = renderTarget->CreateSolidColorBrush(
            D2D1::ColorF(255.0/255.0,223.0/255.0,51.0/255.0),
            &solidOrangeBrush
            );

	hr = renderTarget->CreateSolidColorBrush(
            D2D1::ColorF(D2D1::ColorF::White),
            &solidWhiteBrush
            );

	hr = renderTarget->CreateSolidColorBrush(
            D2D1::ColorF(D2D1::ColorF::Green),
            &transparentGreenBrush
            );
	hr = renderTarget->CreateSolidColorBrush(
            D2D1::ColorF(D2D1::ColorF::RoyalBlue),
            &solidBluishBrush
            );
	ResourceManager::getInstance().setBrush(transparentGreenBrush, "transparentGreenBrush");

	D3D11_RENDER_TARGET_VIEW_DESC txtDesc;
	txtDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	txtDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	txtDesc.Texture2D.MipSlice = 0;
	ResourceManager::getInstance().getDevice()->CreateRenderTargetView(guiTexture,&txtDesc,&guiRtv);

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
	srvDesc.Texture2D.MipLevels = 1;
	srvDesc.Texture2D.MostDetailedMip = 0;
	ResourceManager::getInstance().getDevice()->CreateShaderResourceView(guiTexture,&srvDesc,&guiSrv);

	D2D1_RECT_F rect;
	guiElementsManager->getGuiTimeline()->setRectBottomRight(Egg::Math::int2(ResourceManager::getInstance().getScreenWidth(),
																				ResourceManager::getInstance().getScreenHeight()));


	D3D11_TEXTURE2D_DESC adesc;
	adesc.Width = ResourceManager::getInstance().getScreenWidth();
	adesc.Height = ResourceManager::getInstance().getScreenHeight();
	adesc.MipLevels =1;
	adesc.ArraySize = 1;
	adesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	adesc.SampleDesc.Count = 1;
	adesc.SampleDesc.Quality = 0.9;
	adesc.Usage = D3D11_USAGE_DYNAMIC;
	adesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	adesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	adesc.MiscFlags = 0;

	hr = ResourceManager::getInstance().getDevice()->CreateTexture2D(&adesc,NULL,&awsTexture);

	D3D11_SHADER_RESOURCE_VIEW_DESC	pickTextureSrvDesc;
	pickTextureSrvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	pickTextureSrvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	pickTextureSrvDesc.Texture2D.MipLevels = 1;
	pickTextureSrvDesc.Texture2D.MostDetailedMip = 0;
	ResourceManager::getInstance().getDevice()->CreateShaderResourceView(awsTexture, &pickTextureSrvDesc, &awsSrv);

	 

}
void	GuiManager::releaseSwapChainResources()
{
	guiTexture->Release();
	guiRtv->Release();
	guiSrv->Release();
	renderTarget->Release();
	writeFactory->Release();
	textFormat->Release();
	D2DFactory->Release();
	solidRedBrush->Release();
	solidSofterGrayBrush->Release();
	solidGrayBrush->Release();
	solidBlackBrush->Release();
	solidWhiteBrush->Release();
	solidBluishBrush->Release();
	solidDarkerGrayBrush->Release();
	transparentGreenBrush->Release();

}