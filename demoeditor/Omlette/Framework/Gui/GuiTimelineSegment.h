#ifndef INC_GUITIMELINESEGMENT_H
#define INC_GUITIMELINESEGMENT_H

#include <Omlette\Interfaces\IGuiElement.h>
#include <Omlette\Framework\Segments\Segment.h>
#include <Omlette\Framework\Gui\GuiTimelineKeyFrame.h>
#include <Omlette\Framework\Properties\Property.h>
#include "boost\shared_ptr.hpp"
namespace Oml
{
template<class T>
class GuiTimelineSegment : public IGuiElement
{
public:
	typedef boost::shared_ptr<GuiTimelineSegment> P;
	static P create(typename Segment<T>::P segment, typename Property<T>::P property) { return P(new GuiTimelineSegment(segment, property)); }

	void	draw(ID2D1RenderTarget* renderTarget);
	void	draw(ID2D1RenderTarget* renderTarget, D2D1_RECT_F& rect);
	void	processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	
	ID2D1SolidColorBrush* getSegmentColorBrush();
private:
	GuiTimelineSegment(typename Segment<T>::P segment, typename Property<T>::P property);
	typename Segment<T>::P segment;
	typename Property<T>::P property;
	typename GuiTimelineKeyFrame<T>::P guiTimelineKeyFrames[2];

};
}


#endif 