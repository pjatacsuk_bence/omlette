#ifndef INC_GUITIMELINEKEYFRAME_H
#define INC_GUITIMELINEKEYFRAME_H
#include <Omlette\Interfaces\IGuiElement.h>
#include <Omlette\Framework\Segments\Segment.h>
#include <Omlette\Framework\Properties\Property.h>
namespace Oml
{

template<class T>
class GuiTimelineKeyFrame : public IGuiElement
{
public:
	static const int SIZE_OF_KEYFRAME = 10;
	typedef boost::shared_ptr<GuiTimelineKeyFrame> P;
	static P create(typename KeyFrame<T>* keyFrame, typename Property<T>::P property) { return P(new GuiTimelineKeyFrame(keyFrame,property)); }

	void	draw(ID2D1RenderTarget* renderTarget);
	void	draw(ID2D1RenderTarget* renderTarget, D2D1_RECT_F& rect);
	void	processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	
	ID2D1SolidColorBrush* getKeyFrameColorBrush();
private:
	GuiTimelineKeyFrame(typename KeyFrame<T>* keyFrame, typename Property<T>::P property);
	typename KeyFrame<T>* keyFrame;
	typename Property<T>::P property;
};
}

#endif //HEADER