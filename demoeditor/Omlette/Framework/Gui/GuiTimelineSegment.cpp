#include "DXUT.h"
#include <Omlette\Framework\Gui\GuiTimelineSegment.h>
#include <Omlette\Framework\Gui\GuiManager.h>

using namespace Oml;

template<class T>
GuiTimelineSegment<T>::GuiTimelineSegment(typename Segment<T>::P segment, typename Property<T>::P property)
{
	this->segment = segment;
	this->property = property;
	guiTimelineKeyFrames[0] = GuiTimelineKeyFrame<T>::create(segment->getFirstKeyFrame(), property);
	guiTimelineKeyFrames[1] = GuiTimelineKeyFrame<T>::create(segment->getSecondKeyFrame(), property);
}

template<class T>
void GuiTimelineSegment<T>::draw(ID2D1RenderTarget* renderTarget)
{

}
template<class T>
void GuiTimelineSegment<T>::draw(ID2D1RenderTarget* renderTarget, D2D1_RECT_F& rect)
{
	GuiTimeline::P guiTimeline = GuiManager::getInstance().getGuiTimeline();

	Egg::Math::float1 minTime = guiTimeline->getMinTime();
	Egg::Math::float1 maxTime = guiTimeline->getMaxTime();

	Egg::Math::float1 firstKeyTime = segment->getFirstKeyFrame()->time;
	Egg::Math::float1 secondKeyTime = segment->getSecondKeyFrame()->time;

	using namespace Egg::Math;
	if (firstKeyTime > maxTime)
	{
		return;
	}
	float1 leftKeyFrameTime = firstKeyTime < minTime ? minTime : firstKeyTime;
	float1 rightKeyFrameTime = secondKeyTime > maxTime ? maxTime : secondKeyTime;
	float1 difTime = maxTime - minTime;

	float1 leftDTime = (leftKeyFrameTime-minTime) / difTime;
	float1 leftPosX = leftDTime.lerp(rect.left,rect.right);

	float1 rightDTime = (rightKeyFrameTime - minTime) / difTime;
	float1 rightPosX = rightDTime.lerp(rect.left,rect.right);
	
	D2D1_RECT_F segmentRect = rect;
	segmentRect.left = leftPosX;
	segmentRect.right = rightPosX;

	hitRect = segmentRect;

	renderTarget->FillRectangle(segmentRect, getSegmentColorBrush());

	guiTimelineKeyFrames[0]->draw(renderTarget, rect);
	guiTimelineKeyFrames[1]->draw(renderTarget, rect);
}

template<class T>
ID2D1SolidColorBrush* GuiTimelineSegment<T>::getSegmentColorBrush() {
	return GuiManager::getInstance().getSolidOrangeBrush();
}
template<class T>
void GuiTimelineSegment<T>::processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	POINT p = ResourceManager::getInstance().getMouseCoords(lParam);
	if (GuiManager::isPointInsideRect(p, guiTimelineKeyFrames[0]->getRect())) {
		guiTimelineKeyFrames[0]->processMessage(hWnd, uMsg, wParam, lParam);
	}
	else if (GuiManager::isPointInsideRect(p, guiTimelineKeyFrames[1]->getRect())) {
		guiTimelineKeyFrames[1]->processMessage(hWnd, uMsg, wParam, lParam);
	}
	else {
		
	}
	if (guiTimelineKeyFrames[0]->isDirty() || guiTimelineKeyFrames[1]->isDirty()){
		dirty = true;
	}
	
}

template class GuiTimelineSegment<int>;
template class GuiTimelineSegment<float>;
template class GuiTimelineSegment<class Egg::Math::float1>;
template class GuiTimelineSegment<class Egg::Math::float3>;
template class GuiTimelineSegment<class Egg::Math::float4>;
template class GuiTimelineSegment<std::string>;