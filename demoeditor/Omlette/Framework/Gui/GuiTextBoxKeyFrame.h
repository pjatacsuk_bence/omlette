#ifndef INC_GUITEXTBOXKEYFRAME_H
#define INC_GUITEXTBOXKEYFRAME_H
#include <Omlette\Interfaces\IGuiElement.h>
#include <Omlette\Framework\Gui\GuiTextBoxKeyFrameValue.hpp>
#include <string>
#include <Omlette\Framework\Segments\Segment.h>
#include <Omlette\Framework\Gui\GuiContainer.h>

namespace Oml
{
template<class T>
class GuiTextBoxKeyFrame : public GuiContainer
{
public:
	typedef boost::shared_ptr<GuiTextBoxKeyFrame<T>> P;
	static	P	create(D2D1_RECT_F rect, KeyFrame<T>* keyFrame, Oml::Converter conv = Oml::Converter::NO_CONVERT);

protected:
	GuiTextBoxKeyFrame(D2D1_RECT_F rect, KeyFrame<T>* keyFrame, Oml::Converter conv = Oml::Converter::NO_CONVERT);
	KeyFrame<T>*					keyFrame;

};
}

#endif