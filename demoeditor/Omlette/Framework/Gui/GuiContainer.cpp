#include "DXUT.h"
#include <Omlette\Framework\Gui\GuiContainer.h>
#include <Omlette\Framework\Gui\GuiManager.h>

using namespace Oml;

void GuiContainer::addGuiElement(IGuiElement::P guiElement)
{
	D2D1_RECT_F rect = guiElement->getRect();
	float eheight = rect.bottom - rect.top;
	float ewidth = rect.right - rect.left;
	if(ewidth > (hitRect.right - hitRect.left))
	{
		hitRect.right += abs((hitRect.right - hitRect.left)-ewidth);
	}
	D2D1_RECT_F elementsNewRect;
	elementsNewRect.top = hitRect.bottom;
	elementsNewRect.left = hitRect.left;
	elementsNewRect.right = hitRect.left + ewidth;
	elementsNewRect.bottom = hitRect.bottom + eheight;
	guiElement->setRect(elementsNewRect);
	guiElements.push_back(guiElement);

	hitRect.bottom = hitRect.bottom + eheight;
}

IGuiElement::P GuiContainer::getGuiElement(POINT p)
{
	iterator i = guiElements.begin();
	iterator e = guiElements.end();
	while(i != e)
	{
		D2D1_RECT_F rect = (*i)->getRect();
		if(GuiManager::isPointInsideRect(p,rect))
		{
			return (*i);
		}
		i++;
	}
	return NULL;
}

GuiContainer::GuiContainer(D2D1_RECT_F rect)
{
	this->hitRect = rect;
}
GuiContainer::P GuiContainer::create(D2D1_RECT_F rect)
{
	return P(new GuiContainer(rect));
}

void GuiContainer::draw(ID2D1RenderTarget* renderTarget)
{
//	renderTarget->FillRectangle(&hitRect,GuiManager::getInstance().getSolidGrayBrush());
	iterator i = guiElements.begin();
	iterator e = guiElements.end();
	while(i != e)
	{
		(*i)->draw(renderTarget);
		i++;
	}

}

GuiContainer::~GuiContainer()
{
	guiElements.clear();
}

