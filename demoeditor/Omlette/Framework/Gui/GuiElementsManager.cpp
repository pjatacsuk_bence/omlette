#include "DXUT.h"

#include <Omlette\Framework\Gui\GuiTextBox.h>
#include <Omlette\Framework\Gui\GuiElementsManager.h>
#include <Omlette\Framework\Gui\GuiManager.h>
#include <Omlette\Framework\Gui\CommandManager.h>

using namespace Oml;


GuiElementsManager::GuiElementsManager()
{
	guiKeyFrameEditors[0] = NULL;
	addGuiContainer(guiKeyFrameEditors[0]);
	guiKeyFrameEditors[1] = NULL;
	addGuiContainer(guiKeyFrameEditors[1]);

	guiInfoKeyFrame = NULL;
	addGuiContainer(guiInfoKeyFrame);

	guiTimeline = GuiTimeline::create();
	GuiContainer::P timelineContainer = GuiContainer::create(guiTimeline->getRect());
	timelineContainer->addGuiElement(guiTimeline);

	addGuiContainer(timelineContainer);


}
void	GuiElementsManager::setKeyFrameEditors(GuiContainer::P first, GuiContainer::P second)
{
	guiContainers[0] = first;
	guiContainers[1] = second;
	guiKeyFrameEditors[0] = first;	
	guiKeyFrameEditors[1] = second;
}

GuiElementsManager::~GuiElementsManager()
{
	guiContainers.clear();
}

void	GuiElementsManager::addGuiContainer(GuiContainer::P guiElement)
{
	guiContainers.push_back(guiElement);
}
void	GuiElementsManager::remove(GuiContainer::P guiElement)
{
	iterator f = std::find(guiContainers.begin(),guiContainers.end(),guiElement);
	if(*f != NULL)
	guiContainers.erase(f);
}

void	GuiElementsManager::draw(ID2D1RenderTarget* renderTarget)
{
	iterator i = guiContainers.begin();
	iterator e = guiContainers.end();
	while(i != e)
	{
		if(*i != NULL)
		{
			(*i)->draw(renderTarget);
		}
		i++;
	}
	guiTimeline->draw(renderTarget);
}


void	GuiElementsManager::processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	//guiTimeline->processMessage(hWnd,uMsg,wParam,lParam);
	if(uMsg == WM_KEYDOWN)
	{
		if(wParam == VK_RETURN)
		{

			if(focusedGuiElement != NULL)
			{
				focusedGuiElement->execute();
			}
			focusedGuiElement = NULL;
			CommandManager::getInstance().setCommand(CommandManager::Command::None);
		}
	}
	iterator i = guiContainers.begin();
	iterator e = guiContainers.end();
	while(i != e)
	{
		if(uMsg == WM_LBUTTONDOWN || uMsg == WM_RBUTTONDOWN)
		{
			focusedGuiElement = NULL;
			POINT p = ResourceManager::getInstance().getMouseCoords(lParam);
			if(*i != NULL)
			{
				IGuiElement::P element = (*i)->getGuiElement(p);
				if(element != NULL)
				{
					focusedGuiElement = element;
					break;
				}
			}
		}
		i++;
	}
	if(focusedGuiElement != NULL)
	{
		if (CommandManager::getInstance().getCommand() != CommandManager::Command::DraggingTimeline &&
			CommandManager::getInstance().getCommand() != CommandManager::Command::DraggingScrollbar)
		{
			CommandManager::getInstance().setCommand(CommandManager::Command::HandlingFocusedGuiElement);
		}
		focusedGuiElement->processMessage(hWnd,uMsg,wParam,lParam);
	}
	else if (CommandManager::getInstance().getCommand() == CommandManager::Command::HandlingFocusedGuiElement)
	{
		CommandManager::getInstance().setCommand(CommandManager::Command::None);
	}


}

GuiTimeline::P GuiElementsManager::getGuiTimeline()
{
	return guiTimeline;	
}

IGuiElement::P	GuiElementsManager::getFocusedGuiElement()
{
	return focusedGuiElement;
}

IGuiElement::P	GuiElementsManager::getGuiElementUnderPoint(Egg::Math::int2 point)
{
	iterator i = guiContainers.begin();
	iterator e = guiContainers.end();
	while(i != e)
	{
			POINT p;
			p.x = point.x;
			p.y = point.y;
			if(*i != NULL)
			{
				IGuiElement::P element = (*i)->getGuiElement(p);
				if(element != NULL)
				{
					return element;
				}
			}
		i++;
	}
	return NULL;
}