#include "DXUT.h"
#include <Omlette\Framework\Gui\CommandManager.h>

using namespace Oml;

void CommandManager::setCommand(Command command)
{
	this->command = command;
}

CommandManager::Command CommandManager::getCommand()
{
	return command;
}