#include "DXUT.h"
#include <Omlette\Framework\Gui\GuiLabel.h>
#include <Omlette\Framework\Gui\GuiManager.h>

int Oml::GuiLabel::tabCount = 0;
Oml::GuiLabel::GuiLabel(D2D1_RECT_F rect, std::string text)
{
	this->hitRect = rect;
	this->text = text;
}

Oml::GuiLabel::P Oml::GuiLabel::create(D2D1_RECT_F rect, std::string text)
{
	return boost::shared_ptr<GuiLabel>(new GuiLabel(rect,text));
}

void Oml::GuiLabel::processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
}

void Oml::GuiLabel::draw(ID2D1RenderTarget* renderTarget)
{

		std::wstring wText;
		wText.assign(text.begin(),
					 text.end());  
		renderTarget->DrawTextW(wText.c_str(),
								wText.length(),
								GuiManager::getInstance().getTextFormat(),
								hitRect,
								GuiManager::getInstance().getBlackBrush());
}

std::string Oml::GuiLabel::getText()
{
	return text;
}

void Oml::GuiLabel::setText(std::string text)
{
	this->text = text;
}
void Oml::GuiLabel::draw(ID2D1RenderTarget* renderTarget, D2D1_RECT_F rect, std::string text, ID2D1SolidColorBrush* textBrush,ID2D1SolidColorBrush* backgroundBrush,ALIGN align)
{
		if(backgroundBrush != NULL)
		{
			renderTarget->FillRectangle(rect,backgroundBrush);
		}
		switch (align)
		{
		case CENTER:
			GuiManager::getInstance().getTextFormat()->SetTextAlignment(DWRITE_TEXT_ALIGNMENT::DWRITE_TEXT_ALIGNMENT_CENTER);
			break;
		case LEFT:
			GuiManager::getInstance().getTextFormat()->SetTextAlignment(DWRITE_TEXT_ALIGNMENT::DWRITE_TEXT_ALIGNMENT_LEADING);
			break;
		case RIGHT:
			GuiManager::getInstance().getTextFormat()->SetTextAlignment(DWRITE_TEXT_ALIGNMENT::DWRITE_TEXT_ALIGNMENT_TRAILING);
			break;
		}
		if (textBrush == NULL)
		{
			textBrush = GuiManager::getInstance().getBlackBrush();
		}
		std::wstring wText;
		wText.assign(text.begin(),
					 text.end());  
		renderTarget->DrawTextW(wText.c_str(),
								wText.length(),
								GuiManager::getInstance().getTextFormat(),
								rect,
								textBrush);

}