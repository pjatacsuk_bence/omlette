#include "DXUT.h"
#include <Omlette\Framework\Gui\TransformMesh.h>
#include <Omlette\Framework\Utility\ResourceManager.h>
#include <Omlette\Entities\EntityFactory.h>
#include <Omlette\Components\SingleSolidColorMaterial.h>
#include <Omlette\Components\ComponentFactory.h>
#include <Omlette\Framework\Utility\Caster.h>
#include <Omlette\Cam\CameraManager.h>
#include <Omlette\Entities\EntityManager.h>

using namespace Oml;

TransformMesh::TransformMesh():
Entity("transformMesh")
{

	Oml::Component::P comp = ComponentFactory::getInstance().createTransformComponent("transformComponent");
	TransformComponent::P tc = Caster::getInstance().getComponentCasted<TransformComponent::P>(comp);
	transformComponent = tc;

	componentManager->addComponent(tc);

	multiMesh = MultiMeshRendererComponent::create("transformMesh", "",tc);

	std::string path = "./Resources/Meshes/TransformMesh.obj";
	
	std::vector<Mesh::Geometry::P> geometries;
	geometries = ResourceManager::getInstance().getGeometry(path);
	if (geometries.size() == 0)
	{
		ResourceManager::getInstance().loadGeometry(path);
		geometries = ResourceManager::getInstance().getGeometry(path);
	}
	SingleSolidColorMaterial::P scmRed = SingleSolidColorMaterial::create("scmRed",Egg::Math::float4(1,0,0,1));
	SingleSolidColorMaterial::P scmBlue = SingleSolidColorMaterial::create("scmBlue",Egg::Math::float4(0,0,1,1));
	SingleSolidColorMaterial::P scmGreen = SingleSolidColorMaterial::create("scmGreen",Egg::Math::float4(0,1,0,1));


	multiMesh->addMesh(geometries[0],0, scmRed,IPickable::SpecialID::transformY,path);
	multiMesh->addMesh(geometries[1],1, scmBlue,IPickable::SpecialID::transformZ,path);
	multiMesh->addMesh(geometries[2],2, scmGreen,IPickable::SpecialID::transformX,path);

	componentManager->addComponent(multiMesh);
	componentManager->addComponent(scmRed);
	componentManager->addComponent(scmBlue);
	componentManager->addComponent(scmGreen);
	EntityManager::getInstance().addEntity(P(this));

	
}

TransformMesh::~TransformMesh()
{
	multiMesh = NULL;
	transformComponent = NULL;
}
void	TransformMesh::draw(RenderParameters renderParameters)
{
}

void TransformMesh::update(float t)
{
	Cam::ICam::P currentCam = Cam::CameraManager::getInstance().getCurrentCam();
	Egg::Math::float3 camPos = currentCam->getEyePosition();
	Egg::Math::float3 dist = (transformComponent->getPositionValue(t) - camPos).length();

	std::vector<Oml::Component::P> meshComps = multiMesh->getComponentManager()->getComponentsByType(MeshRendererComponent::getTypeName());
	auto itMeshes = meshComps.begin();
	while (itMeshes != meshComps.end())
	{
		std::vector<Oml::Component::P> transComps = (*itMeshes)->getComponentManager()->getComponentsByType(TransformComponent::getTypeName());
		auto itTrans = transComps.begin();
		while (itTrans != transComps.end())
		{
			TransformComponent::P casted = Caster::getInstance().getComponentCasted<TransformComponent::P>(*itTrans);
			casted->setParentConstraints(TransformComponent::Constraint::NO_ROTATION | TransformComponent::Constraint::NO_SCALE);
			casted->setGlobalScale(dist / 10.0);
			itTrans++;
		}
		itMeshes++;
	}
		
	componentManager->update(t);
}

void	TransformMesh::drawGizmo(RenderParameters renderParameters)
{
	__super::draw(renderParameters);
}

void TransformMesh::setTransformComponent(TransformComponent::P tc)
{
	transformComponent->setParent(tc);
}
void TransformMesh::buildComponentDependencies()
{

}