#ifndef INC_ENTITYINFO_H
#define INC_ENTITYINFO_H
#include <Omlette\Entities\Entity.h>
#include <Omlette\Interfaces\IGuiElement.h>
#include <boost\shared_ptr.hpp>

namespace Oml
{
class EntityInfo : public IGuiElement
{

public:
	typedef boost::shared_ptr<EntityInfo> P;
	static inline P create(){ return P(new EntityInfo()); }
	struct ComponentRect
	{
		Component::P	comp;
		D2D1_RECT_F		rect;
	};

	void		setEntity(Entity::P entity);
	void		replaceComponent(Component::P component, std::wstring componentTypeName);
	void		replaceMesh(Component::P component);
	void		draw(ID2D1RenderTarget* renderTarget);
	void		processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
private:
								EntityInfo();
	Entity::P					entity;
	std::vector<ComponentRect>	componentRects;
	Component::P				getComponentUnderPoint(Egg::Math::int2 point);
	void						createComponentRightClickMenus(HWND hWnd, Component::P component, Egg::Math::int2 pos);
	void						createComponentSpecificRightClickMenus(HWND hWnd, Component::P component, HMENU hmenu);
	Component::P				rightClickedComponent;
};

}






#endif