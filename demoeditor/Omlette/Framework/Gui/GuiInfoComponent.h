#ifndef INC_GUIINFOCOMPONENT_H
#define INC_GUIINFOCOMPONENT_H

#include <Omlette\Interfaces\IGuiElement.h>
#include <Omlette\Components\Component.h>

namespace Oml{
class GuiInfoComponent : public IGuiElement{
public:

	typedef boost::shared_ptr<GuiInfoComponent> P;
	static P create(){ return P(new GuiInfoComponent()); }
	void	init(D2D1_RECT_F rect);
	void	processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	void	draw(ID2D1RenderTarget* renderTarget);

	inline void setComponent(Component::P comp){ component = comp; }
    inline Component::P getComponent(){ return component; }
private:
	Component::P	component;
	GuiInfoComponent();
	
};
}

#endif // !INC_GUIINFOCOMPONENT_H
