#ifndef INC_TRANSFORM_MESH
#define INC_TRANSFORM_MESH
#include <Omlette\Components\MultiMeshRendererComponent.h>
#include <Omlette\Interfaces\IDrawable.h>
#include <Omlette\Components\TransformComponent.h>
namespace Oml
{
class TransformMesh : public Entity 
{
public:
	typedef boost::shared_ptr<TransformMesh> P;
	inline	static P create() { return P(new TransformMesh()); }

	inline	TransformComponent::P	getTransformComponent(){ return transformComponent; }
									~TransformMesh();

	void	draw(RenderParameters renderParameters);
	void	update(float t);
	void	drawGizmo(RenderParameters renderParameters);
	void	buildComponentDependencies();
	void	setTransformComponent(TransformComponent::P tc);
	void	buildDependencies(){}
	void	updateDependencies(){}
private:
	TransformMesh();
	MultiMeshRendererComponent::P	multiMesh;
	TransformComponent::P			transformComponent;
};
}


#endif