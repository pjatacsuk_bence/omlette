#include "DXUT.h"
#include <Omlette\Framework\Gui\GuiTimelineBuilder.h>
#include <Omlette\Framework\Utility\PickableManager.h>
#include <Omlette\Framework\Time\TimeManager.h>
#include <Omlette\Entities\Entity.h>
#include <Omlette\Framework\Utility\Caster.h>



using namespace Oml;

GuiTimelineBuilder::GuiTimelineBuilder(int width):
width(width)
{
	//maxTime = TimeManager::getInstance().getMaxTime();
	maxTime = 6000;
	float3Color = "#03436A";
	float4Color = "#03436A";
	float1Color = "#FFA700";
	floatColor  = "#FFA700";
	stringColor = "#0969A3";
	intColor    = "#0969A3";

}

std::string GuiTimelineBuilder::buildTimeline()
{
	Entity::P entity = PickableManager::getInstance().getPickedEntity();
	if (entity == NULL)
	{
		return std::string("");
	}
	
	std::string timeline;
	std::stringstream ss;
	ss << width;
	std::vector<Oml::Component::P> components = entity->getComponents();
	auto it = components.begin();
	while (it != components.end())
	{
		timeline += getComponentDiv(*it);
		it++;
	}
	return timeline;
}

std::string		GuiTimelineBuilder::getComponentDiv(Oml::Component::P comp)
{
	std::string div = "<div id=\"" + comp->getName() + "\">";
	div += comp->getName() + "::" + comp->getComponentTypeName();
	div += "</div>";
	auto it = comp->getPropertyMap().begin();
	while (it != comp->getPropertyMap().end())
	{
		div += getPropertyDiv(it->second);
		it++;
	}
	return div;
}

std::string	GuiTimelineBuilder::getPropertyDiv(Oml::IPropertyAncestor::P p)
{
	Properties properties;
	std::string div;
	switch (p->getType())
	{
		case Type::FLOAT4:
			properties.pfloat4 = Caster::getInstance().getPropertyCasted<Property<Egg::Math::float4>::P>(p);
			break;
		case Type::FLOAT3:
			properties.pfloat3 = Caster::getInstance().getPropertyCasted<Property<Egg::Math::float3>::P>(p);
			break;
		case Type::FLOAT:
			properties.pfloat1 = Caster::getInstance().getPropertyCasted<Property<Egg::Math::float1>::P>(p);
			break;
		case Type::STRING:
			properties.pstring = Caster::getInstance().getPropertyCasted<Property<std::string>::P>(p);
			break;
		case Type::UINT32:
			properties.pint = Caster::getInstance().getPropertyCasted<Property<unsigned int>::P>(p);
			break;
	}
	if (properties.pfloat != NULL)
	{
		div += getFloatPropertyDiv(properties.pfloat);
	}
	if (properties.pfloat1 != NULL)
	{
		div += getFloat1PropertyDiv(properties.pfloat1);
	}
	if (properties.pfloat3 != NULL)
	{
		div += getFloat3PropertyDiv(properties.pfloat3);
	}
	if (properties.pfloat4 != NULL)
	{
		div += getFloat4PropertyDiv(properties.pfloat4);
	}
	if (properties.pstring != NULL)
	{
		div += getStringPropertyDiv(properties.pstring);
	}
	if (properties.pint != NULL)
	{
		div += getIntPropertyDiv(properties.pint);
	}
	return div;
	
}
std::string		GuiTimelineBuilder::getFloat4PropertyDiv(Property<Egg::Math::float4>::P p)
{

	
	std::string ret="";
	auto segments = p->getSegmentManager()->getSegments();
	auto it = segments.begin();
	while (it != segments.end())
	{

		using namespace Egg::Math;
		float1 timeBegin = (*it)->getFirstKeyFrame()->time;
		float1 timeEnd = (*it)->getSecondKeyFrame()->time;
		float1 dif = timeEnd - timeBegin;
		float1 l = dif / (float1)(maxTime);
		float1 swidth = l.lerp(0,width);
		l = timeBegin / (float1)(maxTime);
		float1 pos = l.lerp(0, width);
		std::stringstream style;
		style << "width: " << (float)(swidth) << ";";
		style << "position:relative; " << "left: " << pos << "px;";
		style << "background-color: " + float4Color + ";";
		std::string div = "<div id=\"" + p->getName() + "\" style=\"" + style.str() + "\">"  ;
		div += p->getName() + "::" + p->getTypeName();
		div += "</div>";
		ret += div;
		it++;
	}
	return ret;
}
std::string		GuiTimelineBuilder::getFloat3PropertyDiv(Property<Egg::Math::float3>::P p)
{
	std::string ret="";
	auto segments = p->getSegmentManager()->getSegments();
	auto it = segments.begin();
	while (it != segments.end())
	{

		using namespace Egg::Math;
		float1 timeBegin = (*it)->getFirstKeyFrame()->time;
		float1 timeEnd = (*it)->getSecondKeyFrame()->time;
		float1 dif = timeEnd - timeBegin;
		float1 l = dif / (float1)(maxTime);
		float1 swidth = l.lerp(0,width);
		l = timeBegin / (float1)(maxTime);
		float1 pos = l.lerp(0, width);
		std::stringstream style;
		style << "width: " << (float)(swidth) << ";";
		style << "position:relative; " << "left: " << pos << "px;";
		style << "background-color:" + float3Color + ";";
		std::string div = "<div id=\"" + p->getName() + "\" style=\"" + style.str() + "\">"  ;
		div += p->getName() + "::" + p->getTypeName();
		div += "</div>";
		ret += div;
		it++;
	}
	return ret;
}
std::string		GuiTimelineBuilder::getFloatPropertyDiv(Property<float>::P p)
{

	std::string ret="";
	auto segments = p->getSegmentManager()->getSegments();
	auto it = segments.begin();
	while (it != segments.end())
	{

		using namespace Egg::Math;
		float1 timeBegin = (*it)->getFirstKeyFrame()->time;
		float1 timeEnd = (*it)->getSecondKeyFrame()->time;
		float1 dif = timeEnd - timeBegin;
		float1 l = dif / (float1)(maxTime);
		float1 swidth = l.lerp(0,width);
		l = timeBegin / (float1)(maxTime);
		float1 pos = l.lerp(0, width);
		std::stringstream style;
		style << "width: " << (float)(swidth) << ";";
		style << "position:relative; " << "left: " << pos << "px;";
		style << "background-color: " + floatColor + ";";
		std::string div = "<div id=\"" + p->getName() + "\" style=\"" + style.str() + "\">"  ;
		div += p->getName() + "::" + p->getTypeName();
		div += "</div>";
		ret += div;
		it++;
	}
	return ret;
}
std::string		GuiTimelineBuilder::getFloat1PropertyDiv(Property<Egg::Math::float1>::P p)
{

	std::string ret="";
	auto segments = p->getSegmentManager()->getSegments();
	auto it = segments.begin();
	while (it != segments.end())
	{

		using namespace Egg::Math;
		float1 timeBegin = (*it)->getFirstKeyFrame()->time;
		float1 timeEnd = (*it)->getSecondKeyFrame()->time;
		float1 dif = timeEnd - timeBegin;
		float1 l = dif / (float1)(maxTime);
		float1 swidth = l.lerp(0,width);
		l = timeBegin / (float1)(maxTime);
		float1 pos = l.lerp(0, width);
		std::stringstream style;
		style << "width: " << (float)(swidth) << ";";
		style << "position:relative; " << "left: " << pos << "px;";
		style << "background-color: " + float1Color + ";";
		std::string div = "<div id=\"" + p->getName() + "\" style=\"" + style.str() + "\">"  ;
		div += p->getName() + "::" + p->getTypeName();
		div += "</div>";
		ret += div;
		it++;
	}
	return ret;
}
std::string		GuiTimelineBuilder::getStringPropertyDiv(Property<std::string>::P p)
{

	std::string ret="";
	auto segments = p->getSegmentManager()->getSegments();
	auto it = segments.begin();
	while (it != segments.end())
	{

		using namespace Egg::Math;
		float1 timeBegin = (*it)->getFirstKeyFrame()->time;
		float1 timeEnd = (*it)->getSecondKeyFrame()->time;
		float1 dif = timeEnd - timeBegin;
		float1 l = dif / (float1)(maxTime);
		float1 swidth = l.lerp(0,width);
		l = timeBegin / (float1)(maxTime);
		float1 pos = l.lerp(0, width);
		std::stringstream style;
		style << "width: " << (float)(swidth) << ";";
		style << "position:relative; " << "left: " << pos << "px;";
		style << "background-color: " + stringColor + ";";
		std::string div = "<div id=\"" + p->getName() + "\" style=\"" + style.str() + "\">"  ;
		div += p->getName() + "::" + p->getTypeName();
		div += "</div>";
		ret += div;
		it++;
	}
	return ret;
}
std::string		GuiTimelineBuilder::getIntPropertyDiv(Property<unsigned int>::P p)
{
	std::string ret="";
	auto segments = p->getSegmentManager()->getSegments();
	auto it = segments.begin();
	while (it != segments.end())
	{

		using namespace Egg::Math;
		float1 timeBegin = (*it)->getFirstKeyFrame()->time;
		float1 timeEnd = (*it)->getSecondKeyFrame()->time;
		float1 dif = timeEnd - timeBegin;
		float1 l = dif / (float1)(maxTime);
		float1 swidth = l.lerp(0,width);
		l = timeBegin / (float1)(maxTime);
		float1 pos = l.lerp(0, width);
		std::stringstream style;
		style << "width: " << (float)(swidth) << ";";
		style << "position:relative; " << "left: " << pos << "px;";
		style << "background-color: " + intColor + ";";
		std::string div = "<div id=\"" + p->getName() + "\" style=\"" + style.str() + "\">"  ;
		div += p->getName() + "::" + p->getTypeName();
		div += "</div>";
		ret += div;
		it++;
	}
	return ret;
}

