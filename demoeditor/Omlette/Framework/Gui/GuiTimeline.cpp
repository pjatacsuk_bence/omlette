#include "DXUT.h"
#include <Omlette\Framework\Gui\GuiTimeline.h>
#include <Omlette\Framework\Gui\GuiManager.h>
#include <Omlette\Framework\Gui\CommandManager.h>
#include <Omlette\Framework\Gui\GuiTextBoxKeyFrame.h>
#include <Omlette\Framework\Gui\GuiLabel.h>
#include <Omlette\Framework\Utility\Caster.h>

using namespace Oml;

GuiTimeline::GuiTimeline()
{
	rectTopLeft = Egg::Math::int2(000,600);
	rectBottomRight = Egg::Math::int2(	ResourceManager::getInstance().getScreenWidth(),
										ResourceManager::getInstance().getScreenHeight());
	guiScrollable = GuiScrollable(D2D1::RectF(rectTopLeft.x, rectTopLeft.y, rectBottomRight.x, rectBottomRight.y));
	scrollBoxHeight = 20;
	minTime = 3000;
	maxTime = 6000;

	scrollVerticalPos = 0.0f;
	scrollDistance = 5.0f;
}
GuiTimeline::~GuiTimeline()
{
    guiTimelineComponents.clear();
}
D2D1_RECT_F GuiTimeline::getRect()
{
	if(PickableManager::getInstance().getPickedEntity() != 0)
	{
		D2D1_RECT_F timelineRect = getTimelineRect();
		D2D1_RECT_F scrollBoxRect = getScrollBoxRect();
		D2D1_RECT_F rect;
		rect.bottom = timelineRect.bottom > scrollBoxRect.bottom ? 
						timelineRect.bottom  : scrollBoxRect.bottom;
		rect.top = timelineRect.top < scrollBoxRect.top ? 
						timelineRect.top  : scrollBoxRect.top;
		rect.left = timelineRect.left < scrollBoxRect.left ? 
						timelineRect.left  : scrollBoxRect.left;
		rect.right = timelineRect.right > scrollBoxRect.right ? 
						timelineRect.right  : scrollBoxRect.right;
		return rect;
	}
	else
	{
		D2D1_RECT_F rect;
		rect.bottom = rect.top = rect.left = rect.right = 0;
		return rect;
	}
}

void GuiTimeline::moveTimelineByTime(Egg::Math::float1 timeMove)
{
	if(minTime + timeMove <(Egg::Math::float1)0 ||
		maxTime + timeMove > (Egg::Math::float1)TimeManager::getInstance().getMaxTime())
	{
		return;	
	}
	minTime = minTime + timeMove;
	maxTime = maxTime + timeMove;
}


void GuiTimeline::addZoomFactor(int zoomFactor)
{
	int zFabs = abs(zoomFactor);
	Egg::Math::float1 timeMove = zFabs * ZOOM_FACTOR_TIME_ZOOM;
	if(zoomFactor > 0)
	{
		if(minTime + timeMove <
			(maxTime - timeMove - (Egg::Math::float1)ZOOM_FACTOR_TIME_ZOOM))
		{
			minTime	= minTime + timeMove;
			maxTime = maxTime - timeMove;
		}
	}
	else 
	{
		if(minTime - timeMove >(Egg::Math::float1)0)
		{
			minTime = minTime - timeMove;
		}
		else
		{
			minTime = 0;
		}
		if(maxTime + timeMove < (Egg::Math::float1)TimeManager::getInstance().getMaxTime())
		{
			maxTime = maxTime + timeMove;
		}
		else
		{
			maxTime = (Egg::Math::float1)TimeManager::getInstance().getMaxTime();	
		}
	}
}

void GuiTimeline::addZoomFactorRelativeToMouse(int zoomFactor, Egg::Math::int2 mousePos)
{

	Egg::Math::float1 lerpParam = (float)(mousePos.x - rectTopLeft.x) /(float)(rectBottomRight.x - rectTopLeft.x);	
	Egg::Math::float1 oldTime = lerpParam.lerp(minTime,maxTime);

	addZoomFactor(zoomFactor);

	Egg::Math::float1 newTime = lerpParam.lerp(minTime,maxTime);
	Egg::Math::float1 diffTime = oldTime - newTime;
	moveTimelineByTime(diffTime);
}


D2D1_RECT_F GuiTimeline::getTimelineRect()
{
	Entity::P pickedEntity = PickableManager::getInstance().getPickedEntity();
	if(pickedEntity != NULL)
	{
		return timelineRect;
	}
}

D2D1_RECT_F GuiTimeline::getScrollBoxRect()
{
	Entity::P pickedEntity = PickableManager::getInstance().getPickedEntity();
	D2D1_RECT_F scrollBoxRect;
	if(pickedEntity != NULL)
	{
		scrollBoxRect.top =  rectBottomRight.y-scrollBoxHeight;
		scrollBoxRect.bottom = rectBottomRight.y;

		Egg::Math::float1 lerpParamLeft = (float)minTime /(float) (TimeManager::getInstance().getMaxTime());
		Egg::Math::float1 lerpParamRight = (float)maxTime /(float) (TimeManager::getInstance().getMaxTime());

		scrollBoxRect.left = lerpParamLeft.lerp(rectTopLeft.x,rectBottomRight.x);
		scrollBoxRect.right = lerpParamRight.lerp(rectTopLeft.x,rectBottomRight.x);
	}
	return scrollBoxRect;
}
void GuiTimeline::draw(ID2D1RenderTarget* renderTarget)
{
	drawProto(renderTarget);
	return;
	Entity::P pickedEntity = PickableManager::getInstance().getPickedEntity();
	ID2D1SolidColorBrush*	timelineBackgroundBrush = GuiManager::getInstance().getSolidGrayBrush();
	ID2D1SolidColorBrush*	keyFramesBrush = GuiManager::getInstance().getSolidRedBrush();
	ID2D1SolidColorBrush*	timePointBrush = GuiManager::getInstance().getBlackBrush();
	ID2D1SolidColorBrush*	scrollBoxBackgroundBrush = GuiManager::getInstance().getSolidGrayBrush();
	ID2D1SolidColorBrush*	timeStepLineBrush = GuiManager::getInstance().getBlackBrush();
	if(pickedEntity != NULL)
	{
		//Timeline drawing
		D2D1_RECT_F timelineRect = getTimelineRect();
		renderTarget->FillRectangle(&timelineRect, timelineBackgroundBrush);

		pickedEntity->timelineDraw(timelineRect, renderTarget, keyFramesBrush, minTime, maxTime);
		drawTimeStepLines(timelineRect, TimeManager::SECOND, renderTarget, timeStepLineBrush);

		//Time drawing
		Egg::Math::float1 l = (TimeManager::getInstance().getTime() - minTime) / (maxTime-minTime);

		D2D1_POINT_2F timePointTop;
		timePointTop.y = timelineRect.top;
		timePointTop.x = l.lerp(timelineRect.left,timelineRect.right);

		D2D1_POINT_2F timePointBottom;
		timePointBottom.y = timelineRect.bottom;
		timePointBottom.x = l.lerp(timelineRect.left,timelineRect.right);
		renderTarget->DrawLine(timePointTop,timePointBottom,timePointBrush);

		//Scrollbox drawing

		D2D1_RECT_F scrollBoxRect = getScrollBoxRect();
		renderTarget->FillRectangle(&scrollBoxRect,scrollBoxBackgroundBrush);

	}
}
void GuiTimeline::drawProto(ID2D1RenderTarget* renderTarget)
{
	
	Entity::P pickedEntity = PickableManager::getInstance().getPickedEntity();
	if (pickedEntity == NULL)
	{
		return;
	}
	ID2D1SolidColorBrush*	timelineBackgroundBrush = GuiManager::getInstance().getSolidGrayBrush();
	ID2D1SolidColorBrush*	keyFramesBrush = GuiManager::getInstance().getSolidRedBrush();
	ID2D1SolidColorBrush*	timePointBrush = GuiManager::getInstance().getBlackBrush();
	ID2D1SolidColorBrush*	scrollBoxBackgroundBrush = GuiManager::getInstance().getSolidGrayBrush();
	ID2D1SolidColorBrush*	timeStepLineBrush = GuiManager::getInstance().getBlackBrush();

	guiScrollable.beginScrollable(renderTarget);

	D2D1_RECT_F rect = guiScrollable.getDrawTargetRect();
	for (auto it = guiTimelineComponents.begin(); it != guiTimelineComponents.end(); it++){
		(*it)->draw(renderTarget,rect, GuiManager::getInstance().getSolidDarkerGrayBrush());
	}
	if (guiTimelineComponents.size() == 0) {
		return;
	}
	auto firstRect = guiTimelineComponents.front()->getRect();
	auto lastRect = guiTimelineComponents.back()->getRect();
//	rect.bottom = rect.top + GuiTimeline::HEIGHT_FOR_COMPONENT;
	rect.top = rectTopLeft.y;
	timelineRect = rect;
		
	guiScrollable.setMaxScrollAmountY(lastRect.bottom - firstRect.top);

	hitRect.top = rectTopLeft.y;
	hitRect.left = rectTopLeft.x;
	hitRect.bottom = ResourceManager::getInstance().getScreenHeight();
	hitRect.right = ResourceManager::getInstance().getScreenWidth();
		
	drawTimeStepLines(rect, TimeManager::SECOND, renderTarget, timeStepLineBrush);

	//Time drawing
	Egg::Math::float1 l = (TimeManager::getInstance().getTime() - minTime) / (maxTime-minTime);

	D2D1_POINT_2F timePointTop;
	timePointTop.y = rect.top;
	timePointTop.x = l.lerp(rect.left,rect.right);

	D2D1_POINT_2F timePointBottom;
	timePointBottom.y = rect.bottom;
	timePointBottom.x = l.lerp(rect.left,rect.right);
	renderTarget->DrawLine(timePointTop,timePointBottom,timePointBrush);

	//Scrollbox drawing

	D2D1_RECT_F scrollBoxRect = getScrollBoxRect();
	renderTarget->FillRectangle(&scrollBoxRect,scrollBoxBackgroundBrush);
	guiScrollable.endScrollable(renderTarget);

}

void GuiTimeline::drawTimeStepLines(D2D1_RECT_F rect, int step,ID2D1RenderTarget* renderTarget, ID2D1SolidColorBrush* color)
{
		if(step < TimeManager::MILISECOND/10) return;
		int secondCount = (float)(maxTime-minTime) / (float)step;
		for(int i=0;i<secondCount;i++)
		{
			Egg::Math::float1 l = (i*step) /(maxTime - minTime);
			D2D1_POINT_2F timePointTop;
			timePointTop.y = rect.top;
			timePointTop.x = l.lerp(rect.left,rect.right);

			D2D1_POINT_2F timePointBottom;
			timePointBottom.y = rect.bottom;
			timePointBottom.x = l.lerp(rect.left,rect.right);
			float width = 0;
			if(step == TimeManager::SECOND)
			{
				width = 0.8;	
			}
			else
			{
				width = 0.5;
			}
			renderTarget->DrawLine(timePointTop,timePointBottom,color,width);

		}
		drawTimeStepLines(rect, step/10, renderTarget, color);
}

void GuiTimeline::init()
{
	if (entity != NULL)
	{
		guiTimelineComponents.clear();
		auto components = entity->getComponentManager()->getComponents();
		auto it = components.begin();

		while (it != components.end())
		{
			GuiTimelineComponent::P gtc = GuiTimelineComponent::create(*it);
			guiTimelineComponents.push_back(gtc);
			it++;
		}
	}
}
void GuiTimeline::update()
{
	if (entity != PickableManager::getInstance().getPickedEntity())
	{
		entity = PickableManager::getInstance().getPickedEntity();
		init();
	}
}
void GuiTimeline::processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if (CommandManager::getInstance().getCommand() != CommandManager::Command::DraggingTimeline &&
		CommandManager::getInstance().getCommand() != CommandManager::Command::DraggingScrollbar && 
		CommandManager::getInstance().getCommand() != CommandManager::Command::None &&
		CommandManager::getInstance().getCommand() != CommandManager::Command::HandlingFocusedGuiElement)
	{
		auto com = CommandManager::getInstance().getCommand();
		return;
	}
	Entity::P pickedEntity = PickableManager::getInstance().getPickedEntity();
	if(pickedEntity != NULL)
	{
		guiScrollable.processMessage(hWnd, uMsg, wParam, lParam);
		POINT p = ResourceManager::getInstance().getMouseCoords(lParam);
		Egg::Math::int2 point(p.x,p.y);
		D2D1_RECT_F timelineRect = getTimelineRect();
		if(GuiManager::isPointInsideRect(p,timelineRect))
		{
			if(uMsg == WM_LBUTTONDBLCLK || uMsg == WM_LBUTTONDOWN)
			{
				GuiManager::getInstance().setReadyToCleanKeyFrameInfo(false);
                GuiManager::getInstance().setOverProperty(false);
				for (auto it = guiTimelineComponents.begin(); it != guiTimelineComponents.end(); it++){
					if (GuiManager::isPointInsideRect(p, (*it)->getRect())){
						(*it)->processMessage(hWnd, uMsg, wParam, lParam);
					}
				}
				if (GuiManager::getInstance().isReadyToCleanKeyFrameInfo()){
					GuiManager::getInstance().resetKeyFrameEditors();
				}
                if (!GuiManager::getInstance().isOverProperty()){
                    GuiManager::getInstance().setCurrentProperty(NULL);
                }
			}

		}
		if(uMsg == WM_MOUSEWHEEL)
		{
			RECT rect;
			GetWindowRect(hWnd,&rect);
			POINT p = ResourceManager::getInstance().getMouseCoordsWithoutBorders(lParam);
			p.x = p.x - rect.left;
			p.y = p.y - rect.top;
			Egg::Math::int2 point(p.x,p.y);
			if (GuiManager::isPointInsideRect(p, timelineRect))
			{

				if (wParam & MK_CONTROL){
					float zDelta = GET_WHEEL_DELTA_WPARAM(wParam);
					int zoomFactor = zDelta / 120.0f;
					addZoomFactorRelativeToMouse(zoomFactor, point);
				}
			}
		}
		if(uMsg == WM_LBUTTONUP)
		{
		CommandManager::getInstance().setCommand(CommandManager::Command::None);
		}
		if(( uMsg == WM_LBUTTONDOWN))
		{
			D2D1_RECT_F timelineRect = getTimelineRect();
			if(GuiManager::isPointInsideRect(p,timelineRect))
			{
				CommandManager::getInstance().setCommand(CommandManager::Command::DraggingTimeline);
				currentProperties = getPropertiesFromPoint(Egg::Math::int2(p.x,p.y),timelineRect);
				currentComponent = getComponentFromPoint(Egg::Math::int2(p.x, p.y), timelineRect);
			}
			else
			{
				//currentProperties.clear();
			}
			D2D1_RECT_F scrollBoxRect = getScrollBoxRect();
			if(GuiManager::isPointInsideRect(p,scrollBoxRect))
			{
				CommandManager::getInstance().setCommand(CommandManager::Command::DraggingScrollbar);
			}
		}
		if(CommandManager::getInstance().getCommand() == CommandManager::Command::DraggingTimeline)
		{
				D2D1_RECT_F timelineRect = getTimelineRect();
			if(uMsg == WM_LBUTTONDOWN || (uMsg == WM_MOUSEMOVE && (wParam & MK_LBUTTON)))
			{
				using namespace Egg::Math;
				float1 lerpParam = (float)(p.x - timelineRect.left) /(timelineRect.right - timelineRect.left);
				TimeManager::getInstance().time  = 
					lerpParam.lerp(minTime,
								   maxTime);
			}
		}
		if(CommandManager::getInstance().getCommand() == CommandManager::Command::DraggingScrollbar)
		{
			if(uMsg == WM_LBUTTONDOWN)
			{
				lButtonOldXPos = p.x;
			}
			if(uMsg == WM_MOUSEMOVE && (wParam & MK_LBUTTON))
			{
				Egg::Math::float1 dif = (p.x - rectTopLeft.x) - (lButtonOldXPos - rectTopLeft.x);
				Egg::Math::float1 sign = dif.sign();
				dif = dif.abs();
			
				Egg::Math::float1 maxTime = TimeManager::getInstance().getMaxTime();
				dif = (float)dif / (float)(rectBottomRight.x - rectTopLeft.x) ;
				Egg::Math::float1 timeMove = dif.lerp(0,maxTime) * sign;

				moveTimelineByTime(timeMove);


				lButtonOldXPos = p.x;
			}
		}
	}
	else
	{
		CommandManager::getInstance().setCommand(CommandManager::Command::None);
		currentComponent = NULL;
		currentProperties.clear();
	}
}

Egg::Math::float1	GuiTimeline::getTimeFromTimelinePoint(Egg::Math::int2 p)
	{
		if(PickableManager::getInstance().getPickedEntity() != NULL)
		{
			D2D1_RECT_F rect = getTimelineRect();
			Egg::Math::float1 lerpParam = (float)(p.x - rect.left) / (rect.right - rect.left);
			return	lerpParam.lerp(minTime,
								   maxTime);
		}
	}

std::vector<IPropertyAncestor::P>	GuiTimeline::getCurrentProperties()
{
	return currentProperties;
}
Oml::Component::P GuiTimeline::getCurrentComponent()
{
	return currentComponent;
}
std::vector<IPropertyAncestor::P>	GuiTimeline::getPropertiesFromPoint(Egg::Math::int2 point, D2D1_RECT_F rect)
{
	Entity::P pickedEntity = PickableManager::getInstance().getPickedEntity();
	if(pickedEntity != NULL)
	{
		return pickedEntity->getPropertiesFromPoint(point,rect);	
	}
}

Oml::Component::P	GuiTimeline::getComponentFromPoint(Egg::Math::int2 point, D2D1_RECT_F rect)
{
	Entity::P pickedEntity = PickableManager::getInstance().getPickedEntity();
	if(pickedEntity != NULL)
	{
		return pickedEntity->getComponentFromPoint(point,rect);	
	}
}