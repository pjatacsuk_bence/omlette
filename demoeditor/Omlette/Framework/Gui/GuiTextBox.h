#ifndef INC_GUITEXTBOX_H
#define INC_GUITEXTBOX_H
#include <Omlette\Interfaces\IGuiElement.h>
#include <string>

namespace Oml
{
class GuiTextBox : public IGuiElement
{
public:
	enum InputType
	{
		CHAR_ONLY,
		NUMBER_ONLY,
		ANY
	};
	static GuiTextBox::P	create(D2D1_RECT_F rect, InputType inputType=InputType::ANY);
			void			processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual	void			draw(ID2D1RenderTarget* renderTarget);
	virtual	void			draw(ID2D1RenderTarget* renderTarget,D2D1_RECT_F rect);
			std::string		getText();

protected:
					GuiTextBox(D2D1_RECT_F rect, InputType inputType=InputType::ANY);
	std::string		text;
	InputType		inputType;
	int				maxLength;
	int				border;

	
};	


}

#endif