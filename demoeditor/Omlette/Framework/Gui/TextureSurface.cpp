#include "DXUT.h"
#include <Omlette\Framework\Gui\TextureSurface.h>
#include <Omlette\Framework\Utility\ResourceManager.h>
using namespace Oml;

TextureSurface::TextureSurface(int width,int height)
{
	dirty = true;
	D3D11_TEXTURE2D_DESC adesc;
	adesc.Width = width;
	adesc.Height = height;
	adesc.MipLevels = 1;
	adesc.ArraySize = 1;
	adesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	adesc.SampleDesc.Count = 1;
	adesc.SampleDesc.Quality = 0.1;
	adesc.Usage = D3D11_USAGE_DYNAMIC;
	adesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	adesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	adesc.MiscFlags = 0;

	ResourceManager::getInstance().getDevice()->CreateTexture2D(&adesc, NULL, &texture);

	D3D11_SHADER_RESOURCE_VIEW_DESC	pickTextureSrvDesc;
	pickTextureSrvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	pickTextureSrvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	pickTextureSrvDesc.Texture2D.MipLevels = 1;
	pickTextureSrvDesc.Texture2D.MostDetailedMip = 0;
	ResourceManager::getInstance().getDevice()->CreateShaderResourceView(texture, &pickTextureSrvDesc, &srv);

	buffer = new char[width*height];
}
ID3D11ShaderResourceView* TextureSurface::getSrv()
{
	return srv;
}
TextureSurface::~TextureSurface()
{
	delete buffer;
	texture->Release();
	srv->Release();
}
void 	TextureSurface::Paint(unsigned char *src_buffer, int src_row_span, const Awesomium::Rect &src_rect, const Awesomium::Rect &dest_rect)
{


	D3D11_MAPPED_SUBRESOURCE mapped;
	if (texture == NULL)
	{
		int j = 01 + 10;
	}
	ID3D11DeviceContext* context;
	/*
	ResourceManager::getInstance().getDevice()->GetImmediateContext(&context);
	D3D11_BOX dstBox;
	dstBox.front = 0;
	dstBox.back = 1;
	dstBox.left = dest_rect.x;
	dstBox.right = dest_rect.x + dest_rect.width;
	dstBox.top = dest_rect.y;
	dstBox.bottom = dest_rect.y + dest_rect.height;
	
	context->UpdateSubresource(texture, 0, &dstBox, src_buffer, src_row_span, src_row_span * src_rect.height);
	*/
	
	int width = ResourceManager::getInstance().getScreenWidth();
	int height = ResourceManager::getInstance().getScreenHeight();
	const size_t size = src_rect.height *  src_row_span;
	unsigned char* pSrc = src_buffer;
	char* pDest = buffer;
	unsigned int srcOffset = src_rect.x * 4 + src_rect.y * src_row_span;
	unsigned int destOffset = dest_rect.x * 4 + dest_rect.y * src_row_span;
	memcpy(pDest+destOffset, pSrc+srcOffset, sizeof(char)*size);

	HRESULT hr = context->Map(texture, 0,D3D11_MAP_WRITE_DISCARD, 0, &mapped);

	if ( FAILED( hr ) )
	{
		return;
	}

	if ( pSrc == NULL )
	{
		context->Unmap( texture, 0 );
		return;
	}
	pDest = static_cast<char*>( mapped.pData );
	memcpy(pDest, buffer, sizeof(buffer));
	context->Unmap( texture, 0 );
	
	dirty = true;
}
void 	TextureSurface::Scroll(int dx, int dy, const Awesomium::Rect &clip_rect)
{
	int j = 222;
}