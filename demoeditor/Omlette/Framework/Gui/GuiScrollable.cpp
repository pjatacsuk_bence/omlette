#include "DXUT.h"
#include <Omlette\Framework\Gui\GuiScrollable.h>
#include <Omlette\Framework\Gui\GuiManager.h>
using namespace Oml;

GuiScrollable::GuiScrollable(D2D1_RECT_F rect) :
scrollableRect(rect)
{
	scrollableRect.right = scrollableRect.right - VERTICAL_SCROLLBOX_WIDTH;
}

GuiScrollable::GuiScrollable()
{
}

void GuiScrollable::beginScrollable(ID2D1RenderTarget* renderTarget)
{
	D2D1_RECT_F verticalScrollBarRect;
	Egg::Math::float1 l = scrollAmountY.x / maxScrollAmountY.x;

	verticalScrollBarRect.left = scrollableRect.right;
	verticalScrollBarRect.right = scrollableRect.right + VERTICAL_SCROLLBOX_WIDTH;
	verticalScrollBarRect.top = l.lerp(scrollableRect.top, scrollableRect.bottom - VERTICAL_SCROLLBOX_HEIGHT);
	verticalScrollBarRect.bottom = verticalScrollBarRect.top + VERTICAL_SCROLLBOX_HEIGHT;

	renderTarget->FillRectangle(verticalScrollBarRect, GuiManager::getInstance().getSolidDarkerGrayBrush());

	layer = NULL;
//	renderTarget->CreateLayer(NULL,&layer);
//	renderTarget->PushLayer(D2D1::LayerParameters(scrollableRect), layer);

}

void GuiScrollable::endScrollable(ID2D1RenderTarget* renderTarget)
{
//	renderTarget->PopLayer();
//	layer->Release();
}

void GuiScrollable::processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if(uMsg == WM_MOUSEWHEEL)
	{
		RECT rect;
		GetWindowRect(hWnd,&rect);
		POINT p = ResourceManager::getInstance().getMouseCoordsWithoutBorders(lParam);
		p.x = p.x - rect.left;
		p.y = p.y - rect.top;
		Egg::Math::int2 point(p.x,p.y);
		if(GuiManager::isPointInsideRect(p,scrollableRect))
		{
			Egg::Math::float1 zDelta = GET_WHEEL_DELTA_WPARAM(wParam);
			scrollAmountY = scrollAmountY  - (zDelta / Egg::Math::float1(30.0f));
			if (scrollAmountY < Egg::Math::float1(0.0f)) {
				scrollAmountY = 0.0f;
			}
			else if (scrollAmountY > maxScrollAmountY){
				scrollAmountY = maxScrollAmountY;
			}
		}
	}
}