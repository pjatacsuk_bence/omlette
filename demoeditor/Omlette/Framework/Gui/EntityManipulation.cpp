#include "DXUT.h"
#include <Omlette\Framework\Gui\EntityManipulation.h>
#include <Omlette\Framework\Gui\CommandManager.h>
#include <Omlette\Cam\CameraManager.h>
#include <Omlette\Framework\Utility\ResourceManager.h>
#include <Omlette\Framework\Gui\GuiManager.h>
#include <Omlette\Framework\Utility\Caster.h>
#include <WindowsX.h>

using namespace Oml;
using namespace Oml::Cam;
void EntityManipulation::processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if (CommandManager::getInstance().getCommand() != CommandManager::Command::DraggingEntityPositionX &&
		CommandManager::getInstance().getCommand() != CommandManager::Command::DraggingEntityPositionY &&
		CommandManager::getInstance().getCommand() != CommandManager::Command::DraggingEntityPositionZ)
	{
		return;
	}
	Entity::P pickedEntity = PickableManager::getInstance().getPickedEntity();
	if (pickedEntity != NULL)
	{
		if (uMsg == WM_LBUTTONDOWN || (uMsg == WM_MOUSEMOVE && (wParam & MK_LBUTTON)))
		{
			auto properties = GuiManager::getInstance().getCurrentProperty();
			auto component = GuiManager::getInstance().getCurrentComponent();
			if (component == NULL)
			{
				return;
			}
			auto transComp = Caster::getInstance().getComponentCasted<TransformComponent::P>(component);
			if (transComp == NULL)
			{
				return;
			}
			if (properties->getType() == FLOAT4)
			{
				auto propfloat4 = Caster::getInstance().getPropertyCasted<Property<Egg::Math::float4>::P>(properties);
				auto keyFrames = propfloat4->getKeyFramesFromTime(TimeManager::getInstance().getTime());
				auto posfloat4 = transComp->getPositionValue(TimeManager::getInstance().getTime());
				Egg::Math::float2 mouseCoord;
				mouseCoord.x = GET_X_LPARAM(lParam);
				mouseCoord.y = GET_Y_LPARAM(lParam);
				Egg::Math::float3 point;
				if (CommandManager::getInstance().getCommand() == CommandManager::Command::DraggingEntityPositionX)
				{
					point = getPositionFromMouseCoord(mouseCoord, Manipulation::TransformX);
					if (keyFrames.size() >= 1)
						keyFrames[0]->key.x = point.x;
					if (keyFrames.size() == 2)
						keyFrames[1]->key.x = point.x;
				}
				else if (CommandManager::getInstance().getCommand() == CommandManager::Command::DraggingEntityPositionY)
				{
					point = getPositionFromMouseCoord(mouseCoord, Manipulation::TransformY);
					if (keyFrames.size() >= 1)
						keyFrames[0]->key.y = point.y;
					if (keyFrames.size() == 2)
						keyFrames[1]->key.y = point.y;
				}
				else if (CommandManager::getInstance().getCommand() == CommandManager::Command::DraggingEntityPositionZ)
				{
					point = getPositionFromMouseCoord(mouseCoord, Manipulation::TransformZ);
					if (keyFrames.size() >= 1)
						keyFrames[0]->key.z = point.z;
					if (keyFrames.size() == 2)
						keyFrames[1]->key.z = point.z;
				}
			}
		}
	}
	if (uMsg == WM_LBUTTONUP)
	{
		CommandManager::getInstance().setCommand(CommandManager::Command::None);
	}
}

Egg::Math::float3	EntityManipulation::getPositionFromMouseCoord(Egg::Math::float2 mouseCoord, Manipulation manipulation)
{
	ICam::P cam = CameraManager::getInstance().getCurrentCam();

	Egg::Math::float4x4 view = cam->getViewMatrix();
	Egg::Math::float4 mousePos;
	Egg::Math::float4 mousePosFar;
	mousePos.x = mouseCoord.x;
	mousePos.y = mouseCoord.y;
	mousePos.z = 0.1;
	mousePos.w = 1;

	mousePosFar.z = 1000;

	Egg::Math::float1 screenWidth = ResourceManager::getInstance().getScreenWidth();
	Egg::Math::float1 screenHeight = ResourceManager::getInstance().getScreenHeight();

	mousePos.x = (mousePos.x - screenWidth / (Egg::Math::float1)2.0) / screenWidth;
	mousePos.x = mousePos.x * 2;
	mousePos.y = (float)screenHeight - (float)mousePos.y;
	mousePos.y = (mousePos.y - screenHeight / (Egg::Math::float1)2.0) / screenHeight;
	mousePos.y = mousePos.y * 2;

	Egg::Math::float4x4 proj = cam->getProjMatrix();
	Egg::Math::float3 camPos = cam->getEyePosition();
	Egg::Math::float4x4 projInv = proj.invert();
	Egg::Math::float4x4 viewInv = view.invert();
	Egg::Math::float4 mouseWorldPos = mousePos * projInv;
	mouseWorldPos = mouseWorldPos * viewInv;
	mouseWorldPos.x = mouseWorldPos.x / mouseWorldPos.w;
	mouseWorldPos.y = mouseWorldPos.y / mouseWorldPos.w;
	mouseWorldPos.z = mouseWorldPos.z / mouseWorldPos.w;
	mouseWorldPos.w = mouseWorldPos.w / mouseWorldPos.w;

	Egg::Math::float4 mouseWorldPosFar = mousePosFar * projInv;
	mouseWorldPosFar = mouseWorldPosFar * viewInv;
	mouseWorldPosFar.x = mouseWorldPosFar.x / mouseWorldPosFar.w;
	mouseWorldPosFar.y = mouseWorldPosFar.y / mouseWorldPosFar.w;
	mouseWorldPosFar.z = mouseWorldPosFar.z / mouseWorldPosFar.w;
	mouseWorldPosFar.w = mouseWorldPosFar.w / mouseWorldPosFar.w;

	Egg::Math::float4 dir = mouseWorldPosFar - mouseWorldPos;
	Egg::Math::float4 origin = mouseWorldPos;

	auto properties = GuiManager::getInstance().getCurrentProperties();
	auto component = GuiManager::getInstance().getCurrentComponent();
	auto transComp = Caster::getInstance().getComponentCasted<TransformComponent::P>(component);
	if (manipulation == Manipulation::TransformX || manipulation == Manipulation::TransformY)
	{
		Egg::Math::float1 targetZ = transComp->getPositionValue(TimeManager::getInstance().getTime()).z;
		Egg::Math::float1 distance = ((float)targetZ - origin.z) / dir.z;
		Egg::Math::float3 point = origin.xyz + dir.xyz * (float)distance;
		return point;
	}
	else if (manipulation == Manipulation::TransformZ)
	{
		Egg::Math::float1 targetY = transComp->getPositionValue(TimeManager::getInstance().getTime()).y;
		Egg::Math::float1 distance = ((float)targetY - origin.y) / dir.y;
		Egg::Math::float3 point = origin.xyz + dir.xyz * (float)distance;
		return point;
	}

}
