#ifndef INC_GUITEXTBOXKEYFRAMEVALUE_H
#define INC_GUITEXTBOXKEYFRAMEVALUE_H
#include <Omlette\Framework\Gui\GuiTextBox.h>
#include <Omlette\Framework\Segments\Segment.h>
#include <string>

namespace Oml
{
template<class T>
class GuiTextBoxKeyFrameValue : public GuiTextBox
{
public:
	typedef boost::shared_ptr<GuiTextBoxKeyFrameValue> P;
	inline static P	create(D2D1_RECT_F rect, InputType inputType, T* keyFrameValue)
	{
		return P(new GuiTextBoxKeyFrameValue(rect,inputType,keyFrameValue));
	}

	void	execute();
protected:
	GuiTextBoxKeyFrameValue(D2D1_RECT_F rect, InputType inputType,T* keyFrameValue):
	GuiTextBox(rect,inputType)
	{
		this->keyFrameValue = keyFrameValue;
		std::stringstream ss;
		ss << *keyFrameValue;
		text = ss.str();
	}

	T*		keyFrameValue;
	
};	

template<class T>
void GuiTextBoxKeyFrameValue<T>::execute()
{
	
}

template<>
inline
void GuiTextBoxKeyFrameValue<Egg::Math::float1>::execute()
{
	if(inputType == InputType::NUMBER_ONLY)
	{
		*keyFrameValue = atof(text.c_str());
	}
}
template<>
inline
void GuiTextBoxKeyFrameValue<float>::execute()
{
	if(inputType == InputType::NUMBER_ONLY)
	{
		*keyFrameValue = atof(text.c_str());
	}
}

template<>
inline
void GuiTextBoxKeyFrameValue<std::string>::execute()
{
	if(inputType == InputType::CHAR_ONLY)
	{
		*keyFrameValue = text.c_str();
	}
}

}
#endif