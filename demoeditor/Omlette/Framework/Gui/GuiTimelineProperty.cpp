#include "DXUT.h"
#include <Omlette\Framework\Gui\GuiTimelineProperty.h>
#include <Omlette\Framework\Properties\Property.h>
#include <Omlette\Framework\Gui\GuiLabel.h>
#include <Omlette\Framework\Gui\GuiManager.h>
#include <Omlette\Framework\Utility\Caster.h>
#include <Omlette\Framework\Utility\Factory.h>
using namespace Oml;

GuiTimelineProperty::GuiTimelineProperty(Oml::IPropertyAncestor::P property)
{
	this->property = property;
	guiSegments = Factory::getInstance().createGuiTimelineSegment(property);
}
void	GuiTimelineProperty::draw(ID2D1RenderTarget* renderTarget, D2D1_RECT_F& rect, ID2D1SolidColorBrush* bgBrush)
{
	D2D1_RECT_F propRect = rect;
	propRect.bottom = rect.top + GuiTimeline::HEIGHT_FOR_PROPERTY; 
	renderTarget->FillRectangle(propRect, bgBrush);

	for (auto it = guiSegments.begin(); it != guiSegments.end(); it++){
		(*it)->draw(renderTarget, propRect);
	}

	std::string text = GuiLabel::getTabs() + property->getName() + "::" + property->getTypeName();
	GuiLabel::draw(renderTarget, propRect, text, GuiManager::getInstance().getBlackBrush(), NULL, Oml::GuiLabel::LEFT);

	hitRect = propRect;
	rect.top = propRect.bottom;
	rect.bottom = propRect.bottom;


}

void GuiTimelineProperty::processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    GuiManager::getInstance().setOverKeyFrame(false);
	bool isOverSegment = false;
	POINT p = ResourceManager::getInstance().getMouseCoords(lParam);
	for (auto it = guiSegments.begin(); it != guiSegments.end(); it++)
	{
		if (GuiManager::isPointInsideRect(p, (*it)->getRect())){
			(*it)->processMessage(hWnd, uMsg, wParam, lParam);
			isOverSegment = true;
		}
	}
	bool areSegmentsDirty = false;
	if (uMsg == WM_LBUTTONDOWN){
		if (!isOverSegment) {
			GuiManager::getInstance().setReadyToCleanKeyFrameInfo(true);
		}
	}

	if (uMsg == WM_LBUTTONDBLCLK)
	{
        if (!GuiManager::getInstance().isOverKeyFrame())
        {
            Egg::Math::int2 point(p.x, p.y);
            Egg::Math::float1 time = GuiManager::getInstance().getGuiTimeline()->getTimeFromTimelinePoint(point);
            property->addKeyFrame(time);
            areSegmentsDirty = true;
        }
	}
	for (auto it = guiSegments.begin(); it != guiSegments.end(); it++){
		if ((*it)->isDirty()){
			areSegmentsDirty = true;
			break;
		}
	}
	if (areSegmentsDirty){
		guiSegments.clear();
		guiSegments = Factory::getInstance().createGuiTimelineSegment(property);
	}
}

