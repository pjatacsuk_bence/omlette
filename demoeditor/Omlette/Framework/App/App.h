#ifndef INC_APP_H
#define INC_APP_H
#include "d3dx11effect.h"
#include "App\SystemEnvironment.h" 
#include "Omlette\Mesh\ShadedMesh.h"
#include "Omlette\Entities\EntityManager.h"
#include "Omlette\Cam\FirstPersonCam.h"
#include "Omlette\Mesh\Binder.h"


namespace Oml
{
class	App
{
public:
							App(ID3D11Device* pd3dDevice);
	virtual					~App(){}

	virtual std::string		getMainEffectFilename(){ return "basic.fx";}
	virtual void			loadEffect();
	Egg::SystemEnvironment& getSystemEnvironment(){return systemEnvironment;}

	ID3DX11Effect*			getEffect(){return effect;}

	void					setSwapChain(IDXGISwapChain* swapChain,const DXGI_SURFACE_DESC* backBufferDesc);
	void					createSwapChainResources();

	HRESULT					createResources(const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc);
	void					animate(float fElapsedTime,float fTime);
	void					render(ID3D11DeviceContext* pd3dImmediateContext);
	void					releaseSwapChainResources();
	HRESULT					releaseResources();
	bool					processMessage(HWND hWnd,unsigned int uMsg,WPARAM wParam,LPARAM lParam);
protected:
	ID3D11Device*			device;
	IDXGISwapChain*			swapChain;
	DXGI_SURFACE_DESC		backBufferSurfaceDesc;
	ID3DX11Effect*			effect;

/***/
	Mesh::Binder::P				binder;
	Cam::FirstPersonCam::P		firstPersonCam;
/***/
	Egg::SystemEnvironment	systemEnvironment;
private:

};

}


#endif