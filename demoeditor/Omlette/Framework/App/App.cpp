#include "DXUT.h"
#include "App.h"
#include "App\EffectInclude.h"
#include "Math\math.h"
#include <assimp\scene.h>
#include <assimp\Importer.hpp>
#include <Omlette\Mesh\MeshImporter.h>
#include <Omlette\Mesh\Material.h>
#include <Omlette\Mesh\Binder.h>
#include <Omlette\Framework\Utility\Caster.h>
#include <Omlette\Framework\Properties\Property.h>
#include <Omlette\Cam\CameraManager.h>
#include <Omlette\Components\ComponentFactory.h>
#include <Omlette\Framework\Utility\ResourceManager.h>
#include <omp.h>
#include <Omlette\Framework\Time\TimeManager.h>
#include <Omlette\Framework\Gui\GuiManager.h>
#include <Omlette\Framework\Gui\RightClickManager.h>
#include <Omlette\Framework\Gui\GuiElementsManager.h>
#include <Omlette\Framework\Utility\LightManager.h>
#include <Omlette\Components\SolidColorMaterial.h>
using namespace Oml;

App::App(ID3D11Device* device):
device(device),
effect(NULL),
swapChain(NULL)
{
}

void	App::setSwapChain(IDXGISwapChain* swapChain,const DXGI_SURFACE_DESC* backBufferDesc)
{
	this->swapChain = swapChain;
	this->backBufferSurfaceDesc = *backBufferDesc;
	ResourceManager::getInstance().setScreenWidth(backBufferDesc->Width);
	ResourceManager::getInstance().setScreenHeight(backBufferDesc->Height);
}

void App::loadEffect()
{
	ID3DBlob* compilationErrors = NULL;
	ID3DBlob* compiledEffect = NULL;

	Egg::EffectInclude effectInclude(systemEnvironment);

	if(FAILED(
		D3DX11CompileFromFileA(
			std::string("./fx/basic.fx").c_str(), NULL, &effectInclude, NULL,
			"fx_5_0", 0, 0, NULL, &compiledEffect, &compilationErrors, NULL))) {
				if(compilationErrors)
					MessageBoxA( NULL, 
					(LPSTR)compilationErrors->GetBufferPointer(),
					"Failed to load effect file!", MB_OK);
				else
					MessageBoxA( NULL, 
					"File cound not be opened",
					"Failed to load effect file!", MB_OK);
				exit(-1);
		}
	else
	if(FAILED(
	D3DX11CreateEffectFromMemory(
		compiledEffect->GetBufferPointer(), 
		compiledEffect->GetBufferSize(),
		0,
		device,
		&effect)  )) {
		MessageBoxA( NULL, 
			"D3DX11CreateEffectFromMemory failed.",
			"Failed to load effect file!", MB_OK);
		exit(-1);
		}
}

void App::createSwapChainResources()
{
	Cam::CameraManager::getInstance().setAspect((float)backBufferSurfaceDesc.Width /(float) backBufferSurfaceDesc.Height);
	ResourceManager::getInstance().setScreenWidth(backBufferSurfaceDesc.Width);
	ResourceManager::getInstance().setScreenHeight(backBufferSurfaceDesc.Height);
	GuiManager::getInstance().createSwapChainResources();

} 
HRESULT	App::createResources(const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc) 
{
	loadEffect();	
	backBufferSurfaceDesc = *pBackBufferSurfaceDesc;
	binder = Oml::Mesh::Binder::create(device);
	ResourceManager::getInstance().init(DXUTGetHWND());
	ResourceManager::getInstance().setBinder(binder);
	ResourceManager::getInstance().setScreenWidth(pBackBufferSurfaceDesc->Width);
	ResourceManager::getInstance().setScreenHeight(pBackBufferSurfaceDesc->Height);
	ResourceManager::getInstance().setDevice(device);
	ResourceManager::getInstance().setEffect(effect);
	firstPersonCam = Oml::Cam::FirstPersonCam::create();
	Cam::CameraManager::getInstance().setEditorCam(firstPersonCam);
	ComponentFactory::getInstance().setDeviceEffectAndBinder(device,effect,binder);
	PickableManager::getInstance().init(device, binder, effect);
	GuiManager::getInstance().init(device,effect);
	EntityManager::getInstance().deserialize("C:\\Users\\Pjata\\Documents\\Visual Studio 2010\\Projects\\demoeditor\\demoeditor\\Resources\\Omlettes\\material2.xml");
/*	Entity::P	hello = EntityManager::getInstance().getEntity("Hello");
	SolidColorMaterial::P scm = SolidColorMaterial::create("scm");
	hello->getComponentManager()->addComponent(scm);*/
		return S_OK;


}

void App::releaseSwapChainResources()
{
	GuiManager::getInstance().releaseSwapChainResources();
}

HRESULT App::releaseResources()
{
	binder.reset();

	return S_OK;
}

bool App::processMessage(HWND hWnd, unsigned uMsg, WPARAM wParam, LPARAM lParam)
{
	if(!firstPersonCam) return false;
	firstPersonCam->processMessage(hWnd, uMsg, wParam, lParam);

	if(uMsg == WM_KEYDOWN)
	{
		if(wParam == VK_F6)
		{
			EntityManager::getInstance().serialize("./material2.xml");
		}
	}

	double start = omp_get_wtime();
	TimeManager::getInstance().processMessage(hWnd, uMsg, wParam, lParam);
	GuiManager::getInstance().processMessage(hWnd, uMsg, wParam, lParam);
	PickableManager::getInstance().processMessage(hWnd, uMsg, wParam, lParam);
	RightClickManager::getInstance().processMessage(hWnd, uMsg, wParam, lParam);
	EntityManager::getInstance().processMessage(hWnd, uMsg, wParam, lParam);
	GuiManager::getInstance().update();
	double end = omp_get_wtime();
	double dif = end - start;
	return false;
}

void App::animate(float fElapsedTime, float fTime)
{
	TimeManager::getInstance().update(fTime, fElapsedTime);

	if(!firstPersonCam) return;
	firstPersonCam->animate(fElapsedTime);


	EntityManager::getInstance().update(TimeManager::getInstance().getTime());

	
}

void App::render(ID3D11DeviceContext* context)
{
	float clearColor[4]	 = {0.3f,0.7f,0.3f,0.0f};
	
	ID3D11RenderTargetView*	defaultRtv = DXUTGetD3D11RenderTargetView();
	ID3D11DepthStencilView*	defaultDsv = DXUTGetD3D11DepthStencilView();

	context->ClearRenderTargetView(defaultRtv,clearColor);
	context->ClearDepthStencilView(defaultDsv,D3D11_CLEAR_DEPTH, 1.0,0);
	context->ClearDepthStencilView(defaultDsv,D3D11_CLEAR_STENCIL, 1.0,0);




	PickableManager::getInstance().handlePicking(context,effect);
	EntityManager::getInstance().draw(context,effect,&(ResourceManager::getInstance().getMien("ambient")));
	for (int i = 0; i < LightManager::getInstance().getLightsNumber(); i++)
	{
		LightManager::getInstance().applyLight(i);
		EntityManager::getInstance().draw(context,effect,&(ResourceManager::getInstance().getMien("shadow")));
	}
	for (int i = 0; i < LightManager::getInstance().getLightsNumber(); i++)
	{
		LightManager::getInstance().applyLight(i);
		EntityManager::getInstance().draw(context,effect,&(ResourceManager::getInstance().getMien("shaded")));
	}
	
	GuiManager::getInstance().draw(context,effect);

}