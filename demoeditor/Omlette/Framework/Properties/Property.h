#ifndef INC_PROPERTY_H
#define INC_PROPERTY_H
#include <Omlette\Interfaces\ISerializable.h>
#include <vector>
#include <string>
#include <Omlette\Framework\Segments\SegmentManager.h>
#include <Omlette\Interfaces\ITimelineDrawable.h>
#include <Omlette\Framework\Segments\Segment.h>
#include <Omlette\Framework\Properties\PropertyBase.h>
#include <map>
#include "Math\math.h"

namespace Oml
{

template<class T>
class Property : public Oml::ISerializable,public PropertyBase<T>
{
public:
	typedef boost::shared_ptr<Property<T>>	P;

							Property(std::string name);
							Property();
	typename Segment<T>::P	getSegment(float t);
	T						getValue(float t);
	void					serialize(pugi::xml_node& node);
	void					deserialize(pugi::xml_node& node);
	void					addKeyFrame(KeyFrame<T>* kf);
	void					addKeyFrame(Egg::Math::float1 time);
	void					deleteKeyFramesByTime(Egg::Math::float1 time);
	void					deleteKeyFrame(KeyFrame<T>* kf);
	void					timelineDraw(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget, ID2D1SolidColorBrush* brush,Egg::Math::float1 timelineMinTime, Egg::Math::float1 timelineMaxTime);
							~Property();

	std::vector<KeyFrame<T>*>	getKeyFramesFromTime(Egg::Math::float1 time);
	std::map<T,bool>				getKeys();
	
	typedef T template_type;
	inline	typename SegmentManager<T>::P	getSegmentManager() { return segmentManager; }
private:
	typename	SegmentManager<T>::P		segmentManager;
};

}//NAMESPACE OML
#endif
