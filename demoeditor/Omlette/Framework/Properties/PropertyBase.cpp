#include "DXUT.h"
#include <Omlette\Framework\Properties\PropertyBase.h>

using namespace Oml;
/*
template<>
class PropertyBase<unsigned int> : public IPropertyAncestor
{
protected:
	PropertyBase() :
		IPropertyAncestor(UINT32)
	{
	}

};

template<>
class PropertyBase<int> : public IPropertyAncestor
{
protected:
	PropertyBase() :
		IPropertyAncestor(Oml::Type::INT32)
	{
	}

};

template<>
class PropertyBase<Egg::Math::float1> : public IPropertyAncestor
{
protected:
	PropertyBase() :
		IPropertyAncestor(FLOAT)
	{
	}
};
template<>
class PropertyBase<float> : public IPropertyAncestor
{
protected:
	PropertyBase() :
		IPropertyAncestor(FLOAT)
	{
	}
};

template<>
class PropertyBase<std::string> : public IPropertyAncestor
{
protected:
	PropertyBase() :
		IPropertyAncestor(STRING)
	{
	}
};

template<>
class PropertyBase<Egg::Math::float4> : public IPropertyAncestor
{
protected:
	PropertyBase() :
		IPropertyAncestor(FLOAT4)
	{
	}
};

template<>
class PropertyBase<Egg::Math::float3> : public IPropertyAncestor
{
protected:
	PropertyBase() :
		IPropertyAncestor(FLOAT3)
	{
	}
};
*/
template<class T> 
PropertyBase<T>::PropertyBase():
IPropertyAncestor(NONE)
{

}

template<class T>
PropertyBase<T>* PropertyBase<T>::getPropertyType(){
	return this;
}

template<>
PropertyBase<Egg::Math::float4>::PropertyBase():
IPropertyAncestor(FLOAT4){

}

template<>
PropertyBase<Egg::Math::float3>::PropertyBase():
IPropertyAncestor(FLOAT3){

}

template<>
PropertyBase<Egg::Math::float1>::PropertyBase():
IPropertyAncestor(FLOAT){

}

template<>
PropertyBase<float>::PropertyBase():
IPropertyAncestor(FLOAT){

}

template<>
PropertyBase<int>::PropertyBase():
IPropertyAncestor(INT32){

}
template<>
PropertyBase<unsigned int>::PropertyBase():
IPropertyAncestor(UINT32){

}
template<>
PropertyBase<std::string>::PropertyBase():
IPropertyAncestor(STRING){

}

template class PropertyBase<int>;
template class PropertyBase<float>;
template class PropertyBase<class Egg::Math::float1>;
template class PropertyBase<class Egg::Math::float3>;
template class PropertyBase<class Egg::Math::float4>;
template class PropertyBase<std::string>;
