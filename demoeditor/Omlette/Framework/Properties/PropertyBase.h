#ifndef INC_PROPERTYBASE_H
#define INC_PROPERTYBASE_H
#include <Omlette\Framework\Properties\IPropertyAncestor.h>
namespace Oml {

template<class T>
class PropertyBase : public IPropertyAncestor
{
public:
					PropertyBase();
	virtual void	timelineDraw(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget, ID2D1SolidColorBrush* brush, Egg::Math::float1 timelineMinTime, Egg::Math::float1 timelineMaxTime) = 0;
	PropertyBase<T>* getPropertyType();

	typedef T template_type;
protected:
};

}

#endif