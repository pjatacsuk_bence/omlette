#ifndef INC_IPROPERTYANCESTOR_H
#define INC_IPROPERTYANCESTOR_H
#include <Omlette\Interfaces\ITimelineDrawable.h>
#include "boost\shared_ptr.hpp"
namespace Oml
{
enum Type
{
	FLOAT, FLOAT4, FLOAT3, UINT32, STRING, INT32, NONE
};


class IPropertyAncestor : public ITimelineDrawable
{
public:

	typedef boost::shared_ptr<IPropertyAncestor> P;

							IPropertyAncestor();
							IPropertyAncestor(Type type);
	virtual					~IPropertyAncestor();
	virtual void			timelineDraw(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget, ID2D1SolidColorBrush* brush, Egg::Math::float1 timelineMinTime, Egg::Math::float1 timelineMaxTime) = 0;
	virtual void			addKeyFrame(Egg::Math::float1 time) = 0;
	virtual void			deleteKeyFramesByTime(Egg::Math::float1 time) = 0;
	inline	std::string		getName() { return name; }
	inline	Type			getType() { return type; }
			std::string		getTypeName();
	virtual IPropertyAncestor* getPropertyType() = 0;
protected:
	std::string			name;
	Type				type;
};

}//NAMESPACE OML

#endif