#include "DXUT.h"
#include <Omlette\Framework\Properties\IPropertyAncestor.h>

using namespace Oml;

IPropertyAncestor::IPropertyAncestor():
type(NONE){
}

IPropertyAncestor::IPropertyAncestor(Type type) :
type(type)
{

}

IPropertyAncestor::~IPropertyAncestor(){

}

std::string IPropertyAncestor::getTypeName()
{
	switch (type)
	{
	case STRING:
		return std::string("STRING");
		break;
	case FLOAT:
		return std::string("FLOAT");
		break;
	case UINT32:
		return std::string("UINT32");
		break;
	case INT32:
		return std::string("INT");
		break;
	case FLOAT4:
		return std::string("FLOAT4");
		break;
	case FLOAT3:
		return std::string("FLOAT4");
		break;
	}
}