#include "DXUT.h"
#include <Omlette\Framework\Properties\Property.h>

using namespace Oml;

template<class T>
Property<T>::Property()
{
	segmentManager = SegmentManager<T>::create();
	name = "";
}
template<class T>
Property<T>::Property(std::string nameString ="")
{
	segmentManager = SegmentManager<T>::create();
	name = nameString;
}
template<class T>
Property<T>::~Property()
{
	segmentManager.reset();
}
template<class T>
std::map<T,bool> Property<T>::getKeys()
{
	return segmentManager->getKeys();
}
template<class T>
void Property<T>::deleteKeyFramesByTime(Egg::Math::float1 time)
{
	segmentManager->deleteKeyFramesByTime(time);
}
template<class T>
std::vector<KeyFrame<T>*> Property<T>::getKeyFramesFromTime(Egg::Math::float1 time)
{
	return	segmentManager->getKeyFramesFromTime(time);
}
template<class T>
void	Property<T>::timelineDraw(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget, ID2D1SolidColorBrush* brush, Egg::Math::float1 timelineMinTime, Egg::Math::float1 timelineMaxTime)
{
	segmentManager->timelineDraw(rect, renderTarget, brush, timelineMinTime, timelineMaxTime);
}

template<class T>
void	Property<T>::serialize(pugi::xml_node& node)
{
	pugi::xml_node paramNode = node.append_child("Property");
	paramNode.append_attribute("name") = getName().c_str();
	paramNode.append_attribute("type") = getTypeName().c_str();
	pugi::xml_node segmentsNode = paramNode.append_child("Segments");
	segmentManager->serialize(segmentsNode);
}
template<class T>
void	Property<T>::deserialize(pugi::xml_node& node)
{
	segmentManager->deserialize(node.first_child());
	
}

template <class T>
void Property<T>::addKeyFrame(Egg::Math::float1 time)
{
	segmentManager->addKeyFrame(time);
}
template <class T>
void Property<T>::addKeyFrame(KeyFrame<T>* kf)
{
	segmentManager->addKeyFrame(kf);
}

template<class T>
typename Segment<T>::P Property<T>::getSegment(float t)
{
	return segmentManager->getSegment(t);
}

template<class T>
T		Property<T>::getValue(float t)
{
	return segmentManager->getValue(t);
}

template<class T>
void	Property<T>::deleteKeyFrame(KeyFrame<T>* kf)
{
	segmentManager->deleteKeyFrame(kf);
}

template class Property<int>;
template class Property<float>;
template class Property<class Egg::Math::float1>;
template class Property<class Egg::Math::float3>;
template class Property<class Egg::Math::float4>;
template class Property<std::string>;