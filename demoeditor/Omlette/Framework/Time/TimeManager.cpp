#include "DXUT.h"
#include "TimeManager.h"
#include <D2D1.h>
#include <Omlette\Framework\Gui\GuiManager.h>

using namespace Oml;

void TimeManager::processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	handleLButtonDown(hWnd,uMsg,wParam,lParam);
	if(uMsg == WM_KEYDOWN)
	{
		if(wParam == VK_ADD)
		{
			playingSpeed = playingSpeed + 0.05 <= maxSpeed ? playingSpeed + 0.05 : maxSpeed;
		}
		if(wParam == VK_SUBTRACT)
		{
			playingSpeed = playingSpeed - 0.05 >= minSpeed ? playingSpeed - 0.05 : minSpeed;
		}
		if(wParam == VK_SPACE)
		{
			isPlaying = !isPlaying;
		}
	}

}

void TimeManager::handleLButtonDown(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
}

void TimeManager::update(float fTime, float dt)
{
	fTime = fTime * 1000;
	dt = dt * 1000;
	if(isPlaying)
	{
		if(time > maxTime)
		{
			time = 0;
		}
		time += dt * playingSpeed;

		
	}
}