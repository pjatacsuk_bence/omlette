#ifndef INC_TIMEMANAGER_H
#define INC_TIMEMANAGER_H

namespace Oml
{
class TimeManager
{
friend class GuiTimeline;
public:
	static const int SECOND = 1000;
	static const int MILISECOND = 1000;
	static TimeManager& getInstance()
	{
		static TimeManager instance;
		return instance;
	}
	
	
	inline	float		getTime()
	{
		return time;
	}
	inline float		getMaxTime()
	{
		return maxTime;	
	}
	inline void			setMaxTime(float mtime)
	{
		maxTime  = mtime;
	}
	void		processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	void		handleLButtonDown(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	void		update(float fTime, float dt);

private:
	float		time;
	float		timeJump;
	float		maxTime;
	float		playingSpeed;
	float		minSpeed;
	float		maxSpeed;
	bool		isPlaying;

	TimeManager():
	time(0),
	timeJump(200),
	maxTime(0),
	isPlaying(false),
	playingSpeed(1.0),
	minSpeed(0.05),
	maxSpeed(5.0)
	{
	}
};


}
#endif