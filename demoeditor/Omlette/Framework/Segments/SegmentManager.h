#ifndef	INC_SEGMENTMANAGER_H
#define INC_SEGMENTMANAGER_H
#include <vector>
#include <map>
#include "boost\shared_ptr.hpp"
#include <Omlette\Framework\Segments\SegmentFactory.hpp>
#include <Omlette\Framework\Segments\Segment.h>
#include <Omlette\Framework\Segments\SquareSegment.hpp>
#include <Omlette\Framework\Time\TimeManager.h>
#include <Omlette\Interfaces\ITimelineDrawable.h>

namespace Oml
{
template<class T>
class SegmentManager : public ITimelineDrawable
{
public:
	typedef	boost::shared_ptr<SegmentManager<T>>	P;

	inline	static	boost::shared_ptr<SegmentManager<T>>	create()
	{
		return SegmentManager<T>::P(new SegmentManager<T>());
	}



	~SegmentManager();
	T							getValue(float t);
	typename	Segment<T>::P	getSegment(float t);
	void						addKeyFrame(KeyFrame<T>* kf);
	void						addKeyFrame(Egg::Math::float1 time);
	void						deleteKeyFramesByTime(Egg::Math::float1 time);
	void						deleteKeyFrame(KeyFrame<T>* kf);
	typename	Segment<T>::P	createDefaultSegment(KeyFrame<T>* kf1, KeyFrame<T>* kf2);
	void						serialize(pugi::xml_node& node);
	void						deserialize(pugi::xml_node& node);
	void						timelineDraw(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget, ID2D1SolidColorBrush* brush, Egg::Math::float1 timelineMinTime, Egg::Math::float1 timelineMaxTime);
	std::map<T, bool>			getKeys();
	std::vector<KeyFrame<T>*>	getKeyFramesFromTime(Egg::Math::float1 time);

	inline const std::vector<typename Segment<T>::P> const		getSegments(){ return segments; }

private:
	SegmentManager()
	{
	}
	std::vector<typename Segment<T>::P>						segments;
};

}
#endif
