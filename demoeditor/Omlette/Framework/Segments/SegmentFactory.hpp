#ifndef INC_SEGMENTFACTORY_H
#define INC_SEGMENTFACTORY_H

#include <Omlette\Framework\Segments\Segment.h>
#include <pugixml.hpp>
#include <Omlette\Framework\Utility\Deserializer.h>
#include <Omlette\Framework\Segments\LerpSegment.h>
#include <Omlette\Framework\Time\TimeManager.h>

namespace Oml
{
	

class SegmentFactory
{
public:
	static	SegmentFactory&	getInstance()
	{
		static SegmentFactory instance;
		return instance;

	}

	template<typename T>
	typename	Segment<T>::P	createSegment(std::string segmentType,pugi::xml_node& node);
private:
};

template<typename T>
typename	Segment<T>::P	SegmentFactory::createSegment(std::string segmentType,pugi::xml_node& node)
{

	pugi::xml_node firstKeyTime = node.first_child().first_child();;
	pugi::xml_node secondKeyTime = node.first_child().next_sibling().first_child();

	
	float firstTime = atof(firstKeyTime.first_child().value());
	float secondTime = atof(secondKeyTime.first_child().value());

	pugi::xml_node firstValueNode = firstKeyTime.next_sibling();
	pugi::xml_node secondValueNode = secondKeyTime.next_sibling();

	T	firstValue = Deserializer::getInstance().deserializeFromNode<T>(firstValueNode);
	T	secondValue = Deserializer::getInstance().deserializeFromNode<T>(secondValueNode);
	
	KeyFrame<T>* one = new KeyFrame<T>(firstTime,firstValue);
	KeyFrame<T>* two = new KeyFrame<T>(secondTime,secondValue);

	if((float)two->time > TimeManager::getInstance().getMaxTime())
	{
		TimeManager::getInstance().setMaxTime(two->time);
	}
	if(segmentType.compare("Square")==0)
	{
		Segment<T>::P square = SquareSegment<T>::create(one,two);
		return square;
	}
	if(segmentType.compare("Lerp")==0)
	{
		Segment<T>::P lerp = LerpSegment<T>::create(one,two);
		return lerp;
	}
	
}

}
#endif 