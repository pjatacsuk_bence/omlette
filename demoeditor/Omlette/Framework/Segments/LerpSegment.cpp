#include "DXUT.h"
#include <Omlette\Framework\Segments\LerpSegment.h>
#include <Omlette\Framework\Utility\ResourceManager.h>
using namespace Oml;

template<class T>
void LerpSegment<T>::drawSegmentInfo(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget)
{
	renderTarget->FillRectangle(rect, ResourceManager::getInstance().getBrush("transparentGreenBrush"));
}
template <class T>
typename LerpSegment<T>::P LerpSegment<T>::create(KeyFrame<T>* first, KeyFrame<T>* second)
{
	return P(new LerpSegment(first,second));
}

template <class T>
T LerpSegment<T>::getValue(float t)
{
	return keyFrames[0]->key;
}
template<>
inline Egg::Math::float1 LerpSegment<Egg::Math::float1>::getValue(float t)
{
		
	Egg::Math::float1 ft = t;
	Egg::Math::float1 dt = (keyFrames[1]->time - ft) /(keyFrames[1]->time - keyFrames[0]->time); 
	Egg::Math::float1 dt1(dt);
	Egg::Math::float1 dt1i(1-dt);
	return  dt1 * keyFrames[0]->key + keyFrames[1]->key * dt1i;
}
template<>
inline Egg::Math::float3 LerpSegment<Egg::Math::float3>::getValue(float t)
{

	Egg::Math::float1 ft = t;
	Egg::Math::float1 dt = (keyFrames[1]->time - ft) /(keyFrames[1]->time - keyFrames[0]->time); 
	Egg::Math::float3 dt3(dt);
	Egg::Math::float3 dt3i(1-dt);
	return  dt3 * keyFrames[0]->key + keyFrames[1]->key * dt3i;
}
template<>
inline Egg::Math::float4 LerpSegment<Egg::Math::float4>::getValue(float t)
{

	Egg::Math::float1 ft = t;
	Egg::Math::float1 dt = (keyFrames[1]->time - ft) /(keyFrames[1]->time - keyFrames[0]->time); 
	Egg::Math::float4 dt4(dt);
	Egg::Math::float4 dt4i(1-dt);
	return  dt4 * keyFrames[0]->key + keyFrames[1]->key * dt4i;
}

template class LerpSegment<int>;
template class LerpSegment<float>;
template class LerpSegment<class Egg::Math::float1>;
template class LerpSegment<class Egg::Math::float3>;
template class LerpSegment<class Egg::Math::float4>;
template class LerpSegment<std::string>;