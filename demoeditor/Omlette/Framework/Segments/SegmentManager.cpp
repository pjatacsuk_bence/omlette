#include "DXUT.h"
#include <Omlette\Framework\Segments\SegmentManager.h>

using namespace Oml;

template<class T>
SegmentManager<T>::~SegmentManager()
{
	for (auto it = segments.begin(); it != segments.end(); it++){
		(*it)->deleteFirstKeyOnDestruct();
		(*it)->deleteSecondKeyOnDestruct();
	}
	segments.clear();
}
template<class T>
void	SegmentManager<T>::timelineDraw(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget, ID2D1SolidColorBrush* brush, Egg::Math::float1 timelineMinTime, Egg::Math::float1 timelineMaxTime)
{

	using namespace Egg::Math;
	if(segments.size() == 0)
	{
		return;
	}
	for(std::vector<typename Segment<T>::P>::iterator it = segments.begin();
		it != segments.end();
		it++)
	{
		(*it)->timelineDraw(rect, renderTarget, brush, timelineMinTime, timelineMaxTime);
		if(it == (--segments.end()))
		{
			(*it)->getFirstKeyFrame()->timelineDraw(rect,renderTarget,brush, timelineMinTime, timelineMaxTime);
			(*it)->getSecondKeyFrame()->timelineDraw(rect,renderTarget,brush, timelineMinTime, timelineMaxTime);
		}
		else 
		{
			(*it)->getFirstKeyFrame()->timelineDraw(rect,renderTarget,brush, timelineMinTime, timelineMaxTime);
		}
	}
}

template<class T>
void	SegmentManager<T>::serialize(pugi::xml_node& node)
{
	for(std::vector<typename Segment<T>::P>::iterator it = segments.begin();
		it != segments.end();
		it++)
	{
		(*it)->serialize(node);
	}
}

template<class T>
void	SegmentManager<T>::deserialize(pugi::xml_node& node)
{
	for(pugi::xml_node segmentNode = node.first_child();
		segmentNode;
		segmentNode = segmentNode.next_sibling())
	{
		pugi::xml_attribute segmentType = segmentNode.first_attribute();
		segments.push_back(SegmentFactory::getInstance().createSegment<T>(std::string(segmentType.as_string()),segmentNode));
		
	}
}

template<class T>
typename Segment<T>::P SegmentManager<T>::getSegment(float t)
{
	if (segments.size() == 0)
	{
		return NULL;
	}
	if(segments.size() == 1) 
	{
			return segments[0];
	}
	for(std::vector<typename Segment<T>::P>::iterator it = segments.begin();
		it != segments.end();
		it++)
	{
		if(t >= (*it)->getFirstKeyFrame()->time && t <= (*it)->getSecondKeyFrame()->time )
		{
			return (*it);
		}
	}
	return segments.back();
}
template<class T>
T	SegmentManager<T>::getValue(float t)
{
	if(segments.size() == 0 )
	{
		return T();
	}
	if(segments.size() == 1) 
	{
			return segments[0]->getValue(t);
	}
	for(std::vector<typename Segment<T>::P>::iterator it = segments.begin();
		it != segments.end();
		it++)
	{
		if(t >= (*it)->getFirstKeyFrame()->time && t <= (*it)->getSecondKeyFrame()->time )
		{
			return (*it)->getValue(t);
		}
	}
	return segments.back()->getValue(t);
}

template<class T>
void	SegmentManager<T>::deleteKeyFrame(KeyFrame<T>* kf)
{
	for (auto it = segments.begin(); it != segments.end(); it++)
	{
		if((*it)->getFirstKeyFrame() == kf)
		{
			if(it == segments.begin() || segments.size() == 1)
			{
				//ha az els� keyframe-t t�r�lj�k akkor az els� szegmenst is t�r�lni kell
				//vele
				(*it)->deleteSecondKeyOnDestruct();
				(*it)->deleteFirstKeyOnDestruct();
				it = segments.erase(it);
			}
			else
			{
				auto prev = std::prev(it);
				(*prev)->setSecondKeyFrame((*it)->getSecondKeyFrame());
				it = segments.erase(it);
				
			}

		}
		if(it != segments.end() && 
			(*it)->getSecondKeyFrame() == kf)
		{
			auto prev_last = --segments.end();
			if(it == prev_last) 
			{
				(*it)->deleteSecondKeyOnDestruct();
				it = segments.erase(it);
			} 
		}
		if(it == segments.end()) return;
	}
}
template <class T>
void SegmentManager<T>::deleteKeyFramesByTime(Egg::Math::float1 time)
{
	std::vector<KeyFrame<T>*> keyFrames = getKeyFramesFromTime(time);
	for(auto it = segments.begin();
		it != segments.end();
		it++)
	{
		float epsilon = 25;
		if(fabs((*it)->getFirstKeyFrame()->time - time) < epsilon)
		{
			if(it == segments.begin() || segments.size() == 1)
			{
				//ha az els� keyframe-t t�r�lj�k akkor az els� szegmenst is t�r�lni kell
				//vele
				it = segments.erase(it);
			}
			else
			{
				auto prev = std::prev(it);
				(*prev)->setSecondKeyFrame((*it)->getSecondKeyFrame());
				it = segments.erase(it);
				
			}

		}
		if(it != segments.end() && 
			fabs((*it)->getSecondKeyFrame()->time - time) < epsilon)
		{
			if(it == (--segments.end()))
			{
				it = segments.erase(it);
			}
		}
		if(it == segments.end()) return;
	}
}

template<class T>
typename Segment<T>::P 
	SegmentManager<T>::createDefaultSegment(KeyFrame<T>* kf1, KeyFrame<T>* kf2)
{
	return SquareSegment<T>::create(kf1,kf2);	
}

template<>
inline
typename Segment<Egg::Math::float3>::P 
	SegmentManager<Egg::Math::float3>::createDefaultSegment(KeyFrame<Egg::Math::float3>* kf1,
															KeyFrame<Egg::Math::float3>* kf2)
{
	return LerpSegment<Egg::Math::float3>::create(kf1,kf2);
}
template<>
inline
typename Segment<Egg::Math::float1>::P 
	SegmentManager<Egg::Math::float1>::createDefaultSegment(KeyFrame<Egg::Math::float1>* kf1,
															KeyFrame<Egg::Math::float1>* kf2)
{
	return LerpSegment<Egg::Math::float1>::create(kf1,kf2);
}
template<>
inline
typename Segment<Egg::Math::float4>::P 
	SegmentManager<Egg::Math::float4>::createDefaultSegment(KeyFrame<Egg::Math::float4>* kf1,
															KeyFrame<Egg::Math::float4>* kf2)
{
	return LerpSegment<Egg::Math::float4>::create(kf1,kf2);
}

template<class T>
void	SegmentManager<T>::addKeyFrame(Egg::Math::float1 time)
{
	KeyFrame<T>* kf = new KeyFrame<T>(time,T());
	addKeyFrame(kf);
}

template<class T>
void	SegmentManager<T>::addKeyFrame(KeyFrame<T>* kf)
{
	if(segments.size() == 0)
	{
		segments.push_back(createDefaultSegment(new KeyFrame<T>(0,T()),kf));
		if((float)kf->time > TimeManager::getInstance().getMaxTime())
		{
			TimeManager::getInstance().setMaxTime(kf->time);
		}
		return;
	}
	std::vector<typename Segment<T>::P>::iterator it = segments.begin();	
	while(it != segments.end() && (*it)->getSecondKeyFrame()->time < kf->time)
	{
		it++;
	}
	if(it == segments.end())
	{
		//step back to the last segment, and push to the last place
		it--;
		segments.push_back(createDefaultSegment((*it)->getSecondKeyFrame(),kf));
		if((float)kf->time > TimeManager::getInstance().getMaxTime())
		{
			TimeManager::getInstance().setMaxTime(kf->time);
		}
	}
	else if(it == segments.begin() && (*it)->getFirstKeyFrame()->time > kf->time)
	{
		segments.insert(it,createDefaultSegment(kf,(*it)->getFirstKeyFrame()));		
	}
	else
	{
		if((*it)->getSecondKeyFrame()->time > kf->time)
		{
			
			//if we are not at the end, we have to instert and set the new
			//keyFrame values. Like this: it1 ---it2=kf=it.next1--- it.next
			//just like a linkedlist
			KeyFrame<T>* temp = new KeyFrame<T>();
			*temp = *(*it)->getSecondKeyFrame();
			(*it)->setSecondKeyFrame(kf);
			it++;
			if(it == segments.end())
			{
				//we step back to the last one
			segments.insert(it,createDefaultSegment(kf,temp));
			}
			else
			{
			segments.insert(it,createDefaultSegment(kf,(*it)->getFirstKeyFrame()));
			}
		}
	}


}

template<class T>
std::map<T,bool> SegmentManager<T>::getKeys()
{
	std::map<T,bool> ret;	
	for(auto i = segments.begin();i != segments.end();i++)
	{
		ret[(*i)->getFirstKeyFrame()->key]	= true;
		ret[(*i)->getSecondKeyFrame()->key] = true;
	}
	return ret;
}
template<class T>
std::vector<KeyFrame<T>*> SegmentManager<T>::getKeyFramesFromTime(Egg::Math::float1 time)
{
	float epsilon = 25;
	std::vector<KeyFrame<T>*> keyFrames;
	if(segments.size() == 0)
	{
		return keyFrames;
	}
	std::vector<typename Segment<T>::P>::iterator it = segments.begin();	
	while(it != segments.end())
	{
		if(fabs((*it)->getFirstKeyFrame()->time - time) < epsilon )
		{
			keyFrames.push_back((*it)->getFirstKeyFrame());
		}
		if(fabs((*it)->getSecondKeyFrame()->time - time) < epsilon )
		{
			keyFrames.push_back((*it)->getSecondKeyFrame());
		}
		it++;
	}
	return keyFrames;
}

template class SegmentManager<int>;
template class SegmentManager<float>;
template class SegmentManager<class Egg::Math::float1>;
template class SegmentManager<class Egg::Math::float3>;
template class SegmentManager<class Egg::Math::float4>;
template class SegmentManager<std::string>;