#include "DXUT.h"
#include <Omlette\Framework\Segments\Segment.h>
#include <Omlette\Framework\Gui\GuiManager.h>
#include <Omlette\Framework\Gui\GuiTimeline.h>
using namespace Oml;

template<typename U>
KeyFrame<U>::KeyFrame(Egg::Math::float1 t=0, U k=U()):
time(t),
key(k)
{}
	
template<typename U> 
KeyFrame<U>::KeyFrame(const KeyFrame<U>& other)
{
	time = other.time;
	key = other.key;
}

template<typename U>
typename KeyFrame<U>& KeyFrame<U>::operator=(const KeyFrame<U>& other)
{
	time = other.time;
	key = other.key;
	return *this;
}
template<>
void typename KeyFrame<Egg::Math::float4>::timelineDraw(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget, ID2D1SolidColorBrush* brush, Egg::Math::float1 timelineMinTime, Egg::Math::float1 timelineMaxTime)
{
	using namespace Egg::Math;
	if(time < timelineMinTime || time > timelineMaxTime)
	{
		return;
	}
	float1 difTime = timelineMaxTime - timelineMinTime;
	float1 dTime = (time-timelineMinTime) / difTime;
	float1 posX = dTime.lerp(rect.left,rect.right);
	float sideSize = (float)GuiTimeline::HEIGHT_FOR_PROPERTY;
	D2D1_RECT_F float4rect;
	float4rect.top = rect.top;
	float4rect.left = posX - (float1)(sideSize / 2.0);
	float4rect.right = posX + (float1)(sideSize / 2.0);
	float4rect.bottom = rect.bottom;
	renderTarget->FillRectangle(float4rect,GuiManager::getInstance().getSolidOrangeBrush());
}
template<class U>
void typename KeyFrame<U>::timelineDraw(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget, ID2D1SolidColorBrush* brush, Egg::Math::float1 timelineMinTime, Egg::Math::float1 timelineMaxTime)
{
		
	using namespace Egg::Math;
	if(time < timelineMinTime || time > timelineMaxTime)
	{
		return;
	}
	float1 difTime = timelineMaxTime - timelineMinTime;
	float1 dTime = (time-timelineMinTime) / difTime;
	float1 posX = dTime.lerp(rect.left,rect.right);
	float radius = (float)GuiTimeline::HEIGHT_FOR_PROPERTY/2.0;
	D2D1_ELLIPSE ellipse;
	D2D1_POINT_2F point;
	point.x = posX;
	point.y = rect.top + radius;
	ellipse.point = point;
	ellipse.radiusX = radius;
	ellipse.radiusY = radius;
	brush = GuiManager::getInstance().getSolidOrangeBrush();
	renderTarget->FillEllipse(ellipse,brush);
}
template<class U>
void typename KeyFrame<U>::serialize(pugi::xml_node& node)
{
	pugi::xml_node nodeTime = node.append_child("time");
	std::string t = boost::lexical_cast<std::string>(time);
	nodeTime.append_child(pugi::node_pcdata).set_value(t.c_str());

	pugi::xml_node nodeValue = node.append_child("value");
	Serializer::serializeToNode(nodeValue,key);

}
	
template<class T>
Segment<T>::Segment(KeyFrame<T>* first,KeyFrame<T>* second)
{
	keyFrames[0] = first;
	keyFrames[1]  = second;

	deleteFirst = false;
	deleteSecond = false;
}

template<class T>
void Segment<T>::timelineDraw(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget, ID2D1SolidColorBrush* brush, Egg::Math::float1 timelineMinTime, Egg::Math::float1 timelineMaxTime)
{
	using namespace Egg::Math;
	if (keyFrames[0]->time > timelineMaxTime)
	{
		return;
	}
	float1 leftKeyFrameTime = keyFrames[0]->time < timelineMinTime ? timelineMinTime : keyFrames[0]->time;
	float1 rightKeyFrameTime = keyFrames[1]->time > timelineMaxTime ? timelineMaxTime : keyFrames[1]->time;
	float1 difTime = timelineMaxTime - timelineMinTime;

	float1 leftDTime = (leftKeyFrameTime-timelineMinTime) / difTime;
	float1 leftPosX = leftDTime.lerp(rect.left,rect.right);

	float1 rightDTime = (rightKeyFrameTime - timelineMinTime) / difTime;
	float1 rightPosX = rightDTime.lerp(rect.left,rect.right);
	
	D2D1_RECT_F segmentRect = rect;
	segmentRect.left = leftPosX;
	segmentRect.right = rightPosX;
	drawSegmentInfo(segmentRect, renderTarget);

}
template<class T>
Segment<T>::~Segment()
{
	if(keyFrames[0] != NULL && deleteFirst)
	delete keyFrames[0];
	if(keyFrames[1] != NULL && deleteSecond)
	delete keyFrames[1];
}
template<class T>
void	Segment<T>::serialize(pugi::xml_node& node)
{
	
	pugi::xml_node segmentNode = node.append_child("Segment");
	segmentNode.append_attribute("segmentType") = segmentType.c_str();
	
	pugi::xml_node firstKeyFrameNode = segmentNode.append_child("KeyFrame");
	keyFrames[0]->serialize(firstKeyFrameNode);

	pugi::xml_node secondKeyFrameNode = segmentNode.append_child("KeyFrame");
	keyFrames[1]->serialize(secondKeyFrameNode);
}

template struct KeyFrame<int>;
template struct KeyFrame<float>;
template struct KeyFrame<class Egg::Math::float1>;
template struct KeyFrame<class Egg::Math::float3>;
template struct KeyFrame<class Egg::Math::float4>;
template struct KeyFrame<std::string>;

template class Segment<int>;
template class Segment<float>;
template class Segment<class Egg::Math::float1>;
template class Segment<class Egg::Math::float3>;
template class Segment<class Egg::Math::float4>;
template class Segment<std::string>;
/*
template struct KeyFrame<float>
template struct KeyFrame<Egg::Math::float1>;
template struct KeyFrame<Egg::Math::float3>;
template struct KeyFrame<Egg::Math::float4>;
template struct KeyFrame<std::string>;

template class Segment<float>
template class Segment<Egg::Math::float1>;
template class Segment<Egg::Math::float3>;
template class Segment<Egg::Math::float4>;
template class Segment<std::string>;

*/
