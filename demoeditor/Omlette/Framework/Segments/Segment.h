#ifndef INC_SEGMENT_H
#define INC_SEGMENT_H

#include <boost\shared_ptr.hpp>
#include <boost\lexical_cast.hpp>
#include "Math\math.h"
#include <Omlette\Framework\Utility\Logger.h>
#include <Omlette\Framework\Utility\Serializer.h>
#include <Omlette\Framework\Time\TimeManager.h>
#include <Omlette\Interfaces\ITimelineDrawable.h>
#include <D2D1.h>

namespace Oml
{
template<class U>
struct KeyFrame : public ITimelineDrawable
{
	U					key;
	Egg::Math::float1	time;


	KeyFrame(Egg::Math::float1 t=0, U k=U());
	KeyFrame(const KeyFrame<U>& other);
	KeyFrame& operator=(const KeyFrame<U>& other);

	void	timelineDraw(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget, ID2D1SolidColorBrush* brush, Egg::Math::float1 timelineMinTime, Egg::Math::float1 timelineMaxTime);
	void	serialize(pugi::xml_node& node);
	
};
template<class T>
class Segment : public ITimelineDrawable
{

friend class SegmentFactory;
public:	
	typedef	boost::shared_ptr<Segment<T>>	P; 

	virtual	T				getValue(float t)=0;

							~Segment();
	void					serialize(pugi::xml_node& node);
	inline	KeyFrame<T>*	getFirstKeyFrame(){return keyFrames[0];}
	inline	KeyFrame<T>*	getSecondKeyFrame(){return keyFrames[1];}
	inline	void			setFirsKeyFrame(KeyFrame<T>* kf)
	{
		if (keyFrames[0] != NULL){
			delete keyFrames[0];
		}
		keyFrames[0] = kf;
	}

	inline	void			setSecondKeyFrame(KeyFrame<T>* kf)
	{
		if (keyFrames[1] != NULL){
			delete keyFrames[1];
		}
		keyFrames[1] = kf;
	}
	inline	std::string		getSegmentType(){return segmentType;}
	inline  T				getFirstKeyFrameKey(){return keyFrames[0]->key;}
	inline  T				getSecondKeyFrameKey(){return keyFrames[1]->key;}
	inline	void			setFirstKeyFrameKey(T key){keyFrames[0]->key = key;}
	inline	void			setSecondKeyFrameKey(T key){keyFrames[1]->key = key;}

			void			timelineDraw(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget, ID2D1SolidColorBrush* brush, Egg::Math::float1 timelineMinTime, Egg::Math::float1 timelineMaxTime);
	virtual void			drawSegmentInfo(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget){}

	inline void				deleteFirstKeyOnDestruct(){ deleteFirst = true; }
	inline void				deleteSecondKeyOnDestruct(){ deleteSecond = true; }

protected:
	KeyFrame<T>*	keyFrames[2];
	Segment(KeyFrame<T>* first, KeyFrame<T>* second);
	std::string	segmentType;

	bool deleteFirst;
	bool deleteSecond;

};

}
#endif