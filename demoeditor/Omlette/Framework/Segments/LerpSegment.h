#ifndef INC_LERPSEGMENT_H
#define INC_LERPSEGMENT_H

#include <Omlette\Framework\Segments\Segment.h>
namespace Oml
{
template<class T>
class LerpSegment : public Segment<T>
{
public:
		typedef boost::shared_ptr<LerpSegment<T>>	P;

		static	P	create(KeyFrame<T>* first,KeyFrame<T>* second);
				T	getValue(float t);

		virtual void	drawSegmentInfo(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget);
protected:
			LerpSegment(KeyFrame<T>* first,KeyFrame<T>* second):
			Segment<T>(first,second)
			{
				segmentType = "Lerp";
			}
private:
	
};
}
#endif