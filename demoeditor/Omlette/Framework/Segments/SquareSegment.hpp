#ifndef INC_SQUARESEGMENT_H
#define INC_SQUARESEGMENT_H

#include "Segment.h"

namespace Oml
{
template<class T>
class SquareSegment : public Segment<T>
{
public:
		typedef boost::shared_ptr<SquareSegment<T>>	P;

		static	P	create(KeyFrame<T>* first,KeyFrame<T>* second);
				T	getValue(float t);
protected:
			SquareSegment(KeyFrame<T>* first,KeyFrame<T>* second):
			Segment<T>(first,second)
			{
				segmentType = "Square";
			}
private:
	
};

template <class T>
typename SquareSegment<T>::P SquareSegment<T>::create(KeyFrame<T>* first, KeyFrame<T>* second)
{
	return P(new SquareSegment(first,second));
}

	template <class T>
T SquareSegment<T>::getValue(float t)
{
	return keyFrames[0]->key;
}

}
#endif