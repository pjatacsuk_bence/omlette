 #ifndef INC_LOGGER_H
 #define INC_LOGGER_H

#include <iostream>
#include <string>
#include <fstream>

namespace Oml	{
class Logger
{
public:
	static	Logger&	getInstance()
	{
		static	Logger	instance;
		return	instance;

	}

	inline	void	consoleLog(std::string str)
	{
		/*boost::posix_time::ptime now = 
			boost::posix_time::second_clock::local_time();

		std::cout	<<	now.time_of_day().seconds()					<< "/"
					<<	now.time_of_day().minutes()					<< "/" 
					<<	now.time_of_day().hours()					<< "/" 
					<<	static_cast<int>(now.date().month())		<< "/" 
					<<	now.date().day()							<< "/" 
					<<	now.date().year();*/
		std::cout << str << std::endl;
	}

	inline void		fileLog(std::string str)
	{
		 std::ofstream myfile;
			 myfile.open ("example.txt",std::ios::app);
		  myfile  << str << std::endl;
		  myfile.close();
	}
	inline std::string getString(Egg::Math::float4 f)
	{
		return "23232";
	}


private:
	Logger(){}
	Logger(Logger const&);
	void operator=(Logger const&);
};

}
#endif