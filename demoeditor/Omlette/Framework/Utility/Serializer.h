#ifndef INC_SERIALIZER_H
#define INC_SERIALIZER_H
#include <string>
#include <pugixml.hpp>
#include "Math\math.h"
class Serializer
{
public:
	inline static void serializeToNode(pugi::xml_node& node,Egg::Math::float4 f)
	{
		std::string x = boost::lexical_cast<std::string>(f.x);
		std::string y = boost::lexical_cast<std::string>(f.y);
		std::string z = boost::lexical_cast<std::string>(f.z);
		std::string w = boost::lexical_cast<std::string>(f.w);


		pugi::xml_node nodeX = node.append_child("x");
		nodeX.append_child(pugi::node_pcdata).set_value(x.c_str());
		pugi::xml_node nodeY = node.append_child("y");
		nodeY.append_child(pugi::node_pcdata).set_value(y.c_str());
		pugi::xml_node nodeZ = node.append_child("z");
		nodeZ.append_child(pugi::node_pcdata).set_value(z.c_str());
		pugi::xml_node nodeW = node.append_child("w");
		nodeW.append_child(pugi::node_pcdata).set_value(w.c_str());
	}
	inline static void serializeToNode(pugi::xml_node& node,Egg::Math::float3 f)
	{
		std::string x = boost::lexical_cast<std::string>(f.x);
		std::string y = boost::lexical_cast<std::string>(f.y);
		std::string z = boost::lexical_cast<std::string>(f.z);


		pugi::xml_node nodeX = node.append_child("x");
		nodeX.append_child(pugi::node_pcdata).set_value(x.c_str());
		pugi::xml_node nodeY = node.append_child("y");
		nodeY.append_child(pugi::node_pcdata).set_value(y.c_str());
		pugi::xml_node nodeZ = node.append_child("z");
		nodeZ.append_child(pugi::node_pcdata).set_value(z.c_str());
	}
	inline static void	serializeToNode(pugi::xml_node& node,Egg::Math::float1 f)
	{
		
		std::string x = boost::lexical_cast<std::string>(f);
		pugi::xml_node nodeX = node.append_child("x");
		nodeX.append_child(pugi::node_pcdata).set_value(x.c_str());
	}
	inline static void	serializeToNode(pugi::xml_node& node,float f)
	{
		std::string x = boost::lexical_cast<std::string>(f);
		pugi::xml_node nodeX = node.append_child("x");
		nodeX.append_child(pugi::node_pcdata).set_value(x.c_str());
	}

	inline static void	serializeToNode(pugi::xml_node& node,std::string str)
	{
		pugi::xml_node nodeX = node.append_child("string");
		nodeX.append_child(pugi::node_pcdata).set_value(str.c_str());
	}


};

#endif