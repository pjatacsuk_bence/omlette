#include "DXUT.h"
#include "ResourceManager.h"
#include <WindowsX.h>
#include <assimp\ai_assert.h>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#include <assimp\Importer.hpp>
#include <Omlette\Mesh\MeshImporter.h>
#include <Omlette\Components\ComponentFactory.h>
using namespace Oml;


void ResourceManager::init(HWND hWnd)
{
	RECT rcClient, rcWind;
	POINT ptDiff;
	GetClientRect(hWnd, &rcClient);
	GetWindowRect(hWnd, &rcWind);
	ptDiff.x = (rcWind.right - rcWind.left) - rcClient.right;
	ptDiff.y = (rcWind.bottom - rcWind.top) - rcClient.bottom;	
	windowBorderWidth = ptDiff.x;
	windowBorderHeight = ptDiff.y;

	addMien("default", Oml::Mesh::Mien());
	addMien("gui", Oml::Mesh::Mien());
	addMien("ambient", Oml::Mesh::Mien());
	addMien("shadow", Oml::Mesh::Mien());
	addMien("shaded", Oml::Mesh::Mien());
	addMien("unpickable", Oml::Mesh::Mien());
	addMien("pickable", Oml::Mesh::Mien());
	addMien("picked", Oml::Mesh::Mien());

}
ResourceManager::~ResourceManager()
{
/*		geometries_iterator i = geometries.begin();
		geometries_iterator e = geometries.end();

		while(i != e)
		{
			(*i).second.reset();
			i++;
		}
		*/
		geometries.clear();/*
		materials_iterator mi = materials.begin();
		materials_iterator me = materials.end();

		while(mi != me)
		{
			(*mi).second.reset();
			mi++;
		}*/

		materials.clear();
	
		for(auto i = textures.begin();
				 i != textures.end();
				 i++)
		{
			i->second->Release();
		}
		textures.clear();

}

void ResourceManager::addGeometry(std::string name,std::vector<Mesh::Geometry::P> geometry)
{
	auto it = geometries.find(name);
	if(it == geometries.end())
	{
		geometries[name] = geometry;
	}
}

std::vector<Mesh::Geometry::P> ResourceManager::getGeometry(std::string name)
{

	auto it = geometries.find(name);
	if(it == geometries.end())
	{
		return std::vector<Mesh::Geometry::P>();
	}
	return geometries[name];
}


void ResourceManager::addMaterial(std::string name,Mesh::Material::P Material)
{
	materials_iterator it = materials.find(name);
	if(it == materials.end())
	{
		materials[name] = Material;
	}
}

Mesh::Material::P ResourceManager::getMaterial(std::string name)
{

	materials_iterator it = materials.find(name);
	if(it == materials.end())
	{
		return NULL;
	}
	return materials[name];
}

void ResourceManager::setDevice(ID3D11Device* device)
{
	this->device = device;
}

ID3D11Device* ResourceManager::getDevice()
{
	return device;
}

void ResourceManager::setBinder(Mesh::Binder::P binder)
{
	this->binder = binder;
}

Mesh::Binder::P ResourceManager::getBinder()
{
	return binder;
}

void ResourceManager::setEffect(ID3DX11Effect* effect)
{
	this->effect = effect;
}

ID3DX11Effect* ResourceManager::getEffect()
{
	return effect;
}

void ResourceManager::setScreenWidth(int wh)
{
	screenWidth = wh;
}

int ResourceManager::getScreenWidth()
{
	return screenWidth;
}

POINT	ResourceManager::getMouseCoords(LPARAM lParam)
{
	POINT p;
	p.x = GET_X_LPARAM(lParam);
	p.y = GET_Y_LPARAM(lParam);
	return p;
}

ID3D11ShaderResourceView* ResourceManager::getTextureSrv(std::string name)
{
	auto it  = 	textures.find(name);
	if(it == textures.end())
	{
		if(!loadTextureSrv(name))
		{
			return NULL;	
		}
		
	}
	return textures[name];
}

bool ResourceManager::loadTextureSrv(std::string name)
{
	ID3D11ShaderResourceView* temp;
	HRESULT result =
		D3DX11CreateShaderResourceViewFromFileA(
		device,
		name.c_str(),
		NULL, NULL, &temp, NULL); 
	
	if(result != S_OK) return false;
	textures[name] = temp;
	return true;

}

POINT	ResourceManager::getMouseCoordsWithoutBorders(LPARAM lParam)
{
	POINT p;
	p.x = GET_X_LPARAM(lParam);
	p.y = GET_Y_LPARAM(lParam);
	p.x = p.x - windowBorderWidth;
	p.y = p.y - windowBorderHeight;
	return p;
}

void ResourceManager::setScreenHeight(int he)
{
	screenHeight = he;
}

int ResourceManager::getScreenHeight()
{
	return screenHeight;
}
void	ResourceManager::setBrush(ID2D1SolidColorBrush* brush, std::string name)
{
	brushes[name] = brush;
}
ID2D1Brush*	ResourceManager::getBrush(std::string name)
{
	return brushes[name];
}

bool ResourceManager::loadShadedMesh(std::string path, Oml::Mesh::Material::P material)
{
	loadGeometry(path);
	Oml::Mesh::IndexedMesh::P indexedMesh = 
		boost::dynamic_pointer_cast<Oml::Mesh::IndexedMesh,Oml::Mesh::Geometry>
		(ResourceManager::getInstance().getGeometry(path)[0]);

	if(material == NULL)
	{
		SolidColorMaterial::P scm = SolidColorMaterial::create("scm");
		material = scm->getMaterial();
	}
	Oml::Mesh::ShadedMesh::P shadedMesh  =  binder->bindMaterial(material, indexedMesh);
	shadedMeshes[path] = shadedMesh;
	return true;
}

int ResourceManager::getGeometryCount(std::string path)
{

	Assimp::Importer importer;
	const aiScene* assScene = importer.ReadFile(path, aiProcess_Triangulate);
	return assScene->mNumMeshes;
}
bool ResourceManager::loadGeometry(std::string path)
{
		Assimp::Importer importer;
		const aiScene* assScene = importer.ReadFile(path, aiProcess_Triangulate);
		
		if(getGeometry(path).size() == 0)
		{

			std::vector<Mesh::Geometry::P> gms;
			for(int i = 0; i < assScene->mNumMeshes; i++)
			{
				gms.push_back(Oml::Mesh::MeshImporter::fromAiMesh(device, assScene->mMeshes[i]));
			}
			addGeometry(path,gms);
			return true;
		}
		return false;
		
}

Oml::Mesh::ShadedMesh::P ResourceManager::getShadedMesh(std::string path)
{
	return shadedMeshes[path];
}

void ResourceManager::addMien(std::string name, Oml::Mesh::Mien& mien)
{
	miens[name] = mien;
}

Oml::Mesh::Mien& ResourceManager::getMien(std::string name)
{
	return miens[name];
}