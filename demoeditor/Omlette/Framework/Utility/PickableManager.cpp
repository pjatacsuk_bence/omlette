#include "DXUT.h"
#include "PickableManager.h"
#include <Omlette\Framework\Utility\ResourceManager.h>
#include <Omlette\Entities\EntityManager.h>
#include <Omlette\Framework\Gui\CommandManager.h>


using namespace Oml;
PickableManager::PickableManager()
{
}


PickableManager::~PickableManager()
{
	pickTexture->Release();
	pickTextureRtv->Release();
	pickTextureSrv->Release();

	dst->Release();
	dstDsv->Release();	
}

void PickableManager::init(ID3D11Device* device,Mesh::Binder::P binder,ID3DX11Effect* effect)
{
	this->binder = binder;
	ID3DX11EffectPass* pickerPassMesh = effect->GetTechniqueByName("picker")->GetPassByName("picker");
	pickerMaterialMesh = Oml::Mesh::Material::create(pickerPassMesh,0);
	ResourceManager::getInstance().addMaterial("pickerMaterialMesh", pickerMaterialMesh);

	ID3DX11EffectPass* pickedPassMesh = effect->GetTechniqueByName("basic")->GetPassByName("SolidColor");
	pickedMaterialMesh = Oml::Mesh::Material::create(pickedPassMesh,0);
	ResourceManager::getInstance().addMaterial("pickedMaterialMesh", pickedMaterialMesh);

	ID3DX11EffectPass* pickerPassMeshNoDepth = effect->GetTechniqueByName("picker")->GetPassByName("pickerNoDepth");
	pickerMaterialMeshNoDepth = Oml::Mesh::Material::create(pickerPassMeshNoDepth,0);
	ResourceManager::getInstance().addMaterial("pickerMaterialMeshNoDepth", pickerMaterialMeshNoDepth);

	ID3DX11EffectPass* pickerPassQuad = effect->GetTechniqueByName("picker")->GetPassByName("pickerFullQuad");
	pickerMaterialQuad = Oml::Mesh::Material::create(pickerPassQuad,0);
	ResourceManager::getInstance().addMaterial("pickerMaterialQuad", pickerMaterialQuad);

	CD3D11_TEXTURE2D_DESC pickTextureDesc(	DXGI_FORMAT_R32G32B32A32_FLOAT,
											ResourceManager::getInstance().getScreenWidth(),
											ResourceManager::getInstance().getScreenHeight(),
											1,
											1,
											D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE
											);

	device->CreateTexture2D(&pickTextureDesc,NULL,&pickTexture);

	CD3D11_TEXTURE2D_DESC readTextureDesc(	DXGI_FORMAT_R32G32B32A32_FLOAT,
										ResourceManager::getInstance().getScreenWidth(),
										ResourceManager::getInstance().getScreenHeight(),
										1,
										1,
										0,
										D3D11_USAGE_STAGING,
										D3D11_CPU_ACCESS_READ
										);

	device->CreateTexture2D(&readTextureDesc,NULL,&readTexture);
	D3D11_RENDER_TARGET_VIEW_DESC rtvDesc;
	rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
	rtvDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	rtvDesc.Texture2D.MipSlice = 0;
	device->CreateRenderTargetView(pickTexture, &rtvDesc, &pickTextureRtv);

	D3D11_SHADER_RESOURCE_VIEW_DESC	pickTextureSrvDesc;
	pickTextureSrvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	pickTextureSrvDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	pickTextureSrvDesc.Texture2D.MipLevels = 1;
	pickTextureSrvDesc.Texture2D.MostDetailedMip = 0;
	device->CreateShaderResourceView(pickTexture, &pickTextureSrvDesc, &pickTextureSrv);


	D3D11_TEXTURE2D_DESC dstDesc;
	dstDesc.ArraySize = 1;
	dstDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	dstDesc.CPUAccessFlags = 0;
	dstDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	dstDesc.Height = ResourceManager::getInstance().getScreenHeight();
	dstDesc.MipLevels = 1;
	dstDesc.MiscFlags = 0;
	dstDesc.SampleDesc.Count = 1;
	dstDesc.SampleDesc.Quality = 0;
	dstDesc.Usage = D3D11_USAGE_DEFAULT;
	dstDesc.Width = ResourceManager::getInstance().getScreenWidth();
	device->CreateTexture2D(&dstDesc, NULL, &dst);

	D3D11_DEPTH_STENCIL_VIEW_DESC dsvDesc;
	dsvDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	dsvDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	dsvDesc.Flags = 0;
	dsvDesc.Texture2D.MipSlice = 0;
	device->CreateDepthStencilView(dst, &dsvDesc, &dstDsv);
 
}

void PickableManager::addPickable(IPickable* p)
{
	pickables[p->unqiueID] = p;
}
void PickableManager::removePickable(IPickable* p)
{
	auto it = pickables.find(p->unqiueID);
	if (it != pickables.end())
	{
		pickables.erase(it);
	}
}

IPickable* PickableManager::getPickable(uint32_t i)
{
	return pickables[i];
}

void PickableManager::setEntityFromLastPicking(ID3D11DeviceContext* context)
{

	picking = false;

	D3D11_BOX dataBox;
	dataBox.left = mouseX;
	dataBox.right = mouseX+1;
	dataBox.top = mouseY;
	dataBox.bottom = mouseY+1;
	dataBox.front = 0;
	dataBox.back = 1;

	context->CopySubresourceRegion(readTexture,0,0,0,0,pickTexture,0,&dataBox);
	D3D11_MAPPED_SUBRESOURCE mappedSubResource;
	context->Map(readTexture,0,D3D11_MAP_READ,0,&mappedSubResource);

	float r = ((float*)mappedSubResource.pData)[0];
	float g = ((float*)mappedSubResource.pData)[1];
	float b = ((float*)mappedSubResource.pData)[2];
	float a = ((float*)mappedSubResource.pData)[3];
	
	context->Unmap(readTexture,0);

	using namespace Egg::Math;

	uint32_t  uniqueID = IPickable::getUnqiueIDFromColor(float4(r,g,b,a));
	if (CommandManager::getInstance().getCommand() != CommandManager::Command::None)
	{
		return;
	}
	if(uniqueID == IPickable::UNPICKABLE )
	{
		//ez nem kiv�laszhat�, minden marad a r�gi
		return;
	}
	if (uniqueID == IPickable::SpecialID::transformX)
	{
		if (CommandManager::getInstance().getCommand() == CommandManager::Command::None)
		{
			CommandManager::getInstance().setCommand(CommandManager::Command::DraggingEntityPositionX);
		}
		return;
	}
	if (uniqueID == IPickable::SpecialID::transformY)
	{
		if (CommandManager::getInstance().getCommand() == CommandManager::Command::None)
		{
			CommandManager::getInstance().setCommand(CommandManager::Command::DraggingEntityPositionY);
		}
		return;
	}
	if (uniqueID == IPickable::SpecialID::transformZ)
	{
		if (CommandManager::getInstance().getCommand() == CommandManager::Command::None)
		{
			CommandManager::getInstance().setCommand(CommandManager::Command::DraggingEntityPositionZ);
		}
		return;
	}
	if (!entityPicking)
	{
		return;
	}
	picked = NULL;
	auto it = pickables.find(uniqueID);
	if (it != pickables.end())
	{
		picked = pickables[uniqueID];
	}
	if(picked == NULL)
	{
		pickedEntity = NULL;		
	}
	else
	{	
		pickedEntity = EntityManager::getInstance().getEntity(picked->parent->getEntityName());
	}



}

void PickableManager::draw(RenderParameters renderParameters)
{
	
	float clearColor[4] = 
		{ 0.0f, 0.0f, 0.0f, 0.0f };
	ID3D11RenderTargetView* defaultRtv = 	DXUTGetD3D11RenderTargetView();
	ID3D11DepthStencilView* defaultDsv = 	DXUTGetD3D11DepthStencilView();

	D3D11_VIEWPORT fbViewport;
	UINT nViewports = 1;
	renderParameters.context->RSGetViewports(&nViewports, &fbViewport);

	D3D11_VIEWPORT rttViewport;
	rttViewport.Height = ResourceManager::getInstance().getScreenHeight();
	rttViewport.MaxDepth = 1;
	rttViewport.MinDepth = 0;
	rttViewport.TopLeftX = 0;
	rttViewport.TopLeftY = 0;
	rttViewport.Width = ResourceManager::getInstance().getScreenWidth();
	renderParameters.context->RSSetViewports(1, &rttViewport);

	renderParameters.context->OMSetRenderTargets(1,&pickTextureRtv, dstDsv);
	renderParameters.context->ClearRenderTargetView(pickTextureRtv, clearColor);
	renderParameters.context->ClearDepthStencilView(dstDsv, D3D11_CLEAR_DEPTH, 1.0, 0);

	iterator i = pickables.begin();
	iterator e = pickables.end();

	while(i != e)
	{
		if(i->second !=  NULL)
		{
			Egg::Math::float4 color = Egg::Math::float4(0, 0, 0, 1);
			renderParameters.effect->GetVariableByName("pickerColor")->AsVector()->SetFloatVector((float*)&(color));
			if (i->second->drawable != NULL)
			{
				if (i->second->drawable->isVisible())
				{
					color = i->second->getUniqueColor();
					renderParameters.effect->GetVariableByName("pickerColor")->AsVector()->SetFloatVector((float*)&(color));
					i->second->drawable->draw(renderParameters);
				}
			}
		}
		i++;
	}

	renderParameters.context->OMSetRenderTargets(1, &defaultRtv, defaultDsv); 
}

void PickableManager::handlePicking(ID3D11DeviceContext* context, ID3DX11Effect* effect)
{
	RenderParameters renderParameters;
	renderParameters.context = context;
	renderParameters.effect = effect;
	renderParameters.mien = &ResourceManager::getInstance().getMien("pickable");
	if(picking)
	{
		draw(renderParameters);
		setEntityFromLastPicking(context);
	}
	if (picked != NULL)
	{
		renderParameters.mien = &ResourceManager::getInstance().getMien("picked");
		renderParameters.effect->GetVariableByName("color")->AsVector()->SetFloatVector((float*)&(Egg::Math::float4(0.0, 1.0, 0.0, 1.0)));
		picked->drawable->draw(renderParameters);
	}
}

void PickableManager::processMessage(HWND hWnd, unsigned uMsg, WPARAM wParam, LPARAM lParam)
{
	if(uMsg ==WM_LBUTTONDOWN  &&
		CommandManager::getInstance().getCommand() == CommandManager::Command::None)
	{
		int x  = LOWORD(lParam);
		int y  = HIWORD(lParam);
		bool ePicking = wParam & MK_CONTROL;
		setPicking(true,x,y,ePicking);
	}
}