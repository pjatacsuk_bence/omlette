#ifndef INC_LIGHTMANAGER_H
#define INC_LIGHTMANAGER_H

#include <Omlette\Components\LightComponent.h>
namespace Oml
{
class LightManager
{
friend class LightComponent;
public:
	static LightManager& getInstance()
	{
		static LightManager instance;
		return instance;
	}

	inline	int		getLightsNumber(){ return lights.size(); }
	void			applyLight(int i);
private:
	void	addLight(LightComponent* light);

	std::vector<LightComponent*> lights;
};

}
#endif