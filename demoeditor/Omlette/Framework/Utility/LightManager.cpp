#include "DXUT.h"

#include <Omlette\Framework\Utility\LightManager.h>
#include <Omlette\Framework\Utility\ResourceManager.h>

using namespace Oml;
void LightManager::addLight(LightComponent* light)
{
	lights.push_back(light);
}

void LightManager::applyLight(int i)
{
		LightComponent* currentLight = lights[i];
		ID3DX11Effect* effect = ResourceManager::getInstance().getEffect();


		effect->GetVariableByName("lightPos")->AsVector()->SetFloatVector((float*)(&currentLight->currentLightPos));
		Egg::Math::float4 lightColor(currentLight->currentLightIntensity);
		lightColor.w = 1.0;
//	effect->GetVariableByName("lightColor")->AsVector->SetFloatVector((float*)(&lightColor));


	
}