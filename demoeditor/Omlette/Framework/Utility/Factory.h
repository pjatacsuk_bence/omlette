#ifndef INC_FACTORY_H
#define INC_FACTORY_H

#include <Omlette\Interfaces\IGuiElement.h>
#include <Omlette\Framework\Properties\IPropertyAncestor.h>
#include <vector>

namespace Oml {
class Factory {
public:
	static Factory& getInstance() {
		static Factory instance;
		return instance;
	}

	std::vector<IGuiElement::P> createGuiTimelineSegment(IPropertyAncestor::P p);
private:
	Factory();

};
};

#endif // INC_FACTORY_H
