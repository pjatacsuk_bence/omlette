#ifndef INC_DESERIALIZER_H
#define INC_DESERIALIZER_H

class Deserializer
{
public:
	static	Deserializer&	getInstance()
	{
		static Deserializer instance;
		return instance;

	}

	template<typename T>
	T	deserializeFromNode(pugi::xml_node& node);
private:

};

template<>
inline Egg::Math::float4 Deserializer::deserializeFromNode<Egg::Math::float4>(pugi::xml_node& node)
{
	pugi::xml_node x = node.first_child();
	pugi::xml_node y = x.next_sibling();
	pugi::xml_node z = y.next_sibling();
	pugi::xml_node w = z.next_sibling();

	std::string x_str = x.first_child().value();
	std::string y_str = y.first_child().value();
	std::string z_str = z.first_child().value();
	std::string w_str = w.first_child().value();

	return Egg::Math::float4(atof(x_str.c_str()),
							 atof(y_str.c_str()),
							 atof(z_str.c_str()),
							 atof(w_str.c_str()));


}

template<>
inline Egg::Math::float3 Deserializer::deserializeFromNode<Egg::Math::float3>(pugi::xml_node& node)
{
	pugi::xml_node x = node.first_child();
	pugi::xml_node y = x.next_sibling();
	pugi::xml_node z = y.next_sibling();

	std::string x_str = x.first_child().value();
	std::string y_str = y.first_child().value();
	std::string z_str = z.first_child().value();

	return Egg::Math::float3(atof(x_str.c_str()),
							 atof(y_str.c_str()),
							 atof(z_str.c_str()));


}

template<>
inline	Egg::Math::float1	Deserializer::deserializeFromNode<Egg::Math::float1>(pugi::xml_node& node)
{
	pugi::xml_node x = node.first_child();
	std::string x_str = x.first_child().value();
	return Egg::Math::float1(atof(x_str.c_str()));
}
template<>
inline	float	Deserializer::deserializeFromNode<float>(pugi::xml_node& node)
{
	pugi::xml_node x = node.first_child();
	std::string x_str = x.first_child().value();
	return atof(x_str.c_str());
}

template<>
inline	int	Deserializer::deserializeFromNode<int>(pugi::xml_node& node)
{
	pugi::xml_node x = node.first_child();
	std::string x_str = x.first_child().value();
	return atoi(x_str.c_str());
}
template<>
inline	unsigned int	Deserializer::deserializeFromNode<unsigned int>(pugi::xml_node& node)
{
	pugi::xml_node x = node.first_child();
	std::string x_str = x.first_child().value();
	return atoi(x_str.c_str());
}
template<>
inline	std::string	Deserializer::deserializeFromNode<std::string>(pugi::xml_node& node)
{
	pugi::xml_node x = node.first_child();
	return x.first_child().value();
}
#endif