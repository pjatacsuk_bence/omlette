#ifndef INC_RESOURCEMANAGER_H
#define INC_RESOURCEMANAGER_H

#include <map>
#include <Omlette\Mesh\Geometry.h>
#include <Omlette\Mesh\Material.h>
#include <pugixml.hpp>
#include <Omlette\Mesh\Binder.h>
#include <D2D1.h>
#include "d3dx11effect.h"
#include <Omlette\Mesh\FlipMesh.h>
namespace Oml{
class ResourceManager
{
public:
	static ResourceManager&		getInstance()
	{
		static ResourceManager instance;
		return instance;
	}
	
							~ResourceManager();
	void					init(HWND hWnd);


	void					addMaterial(std::string name, Mesh::Material::P Material);
	Mesh::Material::P		getMaterial(std::string name);


	void					setDevice(ID3D11Device* device);
	ID3D11Device*			getDevice();

	void					setBinder(Mesh::Binder::P binder);
	Mesh::Binder::P			getBinder();
	
	void					setEffect(ID3DX11Effect* effect);
	ID3DX11Effect*			getEffect();

	void					setBrush(ID2D1SolidColorBrush* brush, std::string name);
	ID2D1Brush*				getBrush(std::string name) ;

	void					addMien(std::string name, Mesh::Mien& mien);
	Mesh::Mien&				getMien(std::string) ;

	void					setScreenWidth(int wh);
	int						getScreenWidth();

	void					setScreenHeight(int he);
	int						getScreenHeight();

	POINT					getMouseCoordsWithoutBorders(LPARAM lParam);
	POINT					getMouseCoords(LPARAM lParam);

	Mesh::ShadedMesh::P		getShadedMesh(std::string path);

	ID3D11ShaderResourceView*	getTextureSrv(std::string name);
	bool						loadTextureSrv(std::string name);
	bool						loadShadedMesh(std::string path, Oml::Mesh::Material::P material = NULL);
	bool						loadGeometry(std::string path);
	int							getGeometryCount(std::string path);

	void								addGeometry(std::string name, std::vector<Mesh::Geometry::P> geometry);
	std::vector<Mesh::Geometry::P>		getGeometry(std::string name);

private:
							ResourceManager():
							screenWidth(1440),
							screenHeight(900){}

	Mesh::Binder::P			binder;

	
	typedef std::map<std::string,Mesh::Geometry::P>::iterator geometries_iterator;
	typedef std::map<std::string,Mesh::Material::P>::iterator materials_iterator;


	int						screenHeight;
	int						screenWidth;

	int						windowBorderWidth;
	int						windowBorderHeight;

	ID3DX11Effect*			effect;
	ID3D11Device*			device;

	std::map<std::string,std::vector<Mesh::Geometry::P>>	geometries;
	std::map<std::string,Mesh::Material::P>					materials;
	std::map<std::string,ID3D11ShaderResourceView*>			textures;
	std::map<std::string,ID2D1Brush*>						brushes;
	std::map<std::string, Mesh::ShadedMesh::P>				shadedMeshes;
	std::map<std::string, Mesh::Mien>						miens;
};


}
#endif