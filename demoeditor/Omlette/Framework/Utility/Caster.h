#ifndef INC_CASTER_H
#define INC_CASTER_H

#include <Omlette\Framework\Properties\Property.h>
#include <Omlette\Components\MeshRendererComponent.h>
#include <Omlette\Components\MaterialComponent.h>
#include <boost\any.hpp>
#include <boost\variant\variant.hpp>

namespace Oml
{

class Caster
{
public:
	static Caster& getInstance()
	{
		static Caster instance;
		return instance;
	}

	typedef boost::variant<Property<float>::P, 
						   Property<int>::P,
						   Property<unsigned int>::P,
						   Property<std::string>::P,
						   Property<Egg::Math::float1>::P,
						   Property<Egg::Math::float3>::P,
						   Property<Egg::Math::float4>::P> PropertyTypes ;
	template<typename T>
	T	getPropertyCasted(IPropertyAncestor::P p);

	template<typename T>
	T	getComponentCasted(Oml::Component::P component);

	auto getPropertyCasted(IPropertyAncestor::P p) -> decltype(p->getPropertyType()){
		return p->getPropertyType();
	} 
	boost::any getPropertyCastedToAny(IPropertyAncestor::P p)
	{
		switch (p->getType())
		{
		case Type::FLOAT:
			return boost::dynamic_pointer_cast<Property<Egg::Math::float1>,IPropertyAncestor>(p);
			break;
		case Type::FLOAT3:
			return boost::dynamic_pointer_cast<Property<Egg::Math::float3>,IPropertyAncestor>(p);
			break;
		case Type::FLOAT4:
			return boost::dynamic_pointer_cast<Property<Egg::Math::float4>,IPropertyAncestor>(p);
			break;
		case Type::STRING:
			return boost::dynamic_pointer_cast<Property<std::string>,IPropertyAncestor>(p);
			break;
		case Type::UINT32:
			return boost::dynamic_pointer_cast<Property<int>,IPropertyAncestor>(p);
			break;
		case Type::NONE:
			return NULL;
			break;
		}
}

	
private:
};	

using namespace Oml;
template<typename T>
T	Caster::getComponentCasted(Oml::Component::P component)
{
}

template<>
inline TransformComponent::P Caster::getComponentCasted<TransformComponent::P>(Oml::Component::P component)
{
	if (component->getComponentTypeName().compare("TransformComponent") == 0)
	{
		return boost::dynamic_pointer_cast<TransformComponent, Component>(component);
	}
	else
	{
		return NULL;
	}
}
template<>
inline MaterialComponent::P Caster::getComponentCasted<MaterialComponent::P>(Oml::Component::P component)
{
	if (component->getComponentTypeName().compare("MaterialComponent") == 0)
	{
		return boost::dynamic_pointer_cast<MaterialComponent, Component>(component);
	}
	else
	{
		return NULL;
	}
}
template<>
inline MeshRendererComponent::P Caster::getComponentCasted<MeshRendererComponent::P>(Oml::Component::P component)
{
	if (component->getComponentTypeName().compare("MeshRendererComponent") == 0)
	{
		return boost::dynamic_pointer_cast<MeshRendererComponent, Component>(component);
	}
	else
	{
		return NULL;
	}
}

template<typename T>
T	Caster::getPropertyCasted(IPropertyAncestor::P p)
{
	return NULL;
}

template<>
inline Property<Egg::Math::float1>::P	Caster::getPropertyCasted<Property<Egg::Math::float1>::P>(IPropertyAncestor::P p)
{
	if(p->getType() == FLOAT)
	{
		return boost::dynamic_pointer_cast<Property<Egg::Math::float1>,IPropertyAncestor>(p);
	}
	else
	{
		return NULL;
	}
}
template<>
inline Property<Egg::Math::float3>::P	Caster::getPropertyCasted<Property<Egg::Math::float3>::P>(IPropertyAncestor::P p)
{
	if(p->getType() == FLOAT3)
	{
		return boost::dynamic_pointer_cast<Property<Egg::Math::float3>,IPropertyAncestor>(p);
	}
	else
	{
		return NULL;
	}
}
template<>
inline Property<Egg::Math::float4>::P	Caster::getPropertyCasted<Property<Egg::Math::float4>::P>(IPropertyAncestor::P p)
{
	if(p->getType() == FLOAT4)
	{
		return boost::dynamic_pointer_cast<Property<Egg::Math::float4>,IPropertyAncestor>(p);
	}
	else
	{
		return NULL;
	}
}

template<>
inline Property<float>::P	Caster::getPropertyCasted<Property<float>::P>(IPropertyAncestor::P p)
{
	if(p->getType() == FLOAT)
	{
		return boost::dynamic_pointer_cast<Property<float>,IPropertyAncestor>(p);
	}
	else
	{
		return NULL;
	}
}

template<>
inline Property<std::string>::P	Caster::getPropertyCasted<Property<std::string>::P>(IPropertyAncestor::P p)
{
	if(p->getType() == STRING)
	{
		return boost::dynamic_pointer_cast<Property<std::string>,IPropertyAncestor>(p);
	}
	else
	{
		return NULL;
	}
}

template<>
inline Property<unsigned int>::P	Caster::getPropertyCasted<Property<unsigned int>::P>(IPropertyAncestor::P p)
{
	if(p->getType() == UINT32)
	{
		return boost::dynamic_pointer_cast<Property<unsigned int>,IPropertyAncestor>(p);
	}
	else
	{
		return NULL;
	}
}
}


#endif