#ifndef INC_PICKABLEMANAGER_H
#define INC_PICKABLEMANAGER_H
#include <Omlette\Interfaces\IDrawable.h>
#include <stdint.h>
#include <Omlette\Interfaces\IPickable.h>
#include <Omlette\Mesh\Binder.h>
#include <Omlette\Entities\Entity.h>

namespace Oml
{
class IPickable;
class PickableManager : public Oml::IDrawable
{
public:
	PickableManager();
	~PickableManager();
	static PickableManager&	getInstance()
	{
		static PickableManager instance;
		return instance;
	}

	void				init(ID3D11Device* device, Mesh::Binder::P binder,ID3DX11Effect* effect);

	void				addPickable(IPickable* p);
	IPickable*			getPickable(uint32_t i);
	void				removePickable(IPickable* p);

	inline	void		setPicking(bool pck, int x, int y, bool entityPicking)
	{ 
		picking = pck;
		mouseX = x;
		mouseY = y; 
		this->entityPicking = entityPicking; 
	}
	inline	bool		isPicking(){return picking;}

	

	inline	Entity::P	getPickedEntity(){return pickedEntity;}

	void				draw(RenderParameters renderParameters);
	void				handlePicking(ID3D11DeviceContext* context, ID3DX11Effect* effect);
	void				processMessage(HWND hWnd, unsigned uMsg, WPARAM wParam, LPARAM lParam);
private:

	void				setEntityFromLastPicking(ID3D11DeviceContext* context);


	Mesh::Binder::P					binder;	
	Mesh::Material::P				pickerMaterialMesh;
	Mesh::Material::P				pickerMaterialQuad;
	Mesh::Material::P				pickerMaterialMeshNoDepth;
	Mesh::Material::P				pickedMaterialMesh;
	std::map<uint32_t,IPickable*>	pickables;
	IPickable*						picked;
	typedef std::map<uint32_t,IPickable*>::iterator iterator;

	ID3D11Texture2D*				pickTexture;
	ID3D11RenderTargetView*			pickTextureRtv;
	ID3D11ShaderResourceView*		pickTextureSrv;

	ID3D11Texture2D*				dst;
	ID3D11DepthStencilView* 		dstDsv;

	ID3D11Texture2D*				readTexture;

	bool							picking;
	bool							entityPicking;
	int								mouseX;
	int								mouseY;

	Entity::P						pickedEntity;
};
}
#endif