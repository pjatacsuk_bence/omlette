#include "DXUT.h"
#include <Omlette\Framework\Utility\Factory.h>
#include <Omlette\Framework\Gui\GuiTimelineSegment.h>
#include <Omlette\Framework\Properties\Property.h>
using namespace Oml;

Factory::Factory(){

}
std::vector<IGuiElement::P> Factory::createGuiTimelineSegment(IPropertyAncestor::P p)
{
	std::vector<IGuiElement::P> guiSegments;
	if (p->getType() == Type::FLOAT) {
		Property<Egg::Math::float1>::P prop = boost::dynamic_pointer_cast<Property<Egg::Math::float1>, IPropertyAncestor>(p);
		auto segments = prop->getSegmentManager()->getSegments();
		for (auto it = segments.begin(); it != segments.end(); it++){
			guiSegments.push_back(GuiTimelineSegment<Egg::Math::float1>::create(*it, prop));
		}
	} else if (p->getType() == Type::FLOAT3) {
		Property<Egg::Math::float3>::P prop = boost::dynamic_pointer_cast<Property<Egg::Math::float3>, IPropertyAncestor>(p);
		auto segments = prop->getSegmentManager()->getSegments();
		for (auto it = segments.begin(); it != segments.end(); it++){
			guiSegments.push_back(GuiTimelineSegment<Egg::Math::float3>::create(*it, prop));
		}
	} else if (p->getType() == Type::FLOAT4) {
		Property<Egg::Math::float4>::P prop = boost::dynamic_pointer_cast<Property<Egg::Math::float4>, IPropertyAncestor>(p);
		auto segments = prop->getSegmentManager()->getSegments();
		for (auto it = segments.begin(); it != segments.end(); it++){
			guiSegments.push_back(GuiTimelineSegment<Egg::Math::float4>::create(*it, prop));
		}
	} else if (p->getType() == Type::INT32) {
		Property<int>::P prop = boost::dynamic_pointer_cast<Property<int>, IPropertyAncestor>(p);
		auto segments = prop->getSegmentManager()->getSegments();
		for (auto it = segments.begin(); it != segments.end(); it++){
			guiSegments.push_back(GuiTimelineSegment<int>::create(*it, prop));
		}
	} /*else if (p->getType() == Type::UINT32) {
		Property<unsigned int>::P prop = boost::dynamic_pointer_cast<Property<unsigned int>, IPropertyAncestor>(p);
		auto segments = prop->getSegmentManager()->getSegments();
		for (auto it = segments.begin(); it != segments.end(); it++){
			guiSegments.push_back(GuiTimelineSegment<unsigned int>::create(*it, prop));
		}
	}*/ else if (p->getType() == Type::STRING) {
		Property<std::string>::P prop = boost::dynamic_pointer_cast<Property<std::string>, IPropertyAncestor>(p);
		auto segments = prop->getSegmentManager()->getSegments();
		for (auto it = segments.begin(); it != segments.end(); it++){
			guiSegments.push_back(GuiTimelineSegment<std::string>::create(*it, prop));
		}
	} 
	return guiSegments;


}