#include "DXUT.h"
#include <Omlette\Components\SingleSolidColorMaterial.h>
#include <Omlette\Framework\Utility\ResourceManager.h>

using  namespace Oml;

SingleSolidColorMaterial::SingleSolidColorMaterial(std::string name, Egg::Math::float4 color, bool depthTest):
MaterialComponent(name,"SingleSolidColorMaterial"),
color(color)
{
	technique = "basic";
	std::string ambientPassName;
	if (depthTest)
	{
		pass = "SolidColor";
		ambientPassName = "ambientSingleSolidColor";
	}
	else
	{
		pass = "SolidColorNoDepth";
		ambientPassName = "ambientSingleSolidColorNoDepth";
		pickerMaterial = ResourceManager::getInstance().getMaterial("pickerMaterialMeshNoDepth");
	}
	

	ID3DX11EffectPass* shadowPass = ResourceManager::getInstance().getEffect()
		->GetTechniqueByName("shadow")->GetPassByName("shadow");
	shadowMaterial =  Oml::Mesh::Material::create(shadowPass,0);

	ID3DX11EffectPass* scmPass = ResourceManager::getInstance().getEffect()
		->GetTechniqueByName(technique.c_str())->GetPassByName(pass.c_str());
	material = Oml::Mesh::Material::create(scmPass,0);

	ID3DX11EffectPass* ambientPass = ResourceManager::getInstance().getEffect()
		->GetTechniqueByName("ambientScene")->GetPassByName(ambientPassName.c_str());
	ambientMaterial =  Oml::Mesh::Material::create(ambientPass,0);
}

void	SingleSolidColorMaterial::serializeProperties(pugi::xml_node& node)
{
}

void	SingleSolidColorMaterial::deserializeProperties(pugi::xml_node& node)
{

}
void SingleSolidColorMaterial::applyVariables(ID3DX11Effect* effect)
{
	effect->GetVariableByName("color")->AsVector()->SetFloatVector((float*)(& (color)));
}