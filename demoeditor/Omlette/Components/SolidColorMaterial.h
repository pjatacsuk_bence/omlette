#ifndef	INC_SOLIDCOLORMATERIAL_H
#define INC_SOLIDCOLORMATERIAL_H
#include <Omlette\Components\MaterialComponent.h>
#include <boost\shared_ptr.hpp>
namespace Oml
{
class SolidColorMaterial : public MaterialComponent
{
friend class ComponentFactory;
public:
	typedef boost::shared_ptr<SolidColorMaterial> P;
	static P	create(std::string name){return P(new SolidColorMaterial(name));}	
	void					serializeProperties(pugi::xml_node&	node);
	void					deserializeProperties(pugi::xml_node&	node);
	
protected:
									SolidColorMaterial(std::string name="");
	
	Property<Egg::Math::float4>::P	color;

};	

}

#endif