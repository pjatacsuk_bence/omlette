#ifndef INC_DUMMYCOMPONENT_H
#define INC_DUMMYCOMPONENT_H

#include "Omlette\Interfaces\IComponentManager.h"
#include "Omlette\Interfaces\IDrawable.h"
#include "Omlette\Interfaces\IUpdateable.h"
#include "Omlette\Interfaces\IGuiDrawable.h"
#include "Omlette\Interfaces\ISerializable.h"
#include "Omlette\Interfaces\ITimelineDrawable.h"
#include <boost\shared_ptr.hpp>


namespace Oml {
class DummyComponent : public Component,public IUpdateable,public ISerializable,public IDrawable
{
friend class ComponentFactory;
public:
	typedef boost::shared_ptr<DummyComponent> P;
	
						~DummyComponent();
	virtual	void		draw(RenderParameters renderParameters);
	virtual	void		update(float t);
	virtual	void		guiDraw();
	virtual	void		serialize(pugi::xml_node&	node);
	virtual	void		deserialize(pugi::xml_node&	node);
	virtual	void		timelineDraw();
	virtual	int			getInterfaces();

protected:
						DummyComponent(std::string name);

//***********************************************************/
	float x,y,z;
	Property<float>	xx;
//***********************************************************/
};
}


#endif