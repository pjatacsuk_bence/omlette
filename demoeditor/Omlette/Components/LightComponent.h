#ifndef INC_LIGHTCOMPONENT_H
#define INC_LIGHTCOMPONENT_H


#include <Omlette\Components\MeshRendererComponent.h>
#include <Omlette\Components\Component.h>
#include <Omlette\Interfaces\IDrawable.h>

#include <Omlette\Interfaces\IComponentManager.h>

namespace Oml
{
class LightComponent : public Component, public IDrawable, public IUpdateable, public ISerializable
{
friend class LightManager;
public:
	typedef boost::shared_ptr<LightComponent> P;
	static P create(std::string name, TransformComponent::P tfc = NULL)
	{
		return P(new LightComponent(name, tfc));
	}

				~LightComponent();
	int			getInterfaces();
	void		draw(RenderParameters renderParameters);
	void		update(float t);
	void		serialize(pugi::xml_node& node);
	void		deserialize(pugi::xml_node& node);
	void		buildDependencies();
	void		updateDependencies();
	void		setEntityName(std::string ename);
private:
							LightComponent(std::string name, TransformComponent::P tfc=NULL);
	TransformComponent::P	transformComponent;
	std::string				transformEntityName;
	std::string				transformComponentName;
	Property<Egg::Math::float1>::P lightIntensity;

	Egg::Math::float1					currentLightIntensity;
	Egg::Math::float3					currentLightPos;

};

}
#endif // !INC_MULTIMESHRENDERERCOMPONENT