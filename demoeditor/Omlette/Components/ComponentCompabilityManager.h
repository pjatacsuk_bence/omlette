#ifndef INC_COMPONENTCOMPABILITYMANAGER_H
#define INC_COMPONENTCOMPABILITYMANAGER_H

#include <map>
#include <string>
#include <vector>

namespace Oml{
class ComponentCompabilityManager
{
public:
	static ComponentCompabilityManager&		getInstance()
	{
		static ComponentCompabilityManager instance;
		return instance;
	}
	
								~ComponentCompabilityManager();
	std::vector<std::string>	getCompatibleComponents(std::string componentName);
private:
								ComponentCompabilityManager();

	std::map<std::string, std::map<std::string, bool>> compabilityMap;


};


}
#endif