#include "DXUT.h"
#include <Omlette\Components\SolidColorMaterial.h>
#include <Omlette\Framework\Utility\ResourceManager.h>

using  namespace Oml;

SolidColorMaterial::SolidColorMaterial(std::string name):
MaterialComponent(name,"SolidColorMaterial")
{
	color = Property<Egg::Math::float4>::P(new Property<Egg::Math::float4>("color"));
	propertyMap[color->getName()] = color;
	technique = "basic";
	pass = "SolidColor";

	ID3DX11EffectPass* shadowPass = ResourceManager::getInstance().getEffect()
		->GetTechniqueByName("shadow")->GetPassByName("shadow");
	shadowMaterial =  Oml::Mesh::Material::create(shadowPass,0);

	ID3DX11EffectPass* scmPass = ResourceManager::getInstance().getEffect()
		->GetTechniqueByName(technique.c_str())->GetPassByName(pass.c_str());
	material = Oml::Mesh::Material::create(scmPass,0);

	ID3DX11EffectPass* ambientPass = ResourceManager::getInstance().getEffect()
		->GetTechniqueByName("ambientScene")->GetPassByName("ambientSolidColor");
	ambientMaterial =  Oml::Mesh::Material::create(ambientPass,0);
}

void	SolidColorMaterial::serializeProperties(pugi::xml_node& node)
{
	color->serialize(node);
}

void	SolidColorMaterial::deserializeProperties(pugi::xml_node& node)
{
	color->deserialize(node.first_child());

}