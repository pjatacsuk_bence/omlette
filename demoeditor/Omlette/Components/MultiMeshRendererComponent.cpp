#include "DXUT.h"

#include <Omlette\Components\MultiMeshRendererComponent.h>
#include <Omlette\Entities\EntityManager.h>
#include <Omlette\Components\ComponentFactory.h>
#include <Omlette\Components\ComponentMap.h>
#include <Omlette\Framework\Utility\Caster.h>
#include <Omlette\Framework\Utility\ResourceManager.h>
#include <Omlette\Components\BlendingTexturedMaterial.h>
using namespace Oml;



MultiMeshRendererComponent::MultiMeshRendererComponent(std::string name, std::string path, 
														TransformComponent::P tfc, MaterialComponent::P mat):
Component(name, "MultiMeshRendererComponent")
{
	this->path = path;
	transformComponent = tfc;
	if (path.compare("") != 0)
	{
		loadMultiMesh(path, mat);
	}
	
}


MultiMeshRendererComponent::~MultiMeshRendererComponent()
{
	delete componentMap;
}

void MultiMeshRendererComponent::draw(RenderParameters renderParameters)
{
	componentMap->draw(renderParameters);
}
void MultiMeshRendererComponent::update(float t)
{
	componentMap->update(t);
}
void MultiMeshRendererComponent::serialize(pugi::xml_node& node)
{
	pugi::xml_node mrnode = node.append_child("MultiMeshRendererComponent");

	pugi::xml_attribute nm = mrnode.append_attribute("name");

	pugi::xml_node pathnode = mrnode.append_child("path");
	pathnode.append_child(pugi::node_pcdata).set_value(path.c_str());
	
	pugi::xml_node transformnode = mrnode.append_child("transform");

	pugi::xml_attribute ent = transformnode.append_attribute("entity");
	ent.set_value(transformComponent->getEntityName().c_str());

	pugi::xml_attribute comp = transformnode.append_attribute("component");
	comp.set_value(transformComponent->getName().c_str());
	nm.set_value(name.c_str());
	
	componentMap->serialize(mrnode);
}
void MultiMeshRendererComponent::deserialize(pugi::xml_node& node)
{
	pugi::xml_attribute nameAttr = node.first_attribute();
	name = nameAttr.as_string();

	pugi::xml_node pathNode = node.first_child();
	path = pathNode.first_child().value();

	pugi::xml_node transNode = pathNode.next_sibling();

	pugi::xml_attribute transEntityName = transNode.first_attribute();
	this->transformEntityName = transEntityName.as_string();

	pugi::xml_attribute transCompName = transEntityName.next_attribute();
	this->transformComponentName  = transCompName.as_string();

	componentMap->deserialize(node.first_child().next_sibling().next_sibling());
	
}
int MultiMeshRendererComponent::getInterfaces()
{
	return SERIALIZABLE | UPDATEABLE | DRAWABLE;
}
void MultiMeshRendererComponent::loadMultiMesh(std::string path, MaterialComponent::P mat)
{
	std::vector<Mesh::Geometry::P> geometries;
	geometries = ResourceManager::getInstance().getGeometry(path);
	if (geometries.size() == 0)
	{
		ResourceManager::getInstance().loadGeometry(path);
		geometries = ResourceManager::getInstance().getGeometry(path);
	}
	int i = 0;
	for (auto	it = geometries.begin();
				it != geometries.end();
				it++)
	{
		addMesh(*it,i, mat);
		i++;
	}
}
void	MultiMeshRendererComponent::addMesh(Mesh::Geometry::P geometry,int meshNumber, MaterialComponent::P mat)
{
	std::stringstream ss;
	ss << "mesh" << componentMap->getSize();
	if (mat == NULL)
	{
		mat = TexturedMaterial::create("btm");
	}

	std::string name = ss.str();
	TransformComponent::P tc = 
		Caster::getInstance().getComponentCasted<TransformComponent::P>(
			ComponentFactory::getInstance().createTransformComponent(name + "tc", transformComponent));
	MeshRendererComponent::P mrc =
		MeshRendererComponent::P(new MeshRendererComponent(name, path, meshNumber, geometry, mat, transformComponent,true));
	componentMap->addComponent(mrc);
}
void	MultiMeshRendererComponent::addMesh(Mesh::Geometry::P geometry, int meshNumber, MaterialComponent::P mat, IPickable::SpecialID id, std::string path)
{
	std::stringstream ss;
	ss << "mesh" << componentMap->getSize();
	if (mat == NULL)
	{
		mat = TexturedMaterial::create("btm");
	}

	std::string name = ss.str();
	MeshRendererComponent::P mrc =
		MeshRendererComponent::P(new MeshRendererComponent(name, path, meshNumber, geometry, mat, transformComponent,id));
	componentMap->addComponent(mrc);
}

void MultiMeshRendererComponent::buildDependencies()
{
	Component::P transf = EntityManager::getInstance().getComponentFromEntity(transformEntityName,transformComponentName);
	transformComponent = boost::dynamic_pointer_cast<TransformComponent,Component>(transf);
	componentMap->buildDependencies();
}

void MultiMeshRendererComponent::updateDependencies()
{
	componentMap->updateDependencies();
}

void MultiMeshRendererComponent::setTransformComponent(TransformComponent::P tc)
{
}