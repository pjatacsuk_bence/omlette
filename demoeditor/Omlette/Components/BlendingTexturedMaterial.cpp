#include "DXUT.h"
#include <Omlette\Framework\Utility\ResourceManager.h>
#include <Omlette\Framework\Utility\Caster.h>
#include <Omlette\Components\BlendingTexturedMaterial.h>

using  namespace Oml;


BlendingTexturedMaterial::BlendingTexturedMaterial(std::string name):
TexturedMaterial(name,"BlendingTexturedMaterial")
{
	technique = "basic";
	pass = "BlendingTextured";

	ID3DX11EffectPass* texturedPass = ResourceManager::getInstance().getEffect()
		->GetTechniqueByName(technique.c_str())->GetPassByName(pass.c_str());
	material = Oml::Mesh::Material::create(texturedPass,0);

	ID3DX11EffectPass* ambientPass = ResourceManager::getInstance().getEffect()
		->GetTechniqueByName("ambientScene")->GetPassByName("ambientBlendingTextured");
	ambientMaterial =  Oml::Mesh::Material::create(ambientPass,0);



	blendTextureValue = Property<Egg::Math::float1>::P(
					new Property<Egg::Math::float1>("blendTextureValue"));
	propertyMap[blendTextureValue->getName()] = blendTextureValue;

}


void BlendingTexturedMaterial::serializeProperties(pugi::xml_node& node)
{
	textureName->serialize(node);
	blendTextureValue->serialize(node);
}

void	BlendingTexturedMaterial::deserializeProperties(pugi::xml_node& node)
{
	pugi::xml_node textureNode = node.first_child();
	textureName->deserialize(textureNode);
	loadTextures();

	pugi::xml_node blendingNode = textureNode.next_sibling();
	blendTextureValue->deserialize(blendingNode);

}

void BlendingTexturedMaterial::applyVariables(ID3DX11Effect* effect)
{
	auto i = propertyMap.begin();
	auto e = propertyMap.end();

	while(i != e)
	{
		
		if(i->second->getType() == FLOAT4)
		{
			Property<Egg::Math::float4>::P prop = 
			Caster::getInstance().getPropertyCasted<Property<Egg::Math::float4>::P>(i->second);
			effect->GetVariableByName(i->second->getName().c_str())
				->AsVector()->SetFloatVector((float*)(& (prop->getValue(time))));
		}
		if(i->second->getType() == STRING)
		{
			Property<std::string>::P prop = 
			Caster::getInstance().getPropertyCasted<Property<std::string>::P>(i->second);
			if(prop->getName().compare("kdTexture") == 0)
			{
				Segment<std::string>::P  segment = prop->getSegment(time);
				if (segment == NULL)
				{
					i++;
					continue;
				}

				std::string value = segment->getFirstKeyFrame()->key;
				ID3D11ShaderResourceView* tmp = ResourceManager::getInstance().getTextureSrv(value);
				if(tmp != NULL)
				effect->GetVariableByName("kdTexture")
					->AsShaderResource()->SetResource(tmp);

				value = segment->getSecondKeyFrame()->key;
				tmp = ResourceManager::getInstance().getTextureSrv(value);
				if(tmp != NULL)
				effect->GetVariableByName("kdTexture2")
					->AsShaderResource()->SetResource(tmp);
			}
		}
		if(i->second->getType() == FLOAT)
		{
			
			Property<Egg::Math::float1>::P prop = 
			Caster::getInstance().getPropertyCasted<Property<Egg::Math::float1>::P>(i->second);
			effect->GetVariableByName(prop->getName().c_str())
				->AsScalar()->SetFloat((prop->getValue(time)));
		}
		i++;
	}
}