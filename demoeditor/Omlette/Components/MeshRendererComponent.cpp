#include "DXUT.h"
#include "MeshRendererComponent.h"
#include <Omlette\Cam\CameraManager.h>
#include <Omlette\Interfaces\IPickable.h>
#include <Omlette\Entities\EntityManager.h>
#include <Omlette\Framework\Utility\ResourceManager.h>
#include <Omlette\Mesh\MeshImporter.h>
#include <Omlette\Components\ComponentFactory.h>
#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <Omlette\Framework\Utility\Caster.h>


Oml::MeshRendererComponent::~MeshRendererComponent()
{
	flipMesh.reset();
}

void Oml::MeshRendererComponent::update(float t)
{
	transformMatrix = transformComponent->getTransform(t);
}

void Oml::MeshRendererComponent::draw(RenderParameters renderParameters)
{

	using namespace Egg::Math;

	renderParameters.effect->GetVariableByName("modelMatrix")->AsMatrix()->SetMatrix((float*)&transformMatrix);
	renderParameters.effect->GetVariableByName("modelMatrixInverse")->AsMatrix()->SetMatrix((float*)&transformMatrix.invert());

	float4x4 modelViewProjMatrix = 
		transformMatrix *
		Cam::CameraManager::getInstance().getCurrentCam()->getViewMatrix() * 
		Cam::CameraManager::getInstance().getCurrentCam()->getProjMatrix();
		renderParameters.effect->GetVariableByName("modelViewProjMatrix")->AsMatrix()->SetMatrix((float*)&modelViewProjMatrix);
		materialComponent->applyVariables(renderParameters.effect);
		flipMesh->draw(renderParameters);
}

void Oml::MeshRendererComponent::serialize(pugi::xml_node& node)
{
	pugi::xml_node mrnode = node.append_child("MeshRendererComponent");
	
	pugi::xml_attribute nm = mrnode.append_attribute("name");
	nm.set_value(name.c_str());


	pugi::xml_node pathnode = mrnode.append_child("path");
	pathnode.append_child(pugi::node_pcdata).set_value(path.c_str());
	
	pugi::xml_node meshNumberNode = mrnode.append_child("meshNumber");
	std::stringstream ss;
	ss  << meshNumber;
	std::string meshNumberStr = ss.str();
	meshNumberNode.append_child(pugi::node_pcdata).set_value(meshNumberStr.c_str());

	pugi::xml_node transformnode = mrnode.append_child("transform");

	pugi::xml_attribute ent = transformnode.append_attribute("entity");
	ent.set_value(transformComponent->getEntityName().c_str());

	pugi::xml_attribute comp = transformnode.append_attribute("component");
	comp.set_value(transformComponent->getName().c_str());

	pugi::xml_node materialNode = mrnode.append_child("material");

	pugi::xml_attribute mnodeEntity = materialNode.append_attribute("entity");
	mnodeEntity.set_value(materialComponent->getEntityName().c_str());

	pugi::xml_attribute mnodeComp = materialNode.append_attribute("component");
	mnodeComp.set_value(materialComponent->getName().c_str());
}

void Oml::MeshRendererComponent::deserialize(pugi::xml_node& node)
{

}

void Oml::MeshRendererComponent::buildDependencies()
{
	Component::P transf = EntityManager::getInstance().getComponentFromEntity(meshDesc.transComponentEntityName,meshDesc.transComponentName);
	TransformComponent::P parent = Caster::getInstance().getComponentCasted<TransformComponent::P>(transf);
	transformComponent->setParent(parent);


	Component::P mat = EntityManager::getInstance().getComponentFromEntity(meshDesc.materialComponentEntityName,meshDesc.materialComponentName);
	materialComponent = boost::dynamic_pointer_cast<MaterialComponent,Component>(mat);
	
	initResources();
	componentMap->buildDependencies();
}
void Oml::MeshRendererComponent::updateDependencies()
{
	Component::P mat = EntityManager::getInstance().getComponentFromEntity(materialComponent->getEntityName(),materialComponent->getName());
	materialComponent = boost::dynamic_pointer_cast<MaterialComponent,Component>(mat);

	Component::P transf = EntityManager::getInstance().getComponentFromEntity(transformComponent->getParent()->getEntityName(), 
																				transformComponent->getParent()->getName());
	TransformComponent::P parent = Caster::getInstance().getComponentCasted<TransformComponent::P>(transf);
	transformComponent->setParent(parent);


	Oml::Mesh::Geometry::P geometry = ResourceManager::getInstance().getGeometry(path)[meshNumber];
	initResources(geometry);
	componentMap->updateDependencies();
}
void Oml::MeshRendererComponent::initResources(Mesh::Geometry::P geometry)
{
	if (flipMesh == NULL)
	{
		flipMesh = Mesh::FlipMesh::create();
	}
	ID3D11Device* device = ResourceManager::getInstance().getDevice();
	if (geometry == NULL)
	{
		if (ResourceManager::getInstance().getGeometry(path).size() == 0)
		{
			ResourceManager::getInstance().loadGeometry(path);
		}
		geometry = ResourceManager::getInstance().getGeometry(path)[meshNumber];
	}
	
	Oml::Mesh::IndexedMesh::P indexedMesh = 
		boost::dynamic_pointer_cast<Oml::Mesh::IndexedMesh,Oml::Mesh::Geometry> (geometry);


	Oml::Mesh::ShadedMesh::P shadedMesh =  ResourceManager::getInstance().getBinder()->bindMaterial(materialComponent->getMaterial(),indexedMesh);
	Oml::Mesh::ShadedMesh::P ambientMesh =  ResourceManager::getInstance().getBinder()->bindMaterial(materialComponent->getAmbientMaterial(),indexedMesh);
	Oml::Mesh::ShadedMesh::P shadowMesh =  ResourceManager::getInstance().getBinder()->bindMaterial(materialComponent->getShadowMaterial(),indexedMesh);
	Oml::Mesh::ShadedMesh::P pickerMesh = ResourceManager::getInstance().getBinder()->bindMaterial(materialComponent->getPickerMaterial(),indexedMesh);
	Oml::Mesh::ShadedMesh::P pickedMesh = ResourceManager::getInstance().getBinder()->bindMaterial(materialComponent->getPickedMaterial(),indexedMesh);

	flipMesh->addMesh(ResourceManager::getInstance().getMien("shaded"), shadedMesh);
	flipMesh->addMesh(ResourceManager::getInstance().getMien("ambient"),ambientMesh);
	flipMesh->addMesh(ResourceManager::getInstance().getMien("shadow"), shadowMesh);
	flipMesh->addMesh(ResourceManager::getInstance().getMien("pickable"), pickerMesh);
	flipMesh->addMesh(ResourceManager::getInstance().getMien("picked"), pickedMesh);
		
}

int Oml::MeshRendererComponent::getInterfaces()
{
	return SERIALIZABLE | UPDATEABLE | DRAWABLE;
}

Oml::MeshRendererComponent::MeshRendererComponent(std::string name, std::string path, int meshNumber, Mesh::Geometry::P geometry, MaterialComponent::P material, TransformComponent::P transform, bool needPivot):
Component(name,"MeshRendererComponent"),
IPickable(this,this)
{
	this->transformComponent =
		Caster::getInstance().getComponentCasted<TransformComponent::P>(
			ComponentFactory::getInstance().createTransformComponent(transformComponent->getName()+"child",transformComponent, needPivot));
	componentMap->addComponent(transformComponent);
	this->path = path;
	this->materialComponent = material;
	this->technique = technique;
	this->pass = pass;
	this->meshNumber = meshNumber;
	
	initResources(geometry);
}
Oml::MeshRendererComponent::MeshRendererComponent(std::string name, std::string path, int meshNumber, Mesh::Geometry::P geometry, MaterialComponent::P material, TransformComponent::P transform, IPickable::SpecialID id, bool needPivot):
Component(name,"MeshRendererComponent"),
IPickable(this,this,id)
{
	this->path = path;
	this->transformComponent =
		Caster::getInstance().getComponentCasted<TransformComponent::P>(
			ComponentFactory::getInstance().createTransformComponent(transform->getName()+"child",transform, needPivot));
	componentMap->addComponent(transformComponent);
	this->materialComponent = material;
	this->technique = technique;
	this->pass = pass;
	this->meshNumber = meshNumber;
	initResources(geometry);
}
Oml::MeshRendererComponent::MeshRendererComponent(std::string name, std::string path, MaterialComponent::P material, TransformComponent::P transform, bool needPivot):
Component(name,"MeshRendererComponent"),
IPickable(this,this)
{
	this->path = path;
	this->transformComponent =
		Caster::getInstance().getComponentCasted<TransformComponent::P>(
			ComponentFactory::getInstance().createTransformComponent(transform->getName()+"child",transform, needPivot));
	componentMap->addComponent(transformComponent);
	this->materialComponent = material;
	this->technique = technique;
	this->pass = pass;
	this->meshNumber = 0;
	initResources();
}

Oml::MeshRendererComponent::MeshRendererComponent(MeshRendererComponentDescription desc):
Component(desc.name,"MeshRendererComponent"),
IPickable(this,this)
{
	this->meshDesc = desc;
	this->path = meshDesc.path;
	this->meshNumber = meshDesc.meshNumber;
	this->transformComponent =
		Caster::getInstance().getComponentCasted<TransformComponent::P>(
			ComponentFactory::getInstance().createTransformComponent("child",NULL,true));
	componentMap->addComponent(transformComponent);
}

void Oml::MeshRendererComponent::replaceMesh(Mesh::Geometry::P geometry)
{
	initResources(geometry);
}

void Oml::MeshRendererComponent::addMien(Oml::Mesh::Mien mien, Oml::Mesh::ShadedMesh::P shadedMesh)
{
	flipMesh->addMesh(mien, shadedMesh);
}