#ifndef	INC_COMPONENTFACTORY_H
#define	INC_COMPONENTFACTORY_H	 
#include <Omlette\Components\Component.h>
#include <Omlette\Components\DummyComponent.h>
#include <Omlette\Components\TransformComponent.h>
#include <Omlette\Components\MeshRendererComponent.h>
#include <Omlette\Components\MultiMeshRendererComponent.h>
#include <Omlette\Mesh\Binder.h>
#include <Omlette\Components\SolidColorMaterial.h>
#include <Omlette\Components\LightComponent.h>
namespace Oml	{
class ComponentFactory
{
public:
	static	ComponentFactory&	getInstance()
	{
		static ComponentFactory instance;
		return instance;

	}
	inline void	setDeviceEffectAndBinder(ID3D11Device* device,ID3DX11Effect* effect,Mesh::Binder::P binder)
	{
		this->device = device;
		this->effect = effect;
		this->binder = binder;
	}
		

	Component::P				createDummyComponent(std::string name);	
	Component::P				createDummyComponent(pugi::xml_node& node);	

	Component::P				createTransformComponent(std::string name, TransformComponent::P parent = NULL, bool needPivot=false);	
	Component::P				createTransformComponent(pugi::xml_node& node);	

	Component::P				createMaterialComponent(pugi::xml_node& node);	

	Component::P				createMeshRendererComponent(std::string name,std::string path, MaterialComponent::P material, Component::P transform);
	Component::P				createMeshRendererComponent(pugi::xml_node& node);	
	Component::P				createMeshRendererComponent(MeshRendererComponent::MeshRendererComponentDescription desc);	

	Component::P				createMultiMeshRendererComponent(pugi::xml_node& node);	

	Component::P				createLightComponent(pugi::xml_node& node);

	Component::P				deserialize(pugi::xml_node& node);	
	Component::P				createComponent(std::wstring componentTypeName);
	


	template<typename T>	T	getInterfaceFromComponent(Component::P p);


private:
	ComponentFactory(){}
	ComponentFactory(ComponentFactory const&);
	void operator=(ComponentFactory const&);
	ID3D11Device*	device;
	ID3DX11Effect*	effect;
	Mesh::Binder::P	binder;
};

template <typename T>
T ComponentFactory::getInterfaceFromComponent(Component::P p)
{
	if(p->getComponentTypeName().compare("MeshRendererComponent")==0)
	{
		MeshRendererComponent::P mc =	boost::dynamic_pointer_cast<MeshRendererComponent,Component>(p);
		return mc;

	}
	if(p->getComponentTypeName().compare("DummyComponent")==0)
	{
		DummyComponent::P dc =	boost::dynamic_pointer_cast<DummyComponent,Component>(p);
		return dc;

	}
	else if(p->getComponentTypeName().compare("TransformComponent")==0)
	{

		TransformComponent::P tc =	boost::dynamic_pointer_cast<TransformComponent,Component>(p);
		return tc;

	}
	else if(p->getComponentTypeName().compare("MaterialComponent")==0)
	{
		MaterialComponent::P mc = boost::dynamic_pointer_cast<MaterialComponent,Component>(p);	
		return mc;
	}
	if(p->getComponentTypeName().compare("MultiMeshRendererComponent")==0)
	{
		MultiMeshRendererComponent::P mmc =	boost::dynamic_pointer_cast<MultiMeshRendererComponent,Component>(p);
		return mmc;
	}
	if (p->getComponentTypeName().compare("LightComponent") == 0)
	{
		LightComponent::P lc = boost::dynamic_pointer_cast<LightComponent, Component>(p);
		return lc;
	}
	return NULL;
}
template<>
inline IDrawable::P	ComponentFactory::getInterfaceFromComponent<IDrawable::P>(Component::P p)
{
	if(p->getComponentTypeName().compare("DummyComponent")==0)
	{
		DummyComponent::P dc =	boost::dynamic_pointer_cast<DummyComponent,Component>(p);
		return dc;
	}
	if(p->getComponentTypeName().compare("MeshRendererComponent")==0)
	{
		MeshRendererComponent::P mc =	boost::dynamic_pointer_cast<MeshRendererComponent,Component>(p);
		return mc;
	}
	if(p->getComponentTypeName().compare("MultiMeshRendererComponent")==0)
	{
		MultiMeshRendererComponent::P mmc =	boost::dynamic_pointer_cast<MultiMeshRendererComponent,Component>(p);
		return mmc;
	}
	if (p->getComponentTypeName().compare("LightComponent") == 0)
	{
		LightComponent::P lc = boost::dynamic_pointer_cast<LightComponent, Component>(p);
		return lc;
	}
	return NULL;
}

}
#endif