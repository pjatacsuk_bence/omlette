#ifndef	INC_BLENDINGTEXTUREDMATERIAL_H
#define INC_BLENDINGTEXTUREDMATERIAL_H
#include <Omlette\Components\TexturedMaterial.h>
#include <boost\shared_ptr.hpp>
namespace Oml
{
class BlendingTexturedMaterial : public TexturedMaterial
{
friend class ComponentFactory;
public:
	typedef boost::shared_ptr<BlendingTexturedMaterial> P;
	static P	create(std::string name){return P(new BlendingTexturedMaterial(name));}	
	void		serializeProperties(pugi::xml_node& node);
	void		deserializeProperties(pugi::xml_node& node);
	void		applyVariables(ID3DX11Effect* effect);
	
protected:
									BlendingTexturedMaterial(std::string name="");
	
	Property<Egg::Math::float1>::P	blendTextureValue;	

};	

}

#endif