#include "DXUT.h"
#include <Omlette\Components\ComponentMap.h>
#include "pugixml.hpp"
#include "ComponentFactory.h"
#include <Omlette\Framework\Utility\Logger.h>
#include "DummyComponent.h"
#include <Omlette\Framework\Gui\GuiManager.h>
#include <Omlette\Framework\Gui\GuiLabel.h>

using namespace Oml;

ComponentMap::ComponentMap(std::string ename):
IComponentManager(ename)
{

}

ComponentMap::~ComponentMap()
{
	drawableComponents.clear();
	updateableComponents.clear();
	serializableComponents.clear();
	components.clear();

}

Oml::Component::P	ComponentMap::getComponent(std::string name)
{
	auto it = components.find(name);
	if (it != components.end())
		return components[name];
	return NULL;
}

void			ComponentMap::addComponent(Oml::Component::P component)
{
	components[component->getName()] = component;
	component->setEntityName(entityName);
	if(component->getInterfaces() & Component::TYPE::DRAWABLE)
	{
		IDrawablePack pack;
		pack.iDrawable = ComponentFactory::getInstance().getInterfaceFromComponent<IDrawable::P>(component);
		pack.componentName = component->getName();
		drawableComponents.push_back(pack);
	}
	if(component->getInterfaces() & Component::TYPE::UPDATEABLE)
	{
		IUpdateablePack pack;
		pack.iUpdateable = ComponentFactory::getInstance().getInterfaceFromComponent<IUpdateable::P>(component);
		pack.componentName = component->getName();
		updateableComponents.push_back(pack);
	}
	if(component->getInterfaces() & Component::TYPE::SERIALIZABLE)
	{
		ISerializablePack pack;
		pack.iSerializable = ComponentFactory::getInstance().getInterfaceFromComponent<ISerializable::P>(component);
		pack.componentName = component->getName();
		serializableComponents.push_back(pack);
	}
}

void			ComponentMap::serialize(pugi::xml_node node)
{
	pugi::xml_node sub_node = node.append_child("Components");

	for(auto	it = serializableComponents.begin(); 
				it != serializableComponents.end();
				it++)
	{
		if (it->iSerializable != NULL)
		{
			it->iSerializable->serialize(sub_node);
		}
	}
}



void			ComponentMap::deserialize(pugi::xml_node node)
{
	for(pugi::xml_node iter = node.first_child();iter;iter = iter.next_sibling())
	{
		if(iter != NULL)
		{
			Logger::getInstance().consoleLog(iter.name());

			Oml::Component::P component = ComponentFactory::getInstance().deserialize(iter);
			component->setEntityName(entityName);
			components[component->getName()] = component;

			if(component->getInterfaces() & Component::TYPE::DRAWABLE)
			{
				IDrawablePack pack;
				pack.iDrawable = ComponentFactory::getInstance().getInterfaceFromComponent<IDrawable::P>(component);
				pack.componentName = component->getName();
				drawableComponents.push_back(pack);
			}
			if(component->getInterfaces() & Component::TYPE::UPDATEABLE)
			{
				IUpdateablePack pack;
				pack.iUpdateable = ComponentFactory::getInstance().getInterfaceFromComponent<IUpdateable::P>(component);
				pack.componentName = component->getName();
				updateableComponents.push_back(pack);
			}
			if(component->getInterfaces() & Component::TYPE::SERIALIZABLE)
			{
				ISerializablePack pack;
				pack.iSerializable = ComponentFactory::getInstance().getInterfaceFromComponent<ISerializable::P>(component);
				pack.componentName = component->getName();
				serializableComponents.push_back(pack);
			}
		}
	}
}


void			ComponentMap::draw(IDrawable::RenderParameters renderParameters)
{

	for(auto	it = drawableComponents.begin(); 
				it != drawableComponents.end();
				it++)
	{
		if (it->iDrawable != NULL)
		{
			it->iDrawable->draw(renderParameters);
		}
	}
	
}

int			ComponentMap::getTimelineHeight()
{
	
	int height = 0;
	for(iterator	it  = components.begin();
					it != components.end();
					it ++)
	{
		height += (*it).second->getTimelineHeight();	
	}
	return height;
}
void			ComponentMap::guiDraw()
{
}
void			ComponentMap::timelineDraw(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget, ID2D1SolidColorBrush* brush, Egg::Math::float1 timelineMinTime, Egg::Math::float1 timelineMaxTime)
{

	int i=0;
	int height = rect.top;
	for(iterator	it  = components.begin();
					it != components.end();
					it ++)
	{
		D2D1_RECT_F compRect = rect;
		compRect.top = height;
		height = height +  it->second->getTimelineHeight();
		compRect.bottom = height;
		it->second->timelineDraw(compRect,renderTarget,brush, timelineMinTime, timelineMaxTime);

	}
}

void ComponentMap::updateDependencies()
{
	for(iterator	it  = components.begin();
					it != components.end();
					it ++)
	{
		it->second->updateDependencies();
	}

}
void ComponentMap::buildDependencies()
{
	for(iterator	it  = components.begin();
					it != components.end();
					it ++)
	{
		it->second->buildDependencies();
	}

}

void ComponentMap::deleteKeyFramesByPoint(Egg::Math::int2 point, D2D1_RECT_F rect)
{
	int i=0;
	int height = rect.top;
	for(iterator	it  = components.begin();
					it != components.end();
					it ++)
	{
		D2D1_RECT_F compRect = rect;
		compRect.top = height;
		height = height +  it->second->getTimelineHeight();
		compRect.bottom = height;
		if(point.y > compRect.top && point.y < compRect.bottom)
		{
			(*it).second->deleteKeyFramesByPoint(point,compRect);
		}
	}
}
void ComponentMap::addNewKeyFrame(Egg::Math::int2 point, D2D1_RECT_F rect)
{
	int i=0;
	int height = rect.top;
	for(iterator	it  = components.begin();
					it != components.end();
					it ++)
	{
		D2D1_RECT_F compRect = rect;
		compRect.top = height;
		height = height +  it->second->getTimelineHeight();
		compRect.bottom = height;
		if(point.y > compRect.top && point.y < compRect.bottom)
		{
			(*it).second->addNewKeyFrame(point,compRect);
		}
	}
}

void			ComponentMap::update(float t)
{
	for(auto	it = updateableComponents.begin(); 
				it != updateableComponents.end();
				it++)
	{
		if (it->iUpdateable != NULL)
		{
			it->iUpdateable->update(t);
		}
	}
}


std::vector<IDrawable::P> ComponentMap::getDrawableComponents()
{
	std::vector<IDrawable::P> ret;
	for (auto	it = drawableComponents.begin();
		it != drawableComponents.end();
		it++)
	{
		if (it->iDrawable != NULL)
		{
			ret.push_back(it->iDrawable);
		}
	}
	return	ret;

}

std::vector<IUpdateable::P> ComponentMap::getUpdateableComponents()
{
	std::vector<IUpdateable::P> ret;
	for(auto	it = updateableComponents.begin(); 
				it != updateableComponents.end();
				it++)
	{
		if (it->iUpdateable != NULL)
		{
			ret.push_back(it->iUpdateable);
		}
	}
	return ret;
}

std::vector<ISerializable::P> ComponentMap::getSerializableComponents()
{
	std::vector<ISerializable::P> ret;
	for(auto	it = serializableComponents.begin(); 
				it != serializableComponents.end();
				it++)
	{
		if (it->iSerializable != NULL)
		{
			ret.push_back(it->iSerializable);
		}
	}
	return ret;
}

int	ComponentMap::getSize()
{
	return components.size();
}

std::vector<IPropertyAncestor::P>	ComponentMap::getPropertiesFromPoint(Egg::Math::int2 point, D2D1_RECT_F rect)
{
	int i=0;
	int height = rect.top;
	for(iterator	it  = components.begin();
					it != components.end();
					it ++)
	{
		D2D1_RECT_F compRect = rect;
		compRect.top = height;
		height = height +  it->second->getTimelineHeight();
		compRect.bottom = height;
		if(point.y > compRect.top && point.y < compRect.bottom)
		{
			return it->second->getPropertiesFromPoint(point,compRect);
		}
	}
	return std::vector<IPropertyAncestor::P>();
}

std::vector<std::string> ComponentMap::getNames()
{
	std::vector<std::string> ret;
	for(auto i = components.begin();
		i != components.end();
		i++)
	{
		ret.push_back(i->second->getName());
	}
	return ret;
}
std::vector<Oml::Component::P> ComponentMap::getComponents()
{
	std::vector<Oml::Component::P> ret;
	for(auto i = components.begin();
		i != components.end();
		i++)
	{
		ret.push_back(i->second);
	}
	return ret;
}

void ComponentMap::swapComponent(std::string componentName, Oml::Component::P component)
{
	deleteInterfacesFromComponent(componentName);
	components[componentName] = component;
	updateDependencies();
	addComponent(component);
	if (component->getName().compare(componentName) != 0)
		components.erase(components.find(componentName));
	
}
std::vector<Oml::Component::P> ComponentMap::getComponentsByType(std::string componentTypeName)
{
	std::vector<Oml::Component::P> ret;
	for (auto	it = components.begin(); 
				it != components.end();
				it++)
	{
		if (it->second->getComponentTypeName().compare(componentTypeName) == 0)
		{
			ret.push_back( (it->second));
		}
	}
	return ret;
}

void ComponentMap::deleteInterfacesFromComponent(std::string componentName)
{
	auto dit = drawableComponents.begin();
	while (dit != drawableComponents.end())
	{
		if (dit->componentName.compare(componentName) == 0)
		{
			dit = drawableComponents.erase(dit);
		}
		else
		{
			dit++;
		}
	}

	auto uit = updateableComponents.begin();
	while (uit != updateableComponents.end())
	{
		if (uit->componentName.compare(componentName) == 0)
		{
			uit = updateableComponents.erase(uit);
		}
		else
		{
			uit++;
		}
	}
	auto sit = serializableComponents.begin();
	while (sit != serializableComponents.end())
	{
		if (sit->componentName.compare(componentName) == 0)
		{
			sit = serializableComponents.erase(sit);
		}
		else
		{
			sit++;
		}
	}
	

}

void ComponentMap::setEntityName(std::string ename)
{
	this->entityName = ename;
	auto it = components.begin();
	while (it != components.end())
	{
		it->second->setEntityName(ename);
		it++;
	}
}
Oml::Component::P ComponentMap::getComponentFromPoint(Egg::Math::int2 point, D2D1_RECT_F rect)
{

	int i=0;
	int height = rect.top;
	for(iterator	it  = components.begin();
					it != components.end();
					it ++)
	{
		D2D1_RECT_F compRect = rect;
		compRect.top = height;
		height = height +  it->second->getTimelineHeight();
		compRect.bottom = height;
		if(point.y > compRect.top && point.y < compRect.bottom)
		{
			return it->second->getComponentFromPoint(point, compRect);
		}
	}
	return NULL;
}
