#include "DXUT.h"

#include <Omlette\Components\Component.h>
#include <Omlette\Interfaces\IComponentManager.h>
#include <Omlette\Components\ComponentMap.h>
#include <Omlette\Framework\Gui\GuiTimeline.h>
#include <Omlette\Framework\Gui\GuiManager.h>
#include <Omlette\Framework\Gui\GuiLabel.h>

using namespace Oml;

Oml::Component::Component(std::string name,std::string ctn):
name(name),
componentTypeName(ctn)
{
	this->componentMap = new Oml::ComponentMap("");
}

Oml::Component::~Component()
{
	propertyMap.clear();
}

void Oml::Component::buildDependencies()
{
	componentMap->buildDependencies();
}

void Oml::Component::updateDependencies()
{
	componentMap->updateDependencies();
}


void Oml::Component::timelineDraw(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget, ID2D1SolidColorBrush* brush, Egg::Math::float1 timelineMinTime, Egg::Math::float1 timelineMaxTime)
{
	


	GuiLabel::incTabCount();
	PropertyMap::iterator i = propertyMap.begin();
	PropertyMap::iterator e = propertyMap.end();
	int h = 1;
	while(i != e)
	{
		
		D2D1_RECT_F propRect = rect;
		propRect.top = rect.top + h * GuiTimeline::HEIGHT_FOR_PROPERTY;
		propRect.bottom = rect.top + (h+1) * GuiTimeline::HEIGHT_FOR_PROPERTY;
		
		ID2D1SolidColorBrush* scb = h % 2 == 0 ? GuiManager::getInstance().getSolidDarkGrayBrush() : GuiManager::getInstance().getSolidDarkerGrayBrush();
		
		renderTarget->FillRectangle(propRect, scb);
		D2D1_POINT_2F p0;
		p0.y = propRect.top;
		p0.x = propRect.left - rect.left;

		D2D1_POINT_2F p1;
		p1.y = propRect.top;
		p1.x = propRect.right;

		renderTarget->DrawLine(p0,p1,GuiManager::getInstance().getSolidGrayBrush());
		(*i).second->timelineDraw(propRect,renderTarget,brush, timelineMinTime, timelineMaxTime);

		D2D1_RECT_F  nameRect;
		nameRect.left = propRect.left - rect.left;
		nameRect.right = propRect.left;
		nameRect.top = propRect.top;
		nameRect.bottom  = propRect.bottom;
		std::string text = GuiLabel::getTabs() + (*i).second->getName() + "::" + (*i).second->getTypeName();
		GuiLabel::draw(renderTarget,
			nameRect,
			text.c_str(),
			GuiManager::getInstance().getBlackBrush(),
			scb, GuiLabel::ALIGN::LEFT);
		i++;
		h++;
	}
	D2D1_POINT_2F p0;
	p0.y = rect.top + (h+2) * GuiTimeline::HEIGHT_FOR_PROPERTY;
	p0.x = rect.left;

	D2D1_POINT_2F p1;
	p1.y = rect.top + (h+2) * GuiTimeline::HEIGHT_FOR_PROPERTY;
	p1.x = rect.right;
	renderTarget->DrawLine(p0,p1,GuiManager::getInstance().getBlackBrush());

	D2D1_RECT_F componentMapRect = rect;
	componentMapRect.top = rect.top + h * GuiTimeline::HEIGHT_FOR_PROPERTY;

	componentMap->timelineDraw(componentMapRect, renderTarget, brush, timelineMinTime, timelineMaxTime);
	GuiLabel::decTabCount();


	D2D1_RECT_F componentNameRect = rect;
	componentNameRect.bottom = rect.top + GuiTimeline::HEIGHT_FOR_COMPONENT;
	componentNameRect.left = 0;


	std::string text = GuiLabel::getTabs() + name;
	GuiLabel::draw(renderTarget,
		componentNameRect,
		text.c_str(),
		GuiManager::getInstance().getSolidWhiteBrush(), GuiManager::getInstance().getSolidBluishBrush(),GuiLabel::ALIGN::LEFT);
}

void Oml::Component::setEntityName(std::string ename)
{
	entityName = ename;
	componentMap->setEntityName(ename);
}

void Oml::Component::deleteKeyFramesByPoint(Egg::Math::int2 point, D2D1_RECT_F rect)
{

	PropertyMap::iterator i = propertyMap.begin();
	PropertyMap::iterator e = propertyMap.end();

	int h = 1;
	while(i != e)
	{
		
		D2D1_RECT_F propRect = rect;
		propRect.top = rect.top + h * GuiTimeline::HEIGHT_FOR_PROPERTY;
		propRect.bottom = rect.top + (h+1) * GuiTimeline::HEIGHT_FOR_PROPERTY;
		if(point.y > propRect.top && point.y < propRect.bottom)
		{
			Egg::Math::float1 time = GuiManager::getInstance().getTimeFromTimelinePoint(point);
			(*i).second->deleteKeyFramesByTime(time);
		}
		
		
		i++;
		h++;
	}
	D2D1_RECT_F componentMapRect = rect;
	componentMapRect.top = rect.top + h * GuiTimeline::HEIGHT_FOR_PROPERTY;
	componentMap->deleteKeyFramesByPoint(point, componentMapRect);
}
void Oml::Component::addNewKeyFrame(Egg::Math::int2 point, D2D1_RECT_F rect)
{

	PropertyMap::iterator i = propertyMap.begin();
	PropertyMap::iterator e = propertyMap.end();

	int h = 1;
	while(i != e)
	{
		
		D2D1_RECT_F propRect = rect;
		propRect.top = rect.top + h * GuiTimeline::HEIGHT_FOR_PROPERTY;
		propRect.bottom = rect.top + (h+1) * GuiTimeline::HEIGHT_FOR_PROPERTY;
		if(point.y > propRect.top && point.y < propRect.bottom)
		{
			Egg::Math::float1 time = GuiManager::getInstance().getTimeFromTimelinePoint(point);
			(*i).second->addKeyFrame(time);
		}
		
		
		i++;
		h++;
	}
	D2D1_RECT_F componentMapRect = rect;
	componentMapRect.top = rect.top + h * GuiTimeline::HEIGHT_FOR_PROPERTY;
	componentMap->addNewKeyFrame(point, componentMapRect);
}

std::vector<IPropertyAncestor::P>	Oml::Component::getPropertiesFromPoint(Egg::Math::int2 point, D2D1_RECT_F rect)
{

	std::vector<IPropertyAncestor::P> list;
	

	PropertyMap::iterator i = propertyMap.begin();
	PropertyMap::iterator e = propertyMap.end();
	int h = 1;
	while(i != e)
	{
		
		D2D1_RECT_F propRect = rect;
		propRect.top = rect.top + h * GuiTimeline::HEIGHT_FOR_PROPERTY;
		propRect.bottom = rect.top + (h+1) * GuiTimeline::HEIGHT_FOR_PROPERTY;
		if(point.y > propRect.top && point.y < propRect.bottom)
		{
			list.push_back(i->second);
		}
		
		
		i++;
		h++;
	}
	D2D1_RECT_F componentMapRect = rect;
	componentMapRect.top = rect.top + h * GuiTimeline::HEIGHT_FOR_PROPERTY;
	std::vector<IPropertyAncestor::P> componentMapPropertyList = componentMap->getPropertiesFromPoint(point,componentMapRect);
	list.insert(list.end(), componentMapPropertyList.begin(), componentMapPropertyList.end());
	return list;
}

Oml::Component::P		Oml::Component::getComponentFromPoint(Egg::Math::int2 point, D2D1_RECT_F rect)
{
	D2D1_RECT_F ownCompRect = rect;
	ownCompRect.top = rect.top + GuiTimeline::HEIGHT_FOR_PROPERTY;
	ownCompRect.bottom = rect.top + (propertyMap.size()+1) * GuiTimeline::HEIGHT_FOR_PROPERTY;
	if (point.y > ownCompRect.top && point.y < ownCompRect.bottom)
	{
		return shared_from_this();
	}
	else
	{
		D2D1_RECT_F componentMapRect = rect;
		componentMapRect.top = rect.top + (propertyMap.size()+1) * GuiTimeline::HEIGHT_FOR_PROPERTY;
		componentMapRect.bottom = rect.top + getTimelineHeight();
		return componentMap->getComponentFromPoint(point, componentMapRect);
	}
}

int Oml::Component::getTimelineHeight()
{
	int ret = (propertyMap.size() + 1) * GuiTimeline::HEIGHT_FOR_PROPERTY;
	if (componentMap->getSize() != 0)
	{
		ret += componentMap->getTimelineHeight();
	}
	return ret;
}