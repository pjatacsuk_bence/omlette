#include "DXUT.h"

#include <Omlette\Components\LightComponent.h>
#include <Omlette\Entities\EntityManager.h>
#include <Omlette\Components\ComponentMap.h>
#include <Omlette\Framework\Utility\ResourceManager.h>
#include <Omlette\Framework\Utility\LightManager.h>
#include <Omlette\Components\SolidColorMaterial.h>
#include <Omlette\Components\MeshRendererComponent.h>
using namespace Oml;

LightComponent::LightComponent(std::string name, TransformComponent::P tfc):
Component(name, "LightComponent")
{
	transformComponent = tfc;
	SolidColorMaterial::P scm = SolidColorMaterial::create("scm");
	MeshRendererComponent::P mesh =
	MeshRendererComponent::create("lightMesh", "./Resources/Meshes/SphereMesh.obj", scm,transformComponent);
	componentMap->addComponent(mesh);
	lightIntensity =
		boost::shared_ptr<Property<Egg::Math::float1>>(new Property<Egg::Math::float1>("lightIntensity"));
	propertyMap[lightIntensity->getName()] = lightIntensity;
	LightManager::getInstance().addLight(this);
	
}

void LightComponent::setEntityName(std::string ename)
{
	this->entityName = ename;
	componentMap->setEntityName(ename);
	
}

LightComponent::~LightComponent()
{
	delete componentMap;
}

void LightComponent::draw(RenderParameters renderParameters)
{
	if (*(renderParameters.mien) == ResourceManager::getInstance().getMien("shaded") ||
		*(renderParameters.mien) == ResourceManager::getInstance().getMien("pickable") ||
		*(renderParameters.mien) == ResourceManager::getInstance().getMien("ambient"))
	{
		componentMap->draw(renderParameters);
	}
}
void LightComponent::update(float t)
{
	currentLightIntensity = lightIntensity->getValue(t);
	currentLightPos = transformComponent->getPositionValue(t);
	componentMap->update(t);
}
void LightComponent::serialize(pugi::xml_node& node)
{
	pugi::xml_node mrnode = node.append_child("LightComponent");

	pugi::xml_attribute nm = mrnode.append_attribute("name");

	pugi::xml_node transformnode = mrnode.append_child("transform");

	pugi::xml_attribute ent = transformnode.append_attribute("entity");
	ent.set_value(transformComponent->getEntityName().c_str());

	pugi::xml_attribute comp = transformnode.append_attribute("component");
	comp.set_value(transformComponent->getName().c_str());
	nm.set_value(name.c_str());
	
	componentMap->serialize(mrnode);
}
void LightComponent::deserialize(pugi::xml_node& node)
{
	pugi::xml_attribute nameAttr = node.first_attribute();
	name = nameAttr.as_string();


	pugi::xml_node transNode = node.first_child();

	pugi::xml_attribute transEntityName = transNode.first_attribute();
	this->transformEntityName = transEntityName.as_string();

	pugi::xml_attribute transCompName = transEntityName.next_attribute();
	this->transformComponentName  = transCompName.as_string();

	componentMap->deserialize(node.first_child().next_sibling().next_sibling());
	
}
int LightComponent::getInterfaces()
{
	return SERIALIZABLE | UPDATEABLE | DRAWABLE;
}

void LightComponent::buildDependencies()
{
	Component::P transf = EntityManager::getInstance().getComponentFromEntity(transformEntityName,transformComponentName);
	transformComponent = boost::dynamic_pointer_cast<TransformComponent,Component>(transf);
	componentMap->buildDependencies();
}

void LightComponent::updateDependencies()
{
	componentMap->updateDependencies();
}