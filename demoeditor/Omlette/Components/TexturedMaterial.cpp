#include "DXUT.h"
#include <Omlette\Components\TexturedMaterial.h>
#include <Omlette\Framework\Utility\ResourceManager.h>
#include <Omlette\Framework\Utility\Caster.h>

using  namespace Oml;


TexturedMaterial::TexturedMaterial(std::string name):
MaterialComponent(name,"TexturedMaterial")
{
	technique = "basic";
	pass = "Textured";

	textureName = Property<std::string>::P(new Property<std::string>("kdTexture"));

	ID3DX11EffectPass* texturedPass = ResourceManager::getInstance().getEffect()
		->GetTechniqueByName(technique.c_str())->GetPassByName(pass.c_str());
	material = Oml::Mesh::Material::create(texturedPass,0);

	ID3DX11EffectPass* shadowPass = ResourceManager::getInstance().getEffect()
		->GetTechniqueByName("shadow")->GetPassByName("shadow");
	shadowMaterial =  Oml::Mesh::Material::create(shadowPass,0);

	ID3DX11EffectPass* ambientPass = ResourceManager::getInstance().getEffect()
		->GetTechniqueByName("ambientScene")->GetPassByName("ambientScene");
	ambientMaterial =  Oml::Mesh::Material::create(ambientPass,0);

	propertyMap[textureName->getName()] = textureName;

}

TexturedMaterial::TexturedMaterial(std::string name, std::string materialName):
MaterialComponent(name,materialName)
{
	ID3DX11EffectPass* shadowPass = ResourceManager::getInstance().getEffect()
		->GetTechniqueByName("shadow")->GetPassByName("shadow");
	shadowMaterial =  Oml::Mesh::Material::create(shadowPass,0);

	textureName = Property<std::string>::P(new Property<std::string>("kdTexture"));
	propertyMap[textureName->getName()] = textureName;
}

void TexturedMaterial::loadTextures()
{
	std::map<std::string,bool> keys = textureName->getKeys();
	for(auto i = keys.begin();i != keys.end();i++)
	{
		if((*i).second == true)
		{
			ResourceManager::getInstance().loadTextureSrv((*i).first);
		}
	}
}

void TexturedMaterial::serializeProperties(pugi::xml_node& node)
{
	textureName->serialize(node);
}

void	TexturedMaterial::deserializeProperties(pugi::xml_node& node)
{
	textureName->deserialize(node.first_child());
	loadTextures();
}

void TexturedMaterial::applyVariables(ID3DX11Effect* effect)
{
	auto i = propertyMap.begin();
	auto e = propertyMap.end();

	while(i != e)
	{
		
		if(i->second->getType() == FLOAT4)
		{
			Property<Egg::Math::float4>::P prop = 
			Caster::getInstance().getPropertyCasted<Property<Egg::Math::float4>::P>(i->second);
			effect->GetVariableByName(i->second->getName().c_str())
				->AsVector()->SetFloatVector((float*)(& (prop->getValue(time))));
		}
		if(i->second->getType() == STRING)
		{
			Property<std::string>::P prop = 
			Caster::getInstance().getPropertyCasted<Property<std::string>::P>(i->second);
			std::string value = prop->getValue(time);
			if(prop->getName().compare("kdTexture") == 0)
			{
				ID3D11ShaderResourceView* tmp = ResourceManager::getInstance().getTextureSrv(value);
				if(tmp != NULL)
				effect->GetVariableByName(prop->getName().c_str())
					->AsShaderResource()->SetResource(tmp);
			}
		}
		i++;
	}
}