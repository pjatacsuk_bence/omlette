#include "DXUT.h"
#include <Omlette\Components\ComponentCompabilityManager.h>

using namespace Oml;

ComponentCompabilityManager::ComponentCompabilityManager()
{
	
	compabilityMap["MaterialComponent"]["SolidColorMaterial"] = true;
	compabilityMap["MaterialComponent"]["TexturedMaterial"] = true;
	compabilityMap["MaterialComponent"]["BlendingTexturedMaterial"] = true;

	compabilityMap["SolidColorMaterial"]["SolidColorMaterial"] = true;
	compabilityMap["SolidColorMaterial"]["TexturedMaterial"] = true;
	compabilityMap["SolidColorMaterial"]["BlendingTexturedMaterial"] = true;

	compabilityMap["TexturedMaterial"]["SolidColorMaterial"] = true;
	compabilityMap["TexturedMaterial"]["TexturedMaterial"] = true;
	compabilityMap["TexturedMaterial"]["BlendingTexturedMaterial"] = true;

	compabilityMap["BlendingTexturedMaterial"]["SolidColorMaterial"] = true;
	compabilityMap["BlendingTexturedMaterial"]["TexturedMaterial"] = true;
	compabilityMap["BlendingTexturedMaterial"]["BlendingTexturedMaterial"] = true;

	compabilityMap["MeshRendererComponent"]["MeshRendererComponent"] = true;

	compabilityMap["TransformComponent"]["TransformComponent"] = true;

}

ComponentCompabilityManager::~ComponentCompabilityManager()
{
	compabilityMap.clear();
}
std::vector<std::string> ComponentCompabilityManager::getCompatibleComponents(std::string componentName)
{
	std::vector<std::string> ret;
	for (auto	it = compabilityMap[componentName].begin();
				it != compabilityMap[componentName].end();
				it++)
	{
		if (it->second == true)
		{
			ret.push_back(it->first);
		}

	}
	return ret;
}