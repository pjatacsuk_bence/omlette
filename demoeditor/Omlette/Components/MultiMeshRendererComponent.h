#ifndef INC_MULTIMESHRENDERERCOMPONENT
#define INC_MULTIMESHRENDERERCOMPONENT


#include <Omlette\Components\MeshRendererComponent.h>
#include <Omlette\Components\Component.h>
#include <Omlette\Interfaces\IDrawable.h>

#include <Omlette\Interfaces\IComponentManager.h>

namespace Oml
{
class MultiMeshRendererComponent : public Component, public IDrawable, public IUpdateable, public ISerializable
{
public:
	typedef boost::shared_ptr<MultiMeshRendererComponent> P;
	static P create(std::string name, std::string path, TransformComponent::P tfc=NULL, MaterialComponent::P mat = NULL)
	{
		return P(new MultiMeshRendererComponent(name, path, tfc,mat));
	}
				~MultiMeshRendererComponent();

	int			getInterfaces();
	void		draw(RenderParameters renderParameters);
	void		update(float t);
	void		serialize(pugi::xml_node& node);
	void		deserialize(pugi::xml_node& node);
	void		buildDependencies();
	void		updateDependencies();
	void		loadMultiMesh(std::string path, MaterialComponent::P mat);
	void		addMesh(Mesh::Geometry::P geometry,int meshNumber, MaterialComponent::P mat);
	void		addMesh(Mesh::Geometry::P geometry,int meshNumber, MaterialComponent::P mat, Oml::IPickable::SpecialID id, std::string path = "");
	void		setTransformComponent(TransformComponent::P tc);

	inline	static	std::string getTypeName(){ return "MultiMeshRendererComponent"; }
private:
							MultiMeshRendererComponent(std::string name, std::string path="",  
														TransformComponent::P tfc=NULL, MaterialComponent::P mat = NULL);
	TransformComponent::P	transformComponent;
	std::string				path;
	std::string				transformEntityName;
	std::string				transformComponentName;
	std::string				materialEntityName;
	std::string				materialComponentName;

};

}
#endif // !INC_MULTIMESHRENDERERCOMPONENT