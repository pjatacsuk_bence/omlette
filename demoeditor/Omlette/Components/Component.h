#ifndef INC_COMPONENT_H
#define INC_COMPONENT_H

#include "boost\shared_ptr.hpp"
#include <string>
#include "Omlette\Interfaces\IDrawable.h"
#include "Omlette\Interfaces\IUpdateable.h"
#include "Omlette\Interfaces\IGuiDrawable.h"
#include "Omlette\Interfaces\ISerializable.h"
#include <Omlette\Interfaces\ITimelineDrawable.h>
#include <Omlette\Framework\Properties\Property.h>
#include <boost\enable_shared_from_this.hpp>
#include <vector>
#include <map>

namespace Oml {
class Component : public ITimelineDrawable, public  boost::enable_shared_from_this<Component>
{
friend class ComponentFactory;
friend class ComponentMap;
friend class IComponentManager;
public:
	enum TYPE
	{
		DRAWABLE=0x0001,
		UPDATEABLE=0x0002,
		SERIALIZABLE=0x0004,
	};
	typedef boost::shared_ptr<Component> P;
	typedef	std::map<std::string,IPropertyAncestor::P> PropertyMap;

	inline				std::string	getName(){return name;}
	inline				std::string	getEntityName(){return entityName;}
	inline				std::string	getComponentTypeName(){return componentTypeName;}
	


	PropertyMap&		getPropertyMap(){return propertyMap;}
	Component::P		getComponentFromPoint(Egg::Math::int2 point, D2D1_RECT_F rect);
	virtual int			getInterfaces()=0;
	virtual void		buildDependencies();
	virtual void		updateDependencies();
	virtual	void		setEntityName(std::string ename);
	virtual void		timelineDraw(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget, ID2D1SolidColorBrush* brush, Egg::Math::float1 timelineMinTime, Egg::Math::float1 timelineMaxTime);
	virtual	void		addNewKeyFrame(Egg::Math::int2 point, D2D1_RECT_F rect);
	virtual	void		deleteKeyFramesByPoint(Egg::Math::int2 point, D2D1_RECT_F rect);
			int			getTimelineHeight();


	std::vector<IPropertyAncestor::P>	getPropertiesFromPoint(Egg::Math::int2 point, D2D1_RECT_F rect);
	IComponentManager*					getComponentManager(){ return componentMap; }

protected:

						Component(std::string name, std::string ctn);
	virtual			 	~Component();
	std::string		 	name;
	std::string			entityName;
	std::string			componentTypeName;
	IComponentManager*	componentMap;
	PropertyMap			propertyMap;	
	unsigned int		type;


};
}

#endif