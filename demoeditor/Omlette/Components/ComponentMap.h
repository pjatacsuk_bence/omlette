#ifndef INC_COMPONENTMAP_H
#define INC_COMPONENTMAP_H

#include <map>
#include <Omlette\Interfaces\IComponentManager.h>
#include <Omlette\Interfaces\IDrawable.h>

namespace Oml{
class ComponentMap : public IComponentManager
{
public:	
						ComponentMap(std::string ename);
						~ComponentMap();	

	struct IDrawablePack
	{
		IDrawable::P iDrawable;
		std::string  componentName;
	};
	struct ISerializablePack
	{
		ISerializable::P	iSerializable;
		std::string			componentName;
	};
	struct IUpdateablePack
	{
		IUpdateable::P	iUpdateable;
		std::string		componentName;
	};
	Component::P						getComponent(std::string name);
	std::vector<Component::P>			getComponentsByType(std::string componentTypeName);
	void								addComponent(Component::P component);				
	void								swapComponent(std::string componentName, Component::P component);
	int									getSize();


	void								serialize(pugi::xml_node node);
	void								deserialize(pugi::xml_node node);

	std::vector<IDrawable::P>			getDrawableComponents();
	std::vector<IUpdateable::P>			getUpdateableComponents();
	std::vector<ISerializable::P>		getSerializableComponents();

	void								draw(IDrawable::RenderParameters renderParameters);
	void								guiDraw();
	void								timelineDraw(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget, ID2D1SolidColorBrush* brush, Egg::Math::float1 timelineMinTime, Egg::Math::float1 timelineMaxTime);
	int									getTimelineHeight();
	void								update(float t);
	void								buildDependencies();
	void								updateDependencies();
	void								deleteInterfacesFromComponent(std::string componentName);
	void								setEntityName(std::string ename);
	void								addNewKeyFrame(Egg::Math::int2 point, D2D1_RECT_F rect);
	void								deleteKeyFramesByPoint(Egg::Math::int2 point, D2D1_RECT_F rect);
	std::vector<IPropertyAncestor::P>	getPropertiesFromPoint(Egg::Math::int2 point, D2D1_RECT_F rect);
	Component::P						getComponentFromPoint(Egg::Math::int2 point, D2D1_RECT_F rect);
	std::vector<std::string>			getNames();
	std::vector<Component::P>			getComponents();
private:
	std::map<std::string,Component::P>	components;
	std::vector<IDrawablePack>			drawableComponents;
	std::vector<IUpdateablePack>		updateableComponents;
	std::vector<ISerializablePack>		serializableComponents;			


	typedef std::map<std::string,Component::P>::iterator	iterator;
	typedef std::vector<IDrawable::P>::iterator				drawableIterator;		
	typedef std::vector<IUpdateable::P>::iterator			updateableIterator;		
	typedef std::vector<ISerializable::P>::iterator			serializableIterator;		
};
}

#endif