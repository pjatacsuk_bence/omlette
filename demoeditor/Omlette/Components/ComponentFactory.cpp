#include "DXUT.h"
#include "ComponentFactory.h"
#include <Omlette\Framework\Utility\Logger.h>
#include "DummyComponent.h"
#include <Omlette\Components\BlendingTexturedMaterial.h>
#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <Omlette\Mesh\MeshImporter.h>
#include <Omlette\Framework\Utility\ResourceManager.h>
#include <Omlette\Entities\EntityManager.h>
#include <Omlette\Components\TexturedMaterial.h>
#include <Omlette\Components\BlendingTexturedMaterial.h>
#include <Omlette\Components\LightComponent.h>
#include <Omlette\Components\SingleSolidColorMaterial.h>
using namespace Oml;

Oml::Component::P	ComponentFactory::createDummyComponent(std::string name)
{
	return boost::shared_ptr<Component>(new DummyComponent(name));
}

Oml::Component::P	ComponentFactory::createDummyComponent(pugi::xml_node& node)
{
	boost::shared_ptr<DummyComponent> ret = 
		boost::shared_ptr<DummyComponent>(new DummyComponent(""));
	ret->deserialize(node);
	return ret;
}

Oml::Component::P ComponentFactory::createMaterialComponent(pugi::xml_node& node)
{
	pugi::xml_attribute nameNode = node.first_child().next_sibling().first_attribute();
	std::string name = nameNode.value();
	MaterialComponent::P ret;
	if(name.compare("SolidColorMaterial") == 0)
	{
		ret = SolidColorMaterial::create("");
		ret->deserialize(node);
	}
	if(name.compare("TexturedMaterial") == 0)
	{
		ret = TexturedMaterial::create("");
		ret->deserialize(node);
	}
	if(name.compare("BlendingTexturedMaterial") == 0)
	{
		ret = BlendingTexturedMaterial::create("");
		ret->deserialize(node);
	}
	return ret;
}
Oml::Component::P	ComponentFactory::createTransformComponent(std::string name, TransformComponent::P parent,bool needPivot)
{
	return boost::shared_ptr<Component>(new TransformComponent(name, parent,0, needPivot));
}
Oml::Component::P ComponentFactory::createTransformComponent(pugi::xml_node& node)
{
	boost::shared_ptr<TransformComponent> ret = 
		boost::shared_ptr<TransformComponent>(new TransformComponent(""));
	ret->deserialize(node);
	return ret;
}

Oml::Component::P ComponentFactory::createMeshRendererComponent(std::string name, std::string path, MaterialComponent::P material, Oml::Component::P transform)
{

	TransformComponent::P tc = boost::dynamic_pointer_cast<TransformComponent, Component>(transform);

	return boost::shared_ptr<MeshRendererComponent>(new MeshRendererComponent(name, path, material, tc));
}


Oml::Component::P ComponentFactory::createMeshRendererComponent(pugi::xml_node& node)
{

	MeshRendererComponent::MeshRendererComponentDescription desc =	MeshRendererComponent::deserializeToDesc(node);

	return	createMeshRendererComponent(desc);
}
Oml::Component::P	ComponentFactory::createLightComponent(pugi::xml_node& node)
{
	LightComponent::P lc = LightComponent::create("");
	lc->deserialize(node);
	return lc;
}
Oml::Component::P	ComponentFactory::createMultiMeshRendererComponent(pugi::xml_node& node)
{
	MultiMeshRendererComponent::P mmrc = MultiMeshRendererComponent::create("", "");
	mmrc->deserialize(node);
	return mmrc;
}

Oml::Component::P ComponentFactory::createMeshRendererComponent(MeshRendererComponent::MeshRendererComponentDescription desc)
{
	MeshRendererComponent::P mc =	boost::shared_ptr<MeshRendererComponent>(new MeshRendererComponent(desc));
	return mc;
}

Oml::Component::P	ComponentFactory::createComponent(std::wstring componentTypeName)
{
	Component::P ret = NULL;
	std::string ctn(componentTypeName.begin(), componentTypeName.end());
	if (componentTypeName.compare(L"SingleSolidColorMaterial") == 0)
	{
		ret = SingleSolidColorMaterial::create(ctn);
	}
	if (componentTypeName.compare(L"SolidColorMaterial") == 0)
	{
		ret = SolidColorMaterial::create(ctn);
	}
	if (componentTypeName.compare(L"TexturedMaterial") == 0)
	{
		ret = TexturedMaterial::create(ctn);
	}
	if (componentTypeName.compare(L"BlendingTexturedMaterial") == 0)
	{
		ret = BlendingTexturedMaterial::create(ctn);
	}
	return ret;
}
Oml::Component::P	ComponentFactory::deserialize(pugi::xml_node& node)
{
	Logger::getInstance().consoleLog(node.name());
	Component::P ret;
	if(std::string("DummyComponent").compare(node.name()) == 0)
	{
		ret =  createDummyComponent(node);	
	}
	if(std::string("TransformComponent").compare(node.name()) == 0)
	{
		ret =  createTransformComponent(node);	
	}
	if(std::string("MeshRendererComponent").compare(node.name()) == 0)
	{
		ret = createMeshRendererComponent(node);	
	} 
	if(std::string("MaterialComponent").compare(node.name()) == 0)
	{
		ret = createMaterialComponent(node);
	}
	if (std::string("MultiMeshRendererComponent").compare(node.name()) == 0)
	{
		ret = createMultiMeshRendererComponent(node);
	}
	if (std::string("LightComponent").compare(node.name()) == 0)
	{
		ret = createLightComponent(node);
	}

return ret;
}

