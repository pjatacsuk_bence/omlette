#include "DXUT.h"
#include <Omlette\Components\TransformComponent.h>
#include <Omlette\Framework\Utility\Logger.h>
#include <Omlette\Entities\EntityManager.h>
#include <Omlette\Framework\Utility\Caster.h>
#include <Omlette\Components\SingleSolidColorMaterial.h>
using namespace Oml;

#define M_PI 3.14159265358979323846
TransformComponent::TransformComponent(std::string name, TransformComponent::P parent, int constraints, bool needPivot):
Component(name,"TransformComponent")
{
	this->name = name;
	if (parent.get() == this)
	{
		this->parent = NULL;
	}
	else
	{
		this->parent = parent;
	}
	this->parentConstraints = constraints;
	this->needPivot = needPivot;
	Logger::getInstance().consoleLog("TransformComponent constructor");	
	position = boost::shared_ptr<Property<Egg::Math::float4>>(new Property<Egg::Math::float4>("position"));
	rotationAngles = boost::shared_ptr<Property<Egg::Math::float3>>(new Property<Egg::Math::float3>("rotationAngles"));
	scale = boost::shared_ptr<Property<Egg::Math::float3>>(new Property<Egg::Math::float3>("scale"));
	propertyMap[position->getName()] = position;
	propertyMap[rotationAngles->getName()] = rotationAngles;
	propertyMap[scale->getName()] = scale;

	globalScale = Egg::Math::float3(1, 1, 1);


	pivotPoint = NULL;


}

TransformComponent::~TransformComponent()
{
	
}

Egg::Math::float4 TransformComponent::getQuatFromEulerAngles(Egg::Math::float3 angles)
{
	Egg::Math::float3 radians = Egg::Math::float3(	angles.x / 180.0 * M_PI,
													angles.y / 180.0 * M_PI,
													angles.z / 180.0 * M_PI);
	Egg::Math::float4 quat = Egg::Math::float4::quatAxisAngle(Egg::Math::float3(0,1,0),radians.y);
	quat = quat.quatMul(Egg::Math::float4::quatAxisAngle(Egg::Math::float3(1,0,0),radians.x));
	quat = quat.quatMul(Egg::Math::float4::quatAxisAngle(Egg::Math::float3(0,0,1),radians.z));
	return quat;
}
Egg::Math::float3 TransformComponent::getPositionValue(float t)
{
	Egg::Math::float4 pos = Egg::Math::float4(position->getValue(t).xyz,1.0);
	if (parent != NULL)
	{
		pos = pos * parent->getTransform(t,parentConstraints);
	}
	return pos.xyz;
}
Egg::Math::float4x4	TransformComponent::getTransform(float t, int constraints)
{

	Egg::Math::float4x4 ret = Egg::Math::float4x4::identity;
	Egg::Math::float4 quat = getQuatFromEulerAngles(rotationAngles->getValue(t));
	Egg::Math::float3 radians = Egg::Math::float3(	rotationAngles->getValue(t).x / 180.0 * M_PI,
													rotationAngles->getValue(t).y / 180.0 * M_PI,
													rotationAngles->getValue(t).z / 180.0 * M_PI);
	if (!(constraints & Constraint::NO_SCALE))
	{
		ret = ret * ret.scaling(scale->getValue(t) * globalScale);
	}
	if (!(constraints & Constraint::NO_ROTATION))
	{
		Egg::Math::float3 pivpoint; 
		if (pivotPoint != NULL)
		{
			pivpoint = pivotPoint->getPositionValue(t);
		}
		ret = ret * ret.translation(-pivpoint);
		ret = ret *	ret.rotation(Egg::Math::float3(0, 1, 0), radians.y);
		ret = ret *	ret.rotation(Egg::Math::float3(1, 0, 0), radians.x);
		ret = ret *	ret.rotation(Egg::Math::float3(0, 0, 1), radians.z);
		ret = ret * ret.translation(pivpoint);
	}
	if (!(constraints & Constraint::NO_POSITION))
	{
		ret = ret * ret.translation(position->getValue(t).xyz);
	}
	if (parent != NULL)
	{
		ret = ret * parent->getTransform(t,parentConstraints | constraints);
	}
	return ret;
}

void	TransformComponent::rotate(Egg::Math::float3 axis,Egg::Math::float1 angle)
{

}

void	TransformComponent::translate(Egg::Math::float3	trans)
{
	position->addKeyFrame(new KeyFrame<Egg::Math::float4>(20000,Egg::Math::float4(trans.xyz,1)));
}

void	TransformComponent::update(float t)
{
	transformMatrix	= Egg::Math::float4x4::identity;
	transformMatrix.translation(position->getValue(t).xyz);
//	transformMatrix.rotation(quat->getValue(t).xyz,quat->getValue(t).w);
}

void	TransformComponent::serialize(pugi::xml_node& node)
{
	
	Logger::getInstance().consoleLog("TransformComponent->serialize()");	

    pugi::xml_node sub_node = node.append_child("TransformComponent");

    pugi::xml_node node_name = sub_node.append_child("name");
	node_name.append_child(pugi::node_pcdata).set_value(name.c_str());

    pugi::xml_node parentNode = sub_node.append_child("parent");

	pugi::xml_attribute parentEntityNameAttr = parentNode.append_attribute("entity");
	parentEntityNameAttr.set_value(parent->getEntityName().c_str());

	pugi::xml_attribute parentComponentNameAttr = parentNode.append_attribute("component");
	parentComponentNameAttr.set_value(parent->getName().c_str());

	pugi::xml_node parameters = sub_node.append_child("Properties");
	position->serialize(parameters);
	rotationAngles->serialize(parameters);
	scale->serialize(parameters);
}

void	TransformComponent::deserialize(pugi::xml_node& node)
{
	for (pugi::xml_node param = node.first_child(); param; param = param.next_sibling())
	{
		if(std::string("name").compare(param.name())==0)
		{
			name = param.first_child().value();
		}
		if(std::string("parent").compare(param.name())==0)
		{
			parentEntityName = param.first_child().first_attribute().as_string();
			parentComponentName = param.first_child().first_attribute().next_attribute().as_string();
		}
		if(std::string("Properties").compare(param.name())==0)
		{
			pugi::xml_node paramFirst = param.first_child();
			position->deserialize(paramFirst);
			rotationAngles->deserialize(paramFirst.next_sibling());
			scale->deserialize(paramFirst.next_sibling().next_sibling());
		}
	}

	Logger::getInstance().consoleLog("DummyComponent->deserialize()");	
}

int		TransformComponent::getInterfaces()
{
	return UPDATEABLE | SERIALIZABLE;
}


void TransformComponent::buildDependencies()
{
	if (parentEntityName.size() != 0 && parentComponentName.size() != 0)
	{
		Component::P parentComp = EntityManager::getInstance().getComponentFromEntity(parentEntityName, parentComponentName);
		if (parentComp != NULL)
		{
			TransformComponent::P castedParentComp = Caster::getInstance().getComponentCasted<TransformComponent::P>(parentComp);
			setParent(castedParentComp);
		}

	}
	if (pivotPoint == NULL && needPivot)
	{
		SingleSolidColorMaterial::P scmBlue = SingleSolidColorMaterial::create("scmBlue", Egg::Math::float4(0, 0, 1, 1), false);
		TransformComponent::P tcThis = Caster::getInstance().getComponentCasted<TransformComponent::P>(shared_from_this());
		MeshRendererComponent::P mesh = MeshRendererComponent::create("pivotPoint", "./Resources/Meshes/SphereMesh.obj", scmBlue, tcThis, false);
		componentMap->addComponent(mesh);
		TransformComponent::P comp = mesh->getTransformComponent();
		pivotPoint = comp;
		pivotPoint->setParentConstraints(Constraint::NO_ROTATION | Constraint::NO_SCALE);
	}
}

void TransformComponent::updateDependencies()
{
	if (parent != NULL)
	{
		parentEntityName = parent->getEntityName();
		parentComponentName = parent->getName();
		buildDependencies();
	}
}
void	TransformComponent::setParent(TransformComponent::P parent)
{
	if (parent.get() == this) 
	{
		this->parent = NULL;
	}
	else
	{
		this->parent = parent;
		name = parent->getName() + "_child";
	}
}