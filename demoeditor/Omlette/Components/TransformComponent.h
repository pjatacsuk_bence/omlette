#ifndef	INC_TRANSFORMCOMPONENT_H
#define INC_TRANSFORMCOMPONENT_H
#include "Component.h"
#include "boost\shared_ptr.hpp"
#include <boost\enable_shared_from_this.hpp>
namespace Oml
{
class TransformComponent : public Component,public IUpdateable,public ISerializable
{
friend class ComponentFactory;
public:
	typedef boost::shared_ptr<TransformComponent> P;

	enum Constraint
	{
		NO_ROTATION		=	0x1,
		NO_SCALE		=	0x2,
		NO_POSITION		=	0x4
	};
	
							~TransformComponent();
	Egg::Math::float4x4		getTransform(float t, int constraints = 0);
	void					rotate(Egg::Math::float3 axis,Egg::Math::float1 angle);
	void					translate(Egg::Math::float3 trans);
	void					update(float t);
	void					serialize(pugi::xml_node&	node);
	void					deserialize(pugi::xml_node&	node);
	void					buildDependencies();
	void					updateDependencies();
	int						getInterfaces();
	Egg::Math::float4		getQuatFromEulerAngles(Egg::Math::float3 angles);
	Egg::Math::float3		getPositionValue(float t);

	inline	Egg::Math::float3		getGlobalScale(){ return globalScale; }
	inline	void					setGlobalScale(Egg::Math::float3 globalScale){ this->globalScale = globalScale; }

	inline	TransformComponent::P	getParent(){ return parent; }
	void							setParent(TransformComponent::P parent);
	inline	int						getParentConstraints(){ return parentConstraints; }
	inline	void					setParentConstraints(int constraints){ parentConstraints = parentConstraints | constraints; }


	inline static	std::string		getTypeName(){ return "TransformComponent"; }

	
	
protected:
				TransformComponent(std::string name="", TransformComponent::P parent = NULL, int constraints = 0,bool needPivot=false);
private:
	Property<Egg::Math::float4>::P	position;
	Property<Egg::Math::float3>::P	rotationAngles;
	Property<Egg::Math::float3>::P	scale;
	Egg::Math::float4x4			transformMatrix;

	TransformComponent::P		parent;
	TransformComponent::P		pivotPoint;
	bool						needPivot;
	std::string					parentEntityName;
	std::string					parentComponentName;
	int							parentConstraints;

	Egg::Math::float3			globalScale;

};	

}

#endif