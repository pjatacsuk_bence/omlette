#ifndef	INC_SINGLESOLIDCOLORMATERIAL_H
#define INC_SINGLESOLIDCOLORMATERIAL_H
#include <Omlette\Components\MaterialComponent.h>
#include <boost\shared_ptr.hpp>
namespace Oml
{
class SingleSolidColorMaterial : public MaterialComponent
{
friend class ComponentFactory;
public:
	typedef boost::shared_ptr<SingleSolidColorMaterial> P;
	static P	create(std::string name, Egg::Math::float4 color=Egg::Math::float4(0,0,0,1),bool depthTest = false)
	{
		return P(new SingleSolidColorMaterial(name,color,depthTest));
	}	
	void					serializeProperties(pugi::xml_node&	node);
	void					deserializeProperties(pugi::xml_node&	node);
	void					applyVariables(ID3DX11Effect* effect);
	
protected:
							SingleSolidColorMaterial(std::string name="", Egg::Math::float4 color=Egg::Math::float4(0,0,0,1), bool depthTest = false);
	
	Egg::Math::float4		color;

};	

}

#endif