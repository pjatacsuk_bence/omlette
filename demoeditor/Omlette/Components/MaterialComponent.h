#ifndef	INC_MATERIALCOMPONENT_H
#define INC_MATERIALCOMPONENT_H
#include <Omlette\Components\Component.h>
#include <boost\shared_ptr.hpp>
namespace Oml
{
	class MaterialComponent : public Component, public IUpdateable, public ISerializable
	{
		friend class ComponentFactory;
	public:
		typedef boost::shared_ptr<MaterialComponent> P;

		virtual					~MaterialComponent();
		virtual void			update(float t);
		virtual void			applyVariables(ID3DX11Effect* effect);
		std::string				getMaterialName(){ return materialName; }
		void					serialize(pugi::xml_node&	node);
		virtual	void			serializeProperties(pugi::xml_node& node);
		void					deserialize(pugi::xml_node&	node);
		virtual	void			deserializeProperties(pugi::xml_node& node);
		int						getInterfaces();
		Mesh::Material::P		getMaterial(){ return material; }
		Mesh::Material::P		getAmbientMaterial(){ return ambientMaterial; }
		Mesh::Material::P		getShadowMaterial(){ return shadowMaterial; }
		Mesh::Material::P		getPickerMaterial(){ return pickerMaterial; }
		Mesh::Material::P		getPickedMaterial(){ return pickedMaterial; }
protected:
							MaterialComponent(std::string name="", std::string materialName = "");
	Mesh::Material::P		material;
	Mesh::Material::P		ambientMaterial;
	Mesh::Material::P		shadowMaterial;
	Mesh::Material::P		pickerMaterial;
	Mesh::Material::P		pickedMaterial;
	float					time;
	std::string				technique;
	std::string				pass;
	std::string				materialName;

	struct isNodeNameProperties 
	{
		bool operator() (pugi::xml_node node) const
		{
			return std::string("Properties").compare(node.name()) == 0;
		}
	};

};	

}

#endif