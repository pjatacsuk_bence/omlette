#ifndef INC_MESHRENDERERCOMPONENT_H
#define INC_MESHRENDERERCOMPONENT_H
#include "Omlette\Components\Component.h"
#include "boost\shared_ptr.hpp"
#include "Omlette\Components\TransformComponent.h"
#include "Omlette\Mesh\ShadedMesh.h"
#include  <Omlette\Interfaces\IPickable.h>
#include <Omlette\Components\MaterialComponent.h>
#include <Omlette\Mesh\FlipMesh.h>
namespace Oml
{
class MeshRendererComponent : public Component,public IUpdateable, public ISerializable, public IDrawable, public IPickable
{
friend class ComponentFactory;
friend class MultiMeshRendererComponent;
public:
	typedef boost::shared_ptr<MeshRendererComponent> P;

	struct MeshRendererComponentDescription
	{
		std::string			name;
		std::string			path;
		int					meshNumber;
		std::string			transComponentEntityName;
		std::string			transComponentName;	
		std::string			materialComponentEntityName;
		std::string			materialComponentName;	
		
	};
	
	static MeshRendererComponentDescription	deserializeToDesc(pugi::xml_node&	node)
	{
		MeshRendererComponentDescription desc;
		pugi::xml_attribute nameAttr = node.first_attribute();
		desc.name = nameAttr.as_string();

		pugi::xml_node pathNode = node.first_child();
		desc.path = pathNode.first_child().value();

		pugi::xml_node meshNumberNode = pathNode.next_sibling();
		desc.meshNumber = atoi(meshNumberNode.first_child().value());

		pugi::xml_node transNode = meshNumberNode.next_sibling();

		pugi::xml_attribute transEntityName = transNode.first_attribute();
		desc.transComponentEntityName = transEntityName.as_string();

		pugi::xml_attribute transCompName = transEntityName.next_attribute();
		desc.transComponentName = transCompName.as_string();

		pugi::xml_node materialCompNode = transNode.next_sibling();

		pugi::xml_attribute materialEntityName = materialCompNode.first_attribute();
		desc.materialComponentEntityName = materialEntityName.as_string();

		pugi::xml_attribute materialCompName = materialEntityName.next_attribute();
		desc.materialComponentName = materialCompName.as_string();
		return desc;	
	}
	
	static P create(std::string name, std::string path, MaterialComponent::P material, TransformComponent::P transform, bool needPivot=false)
	{
		return P(new MeshRendererComponent(name, path, material, transform, needPivot));
	}
	virtual					~MeshRendererComponent();
	void					update(float t);
	void					draw(RenderParameters renderParameters);
	void					serialize(pugi::xml_node&	node);
	void					deserialize(pugi::xml_node&	node);
	void					buildDependencies();
	void					updateDependencies();
	void					setDesc(MeshRendererComponentDescription desc){meshDesc = desc;}
	void					initResources(Mesh::Geometry::P geometry = NULL);
	int						getInterfaces();
	void					replaceMesh(Mesh::Geometry::P geometry);
	void					addMien(Mesh::Mien mien, Mesh::ShadedMesh::P shadedMesh);

	inline TransformComponent::P	getTransformComponent(){ return transformComponent; }
	inline static	std::string		getTypeName(){ return "MeshRendererComponent"; }
	
protected:
	MeshRendererComponent(std::string name,std::string path, MaterialComponent::P material, TransformComponent::P transform, bool needPivot = true);
	MeshRendererComponent(std::string name,std::string path, int meshNumber, Mesh::Geometry::P, MaterialComponent::P material, TransformComponent::P transform, bool needPivot = true);
	MeshRendererComponent(std::string name,std::string path, int meshNumber, Mesh::Geometry::P, MaterialComponent::P material, TransformComponent::P transform, Oml::IPickable::SpecialID id, bool needPivot = false);
	MeshRendererComponent(MeshRendererComponentDescription desc);
//	MeshRendererComponent(std::string name):Component(name,"MeshRendererComponent"){}
private:
	Egg::Math::float4x4		transformMatrix;
	Mesh::FlipMesh::P		flipMesh;
	TransformComponent::P	transformComponent;
	std::string				path;
	int						meshNumber;
	std::string				technique;
	std::string				pass;
	MaterialComponent::P	materialComponent;


	MeshRendererComponentDescription meshDesc;
};
}


#endif