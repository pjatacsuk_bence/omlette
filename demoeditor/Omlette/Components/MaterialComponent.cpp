#include "DXUT.h"
#include <Omlette\Components\MaterialComponent.h>
#include <Omlette\Framework\Utility\ResourceManager.h>
#include <Omlette\Framework\Utility\Caster.h>
using namespace Oml;


MaterialComponent::MaterialComponent(std::string name,std::string materialName):
Component(name,"MaterialComponent")
{
	this->name = name;
	this->materialName = materialName;
	pickerMaterial = ResourceManager::getInstance().getMaterial("pickerMaterialMesh");
	pickedMaterial = ResourceManager::getInstance().getMaterial("pickedMaterialMesh");
}

MaterialComponent::~MaterialComponent()
{
	material.reset();
}

/*
Mesh::Material MaterialComponent::getMaterial()
{
	return material;
}*/

void	MaterialComponent::update(float t)
{
	time = t;
}

void MaterialComponent::applyVariables(ID3DX11Effect* effect)
{
	auto i = propertyMap.begin();
	auto e = propertyMap.end();

	while(i != e)
	{
		
		if(i->second->getType() == FLOAT4)
		{
			Property<Egg::Math::float4>::P prop = 
			Caster::getInstance().getPropertyCasted<Property<Egg::Math::float4>::P>(i->second);
			effect->GetVariableByName(i->second->getName().c_str())
				->AsVector()->SetFloatVector((float*)(& (prop->getValue(time))));
		}
		i++;
	}
}

void	MaterialComponent::serialize(pugi::xml_node& node)
{
	
    pugi::xml_node sub_node = node.append_child("MaterialComponent");

    pugi::xml_node node_name = sub_node.append_child("name");
	node_name.append_child(pugi::node_pcdata).set_value(name.c_str());
	
	pugi::xml_node mtNode = sub_node.append_child("material");

	pugi::xml_attribute mtNameNode = mtNode.append_attribute("name");
	mtNameNode.set_value(materialName.c_str());

	pugi::xml_attribute techNode = mtNode.append_attribute("technique");
	techNode.set_value(technique.c_str());

	pugi::xml_attribute passNode = mtNode.append_attribute("pass");
	passNode.set_value(pass.c_str());

	pugi::xml_node parameters = sub_node.append_child("Properties");
	serializeProperties(parameters);
}

void MaterialComponent::serializeProperties(pugi::xml_node&)
{
}
void MaterialComponent::deserializeProperties(pugi::xml_node&)
{
}

void	MaterialComponent::deserialize(pugi::xml_node& node)
{
	for (pugi::xml_node param = node.first_child(); param; param = param.next_sibling())
	{
		if(std::string("name").compare(param.name())==0)
		{
			name = param.first_child().value();
		}
		if(std::string("material").compare(param.name())==0)
		{
			pugi::xml_attribute techNode = param.first_attribute().next_attribute();
			technique = techNode.value();

			pugi::xml_attribute passNode = techNode.next_attribute();
			pass = passNode.value();
		}
		if(std::string("Properties").compare(param.name())==0)
		{
			deserializeProperties(param);
		}
	}

}

int		MaterialComponent::getInterfaces()
{
	return UPDATEABLE | SERIALIZABLE;
}
