#include "DXUT.h"
#include "DummyComponent.h"
#include  <Omlette\Framework\Utility\Logger.h>
using namespace Oml;


DummyComponent::DummyComponent(std::string name):
Component(name,"DummyComponent")
{
	this->name = name;
	Logger::getInstance().consoleLog("DummyComponent constructor");	
	xx = Property<float>("xx");
}
DummyComponent::~DummyComponent()
{
	Logger::getInstance().consoleLog("DummyComponent->deconstructor()");	
}
void	DummyComponent::draw(RenderParameters renderParameters)
{
	Logger::getInstance().consoleLog("DummyComponent->draw()");	
}

void	DummyComponent::update(float t)
{
	Logger::getInstance().consoleLog("DummyComponent->update()");	
}

void	DummyComponent::guiDraw()
{
	Logger::getInstance().consoleLog("DummyComponent->guiDraw()");	
}

void	DummyComponent::serialize(pugi::xml_node& node)
{
	Logger::getInstance().consoleLog("DummyComponent->serialize()");	

    pugi::xml_node sub_node = node.append_child("DummyComponent");

    pugi::xml_node node_name = sub_node.append_child("name");
	node_name.append_child(pugi::node_pcdata).set_value(name.c_str());

    pugi::xml_node param = sub_node.insert_child_after("position", node_name);

    param.append_attribute("x") = x;
    param.append_attribute("y") = y;
    param.append_attribute("z") = z;

	pugi::xml_node parameters = sub_node.append_child("Properties");
	xx.serialize(parameters);



}

void	DummyComponent::deserialize(pugi::xml_node& node)
{
	for (pugi::xml_node param = node.first_child(); param; param = param.next_sibling())
	{
		if(std::string("name").compare(param.name())==0)
		{
			name = param.first_child().value();
		}
		if(std::string("Properties").compare(param.name())==0)
		{
			pugi::xml_node paramFirst = param.first_child();
			xx.deserialize(param);
		}
		for (pugi::xml_attribute attr = param.first_attribute(); attr; attr = attr.next_attribute())
		{
			if(std::string("x").compare(attr.name())==0)
			{
				x = atof(attr.value());
			}
			if(std::string("y").compare(attr.name())==0)
			{
				y = atof(attr.value());
			}
			if(std::string("z").compare(attr.name())==0)
			{
				z = atof(attr.value());
			}
		}
	}
	Logger::getInstance().consoleLog("DummyComponent->deserialize()");	

}

void	DummyComponent::timelineDraw()
{
	Logger::getInstance().consoleLog("DummyComponent->timelineDraw()");	
}

int		DummyComponent::getInterfaces()
{
	return DRAWABLE | UPDATEABLE | SERIALIZABLE;	
}