#ifndef	INC_TEXTUREDMATERIAL_H
#define INC_TEXTUREDMATERIAL_H
#include <Omlette\Components\MaterialComponent.h>
#include <boost\shared_ptr.hpp>
namespace Oml
{
class TexturedMaterial : public MaterialComponent
{
friend class ComponentFactory;
public:
	typedef boost::shared_ptr<TexturedMaterial> P;
	static P	create(std::string name){return P(new TexturedMaterial(name));}	
	void		serializeProperties(pugi::xml_node& node);
	void		deserializeProperties(pugi::xml_node& node);
	void		applyVariables(ID3DX11Effect* effect);
	void		loadTextures();
	
protected:
								TexturedMaterial(std::string name="");
								TexturedMaterial(std::string name,std::string materialName);
	
	Property<std::string>::P	textureName;

};	

}

#endif