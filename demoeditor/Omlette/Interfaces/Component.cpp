
#include "DXUT.h"
#include <Omlette\Components\Component.h>
#include <Omlette\Framework\Gui\GuiDrawManager.h>

using namespace Oml;

void Component::timelineDraw(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget, ID2D1SolidColorBrush* brush)
{
	PropertyMap::iterator i = propertyMap.begin();
	PropertyMap::iterator e = propertyMap.end();

	int h = 0;
	while(i != e)
	{
		
		D2D1_RECT_F propRect = rect;
		propRect.top = rect.top + h * GuiDrawManager::HEIGHT_FOR_PROPERTY;
		propRect.bottom = rect.top + (h+1) * GuiDrawManager::HEIGHT_FOR_PROPERTY;
		(*i).second->timelineDraw(propRect,renderTarget,brush);
		i++;
		h++;
	}
}

int Component::getTimelineHeight()
{
	return propertyMap.size() * GuiDrawManager::HEIGHT_FOR_PROPERTY;
}
