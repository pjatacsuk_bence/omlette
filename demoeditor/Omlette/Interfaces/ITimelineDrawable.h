#ifndef INC_ITIMELINEDRAWABLE_H
#define INC_ITIMELINEDRAWABLE_H
#include <D2D1.h>
#include <Math\math.h>

namespace Oml {
class ITimelineDrawable
{
public:
	virtual			~ITimelineDrawable(){}
	virtual	void	timelineDraw(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget, ID2D1SolidColorBrush* brush,Egg::Math::float1 timelineMinTime, Egg::Math::float1 timelineMaxTime)=0;
private:
};
}

#endif