#ifndef INC_PICKABLE_H
#define INC_PICKABLE_H
#include <string>
#include <Math\math.h>
#include <Omlette\Interfaces\IDrawable.h>
#include <stdint.h>
#include <Omlette\Components\Component.h> 
#include <Omlette\Framework\Utility\PickableManager.h>

namespace Oml
{
class IPickable
{
friend class PickableManager;
public:
	static const uint32_t UNPICKABLE = 1;
	enum SpecialID
	{
		transformX = 0xFFFFFF,
		transformY = 0xFFFFFE,
		transformZ = 0xFFFFFD 
	};

	IPickable(Oml::Component* parent,IDrawable* drawable);
	IPickable(Oml::Component* parent, IDrawable* drawable, SpecialID id);
	~IPickable();

	Egg::Math::float4 getUniqueColor()
	{
		using namespace Egg::Math;

		float1 r = unqiueID & 0xFF;
		float1 g = (unqiueID & (0xFF00)) >> 8;
		float1 b = (unqiueID & (0xFF0000)) >> 16;

		r = r / (float1)255.0;
		g = g / (float1)255.0;
		b = b / (float1)255.0;

		return float4(r,g,b,1);
	}

	static uint32_t getUnqiueIDFromColor(Egg::Math::float4 color)
	{
		using namespace Egg::Math;
		uint32_t r = color.x * 255.0;
		uint32_t g = color.y * 255.0;
		uint32_t b = color.z * 255.0;

		uint32_t rr = r;
		uint32_t rg = g << 8;
		uint32_t rb = b << 16;

		uint32_t result = rr | rg | rb;
		return result;
	}
	
	static uint32_t ids;

protected:
	uint32_t			unqiueID;	
	Oml::Component*		parent;
	IDrawable*			drawable;

						IPickable(){}

};

class Unpickable : public IPickable
{
public:
	Unpickable(IDrawable* drawable);

private:
};
}
#endif