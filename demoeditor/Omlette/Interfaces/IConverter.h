#ifndef INC_ICONVERTER_H
#define INC_ICONVERTER_H
#include <boost\shared_ptr.hpp>

namespace Oml{
	enum Converter{
		FROM_RGB_TO_COLOR,
		NO_CONVERT,
		SIZE
	};
template<class T>
class IConverter {
public:
	typedef boost::shared_ptr<IConverter> P;
	virtual T convert(T value) = 0;
};
}

#endif