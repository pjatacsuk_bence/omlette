#ifndef INC_ISERIALIZABLE_H
#define INC_ISERIALIZABLE_H

#include "pugixml.hpp"
#include "boost\shared_ptr.hpp"

namespace Oml {
class ISerializable
{
public:
	virtual			~ISerializable(){}
	virtual	void	serialize(pugi::xml_node& node)=0;
	virtual	void	deserialize(pugi::xml_node& node)=0;


	typedef	boost::shared_ptr<ISerializable>	P;

private:
};
}

#endif