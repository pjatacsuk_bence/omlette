#ifndef INC_IDRAWABLE_H
#define INC_IDRAWABLE_H

#include "boost\shared_ptr.hpp"
#include "d3dx11effect.h"
#include <Omlette\Mesh\Material.h>
#include <Omlette\Mesh\Mien.h>

namespace Oml {
class IDrawable
{
public:

	enum DrawableType
	{
		MESH = 0x1,
		QUAD = 0x2
	};
	struct RenderParameters
	{
		ID3D11DeviceContext* context;
		ID3DX11Effect* effect;
		Mesh::Mien* mien;
	};

	virtual			~IDrawable(){}
					IDrawable(DrawableType type=MESH):
					type(type),
					visible(true)
					{}
					DrawableType	getType(){return type;}
					bool			isVisible(){return visible;}
					void			setVisible(bool v){ visible = v; }



	virtual	void	draw(RenderParameters renderParameters)=0;
	

	typedef boost::shared_ptr<IDrawable> P;
private:
	DrawableType	type;
	bool			visible;


};
}

#endif