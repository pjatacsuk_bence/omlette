#ifndef INC_ICOMPONENTMANAGER_H
#define INC_ICOMPONENTMANAGER_H

#include <string>
#include "pugixml.hpp"
#include <Omlette\Components\Component.h>

#include <vector>
#include <Iterator>
namespace Oml {
class IComponentManager
{
public:
												IComponentManager(std::string ename):entityName(ename){}
	virtual										~IComponentManager(){}
	virtual	Oml::Component::P					getComponent(std::string name)=0;
	virtual	std::vector<Oml::Component::P>		getComponentsByType(std::string componentTypeName)=0;
	virtual	void								addComponent(Oml::Component::P component)=0;
	virtual	int									getSize()=0;
	virtual void								setEntityName(std::string ename) = 0;


	virtual	void								serialize(pugi::xml_node node)=0;
	virtual	void								deserialize(pugi::xml_node node)=0;

	virtual std::vector<IDrawable::P>			getDrawableComponents()=0;
	virtual std::vector<IUpdateable::P>			getUpdateableComponents()=0;
	virtual std::vector<ISerializable::P>		getSerializableComponents()=0;

	virtual void								swapComponent(std::string componentName, Oml::Component::P component) = 0;
	virtual	void								draw(IDrawable::RenderParameters renderParameters)=0;
	virtual	void								guiDraw()=0;
	virtual void								timelineDraw(D2D1_RECT_F rect, ID2D1RenderTarget* renderTarget, ID2D1SolidColorBrush* brush, Egg::Math::float1 timelineMinTime, Egg::Math::float1 timelineMaxTime)=0;
	virtual	int									getTimelineHeight()=0;
	virtual	void								update(float t)=0;
	virtual void								buildDependencies()=0;
	virtual void								updateDependencies()=0;
	virtual	void								addNewKeyFrame(Egg::Math::int2 point, D2D1_RECT_F rect)=0;
	virtual	void								deleteKeyFramesByPoint(Egg::Math::int2 point, D2D1_RECT_F rect)=0;
	virtual Component::P						getComponentFromPoint(Egg::Math::int2 point, D2D1_RECT_F rect)=0;
	virtual std::vector<IPropertyAncestor::P>	getPropertiesFromPoint(Egg::Math::int2 point, D2D1_RECT_F rect)=0;
	virtual std::vector<std::string>			getNames()=0;
	virtual std::vector<Component::P>			getComponents()=0;

	
protected:
	std::string								entityName;

};

}

#endif