#ifndef INC_IGUIELEMENT_H
#define INC_IGUIELEMENT_H

#include <WindowsX.h>
#include <D2D1.h>
#include <boost\shared_ptr.hpp>
namespace Oml {
class IGuiElement
{
public:
	typedef	boost::shared_ptr<IGuiElement> P;
	
									IGuiElement():dirty(false){}
	virtual							~IGuiElement(){}
	virtual	void					processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)=0;
	virtual void					draw(ID2D1RenderTarget* renderTarget)=0;
	virtual void					draw(ID2D1RenderTarget* renderTarget, D2D1_RECT_F& rect){};
	virtual void					execute(){}
	virtual D2D1_RECT_F				getRect(){return hitRect;}
	virtual void					setRect(D2D1_RECT_F rect){hitRect = rect;}
	virtual bool					isDirty(){ return dirty; }
protected:
	D2D1_RECT_F		hitRect;
	bool dirty;
};

}

#endif