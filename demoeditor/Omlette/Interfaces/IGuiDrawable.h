#ifndef INC_IGUIDRAWABLE_H
#define INC_IGUIDRAWABLE_H

namespace Oml {
class IGuiDrawable
{
public:
	virtual			~IGuiDrawable(){}
	virtual	void	guiDraw()=0;
private:
};
}

#endif