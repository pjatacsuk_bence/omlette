#include "DXUT.h"
#include <Omlette\Interfaces\IPickable.h>

uint32_t Oml::IPickable::ids = 2;


Oml::IPickable::IPickable(Oml::Component* parent,IDrawable* drawable)
{
	 this->unqiueID = ids++;	
	 this->parent = parent;
	 this->drawable = drawable;
	 PickableManager::getInstance().addPickable(this);
}
Oml::IPickable::IPickable(Oml::Component* parent,IDrawable* drawable, Oml::IPickable::SpecialID id)
{
	this->unqiueID = id;
	 this->parent = parent;
	 this->drawable = drawable;
	 PickableManager::getInstance().addPickable(this);
}

Oml::IPickable::~IPickable()
{
}
Oml::Unpickable::Unpickable(IDrawable* drawable)
{
	this->parent = NULL;
	this->drawable = drawable;
	this->unqiueID = 1;	
	PickableManager::getInstance().addPickable(this);
}