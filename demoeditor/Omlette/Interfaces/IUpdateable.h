#ifndef INC_IUPDATEABLE_H
#define INC_IUPDATEABLE_H

#include	"boost\shared_ptr.hpp"

namespace Oml {
class IUpdateable
{
public:
	virtual			~IUpdateable(){}
	virtual	void	update(float t)=0;

	typedef boost::shared_ptr<IUpdateable> P;
private:
};
}

#endif