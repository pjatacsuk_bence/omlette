#ifndef INC_CAMENTITY_H
#define INC_CAMENTITY_H
#include <Omlette\Entities\Entity.h>
#include "ICam.h"
#include <boost\shared_ptr.hpp>
namespace Oml
{
	

class CamEntity : public Entity
{
friend class EntityFactory;
public:
	typedef boost::shared_ptr<CamEntity> P;

	virtual					~CamEntity();
	void					serialize(pugi::xml_node& node);
	void					deserialize(pugi::xml_node& node);
	inline	void					setAspect(float aspect){ camera->setAspect(aspect); }


	inline	Cam::ICam::P	getICam(){return camera;}
	
protected:
	CamEntity(std::string name);
private:
	Cam::ICam::P	camera;
};
}

#endif