#include "DXUT.h"
#include "CamEntity.h"
#include <Omlette\Components\ComponentFactory.h>
using namespace Oml;

CamEntity::~CamEntity()
{
	camera.reset();
}

void CamEntity::serialize(pugi::xml_node&)
{
}

void CamEntity::deserialize(pugi::xml_node&)
{
}

CamEntity::CamEntity(std::string name):
Entity(name)
{
	componentManager->addComponent(ComponentFactory::getInstance().createTransformComponent("transform"));
}