#ifndef INC_FIRSTPERSONCAM_H
#define INC_FIRSTPERSONCAM_H

#include "ICam.h"
#include "boost\enable_shared_from_this.hpp"

namespace Oml
{
namespace Cam
{

class FirstPersonCam : public ICam, public boost::enable_shared_from_this<FirstPersonCam>
{
public:
	typedef	boost::shared_ptr<FirstPersonCam> P;
	static	P	create(){return P(new FirstPersonCam());}


	const	Egg::Math::float3&		getEyePosition();
	const	Egg::Math::float3&		getAhead();
	const	Egg::Math::float4x4&	getViewDirMatrix();
	const	Egg::Math::float4x4&	getViewDirMatrixBack();
	const	Egg::Math::float4x4&	getViewMatrix();
	const	Egg::Math::float4x4&	getProjMatrix();
			FirstPersonCam::P		setView(Egg::Math::float3 position,Egg::Math::float3 ahead);
			FirstPersonCam::P		setProj(float fov, float aspect, float front, float back);
			FirstPersonCam::P		setSpeed(float speed);


	virtual	void					animate(double dt);
	virtual	void					setAspect(float aspect);
	virtual void					processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

		
private:
	Egg::Math::float3	position;
	Egg::Math::float3	ahead;
	Egg::Math::float3	right;

	float				yaw;
	float				pitch;

	float				fov;
	float				aspect;
	float				nearPlane;
	float				farPlane;

	Egg::Math::float4x4	viewMatrix;
	Egg::Math::float4x4	projMatrix;
	Egg::Math::float4x4	viewDirMatrix;
	Egg::Math::float4x4	viewDirMatrixBack;

	float				speed;
	Egg::Math::int2		lastMousePos;
	Egg::Math::float2	mouseDelta;


	bool				wPressed;
	bool				aPressed;
	bool				sPressed;
	bool				dPressed;
	bool				qPressed;
	bool				ePressed;
	bool				shiftPressed;

	void				updateView();
	void				updateProj();

						FirstPersonCam();
};

}
}
#endif