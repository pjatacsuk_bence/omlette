#ifndef INC_CAMERAMANAGER_H
#define INC_CAMERAMANAGER_H
#include "ICam.h"
#include <map>
#include <Omlette\Framework\Properties\Property.h>
#include "CamEntity.h"
#include <Omlette\Interfaces\ISerializable.h>
#include <Omlette\Interfaces\IUpdateable.h>

namespace Oml
{
namespace Cam
{

class CameraManager :  public ISerializable, public IUpdateable
{
public:
	static	CameraManager& getInstance()
	{
		static CameraManager instance;
		return instance;
	}

	void			serialize(pugi::xml_node& node);
	void			deserialize(pugi::xml_node& node);
	void			update(float t);
	void			setAspect(float aspect);


	ICam::P			getCurrentCam();
	inline void		setEditorCam(ICam::P editorCam){this->editorCam = editorCam;}

					~CameraManager();
protected:
	CameraManager();
private:
	std::map<std::string, CamEntity::P> cameras;
	ICam::P						   currentCam;
	ICam::P						   editorCam;
	Property<std::string>		   currentCameraName;
	bool						   isEditorCamMode;


};

}
}
#endif