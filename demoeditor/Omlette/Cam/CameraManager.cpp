#include "DXUT.h"
#include "CameraManager.h"

using namespace Oml::Cam;
using namespace Oml;

void CameraManager::serialize(pugi::xml_node&)
{
}

void CameraManager::deserialize(pugi::xml_node&)
{
}

void CameraManager::setAspect(float aspect)
{
	auto it = cameras.begin();
	while (it != cameras.end())
	{
		it->second->setAspect(aspect);
		it++;
	}
	editorCam->setAspect(aspect);
}

void CameraManager::update(float t)
{
	currentCam = cameras[currentCameraName.getValue(t)]->getICam();
}

ICam::P CameraManager::getCurrentCam()
{
	if(isEditorCamMode)
	{
		return editorCam;
	}
	else
	{
		return currentCam;
	}
}

CameraManager::~CameraManager()
{
	cameras.clear();
}

CameraManager::CameraManager()
{
	isEditorCamMode = true;
}