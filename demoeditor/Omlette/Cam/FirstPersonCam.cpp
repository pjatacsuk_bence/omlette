#include "DXUT.h"
#include "FirstPersonCam.h"


using namespace Oml::Cam;
using namespace Egg::Math;

FirstPersonCam::FirstPersonCam()
{
	position = float3::zero;
	ahead = float3::zUnit;
	right = float3::xUnit;

	
	yaw = 0.0;
	pitch = 0.0;

	fov = 1.57;
	nearPlane = 0.1f;
	farPlane = 1000.0f;
	setAspect(1.33f);

	viewMatrix = float4x4::view(position,ahead,float3::yUnit);
	viewDirMatrix = (float4x4::view(float3::zero,ahead,float3::yUnit) * projMatrix).invert();

	speed = 10.5f;
	lastMousePos = int2::zero;
	mouseDelta = float2::zero;

	wPressed = false;
	aPressed = false;
	sPressed = false;
	dPressed = false;
	qPressed = false;
	ePressed = false;
	shiftPressed = false;

}

const	float3&	FirstPersonCam::getEyePosition()
{
	return position;
}

const	float3&	FirstPersonCam::getAhead()
{
	return ahead;
}

const float4x4& FirstPersonCam::getViewDirMatrix()
{
	return viewDirMatrix;	
}

const float4x4& FirstPersonCam::getViewDirMatrixBack()
{
	return viewDirMatrixBack;
}

const float4x4& FirstPersonCam::getViewMatrix()
{
	return viewMatrix;
}

const float4x4& FirstPersonCam::getProjMatrix()
{
	return projMatrix;
}

void FirstPersonCam::updateView()
{
	viewMatrix = float4x4::view(position,ahead,float3::yUnit);
	viewDirMatrix = (float4x4::view(float3::zero,ahead,float3::yUnit)*projMatrix).invert();
	viewDirMatrixBack = 
		(float4x4::view(float3::zero, ahead, float3::yUnit) 
		* float4x4::rotation(float3(1, 0, 0), 3.14)
		* projMatrix).invert();


	right = float3::yUnit.cross(ahead).normalize();
	yaw = atan2f( ahead.x, ahead.z );
	pitch = -atan2f( ahead.y, ahead.xz.length() );

}
void FirstPersonCam::updateProj()
{
	projMatrix =  float4x4::proj(fov,aspect,nearPlane,farPlane);
}

void FirstPersonCam::animate(double dt)
{
	if(wPressed)
		position += ahead * (shiftPressed?speed*5.0:speed) * dt;
	if(sPressed)
		position -= ahead * (shiftPressed?speed*5.0:speed) * dt;
	if(aPressed)
		position -= right * (shiftPressed?speed*5.0:speed) * dt;
	if(dPressed)
		position += right * (shiftPressed?speed*5.0:speed) * dt;
	if(qPressed)
		position -= float3(0,1,0) * (shiftPressed?speed*5.0:speed) * dt;
	if(ePressed)
		position += float3(0,1,0) * (shiftPressed?speed*5.0:speed) * dt;

	yaw += mouseDelta.x * 0.02f;
	pitch += mouseDelta.y * 0.02f;
	mouseDelta = float2::zero;

	if(pitch > 1.57)
		pitch = 1.57;
	if(pitch < -1.57)
		pitch = -1.57;
	ahead = float3(sin(yaw)*cos(pitch), -sin(pitch), cos(yaw)*cos(pitch) );

	updateView();
}		

void FirstPersonCam::processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if(uMsg == WM_KEYDOWN)
	{
		if(wParam == 'W')
			wPressed = true;
		else if(wParam == 'A')
			aPressed = true;
		else if(wParam == 'S')
			sPressed = true;
		else if(wParam == 'D')
			dPressed = true;
		else if(wParam == 'Q')
			qPressed = true;
		else if(wParam == 'E')
			ePressed = true;
		else if(wParam == VK_SHIFT)
			shiftPressed = true;
	}
	else if(uMsg == WM_KEYUP)
	{
		if(wParam == 'W')
			wPressed = false;
		else if(wParam == 'A')
			aPressed = false;
		else if(wParam == 'S')
			sPressed = false;
		else if(wParam == 'D')
			dPressed = false;
		else if(wParam == 'Q')
			qPressed = false;
		else if(wParam == 'E')
			ePressed = false;
		else if(wParam == VK_SHIFT)
			shiftPressed = false;
	}
	else if(uMsg == WM_KILLFOCUS)
	{
		wPressed = false;
		aPressed = false;
		sPressed = false;
		dPressed = false;
		qPressed = false;
		ePressed = false;
		shiftPressed = false;
	}
	else if(uMsg == WM_MBUTTONDOWN)
	{
		lastMousePos = int2( LOWORD(lParam), HIWORD(lParam));
	}
	else if(uMsg == WM_MBUTTONUP)
	{
		mouseDelta = float2::zero;
	}
	else if(uMsg == WM_MOUSEMOVE && (wParam & MK_MBUTTON))
	{
		int2 mousePos( LOWORD(lParam), HIWORD(lParam));
		mouseDelta = mousePos - lastMousePos;

		lastMousePos = mousePos;
	}
}

void	FirstPersonCam::setAspect(float aspect)
{
	this->aspect = aspect;
	updateProj();
}


FirstPersonCam::P	FirstPersonCam::setView(Egg::Math::float3 position, Egg::Math::float3 ahead)
{
	this->position = position;	
	this->ahead	= ahead;
	updateView();
	return shared_from_this();
}


FirstPersonCam::P	FirstPersonCam::setProj(float fov,float aspect,float front, float back)
{
	this->fov = fov;
	this->aspect = aspect;
	this->nearPlane = front;
	this->farPlane = back;
	updateProj();
	return shared_from_this();
}

FirstPersonCam::P	FirstPersonCam::setSpeed(float speed)
{
	this->speed = speed;
	return shared_from_this();
}