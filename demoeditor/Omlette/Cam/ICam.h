#ifndef INC_ICAM_H
#define INC_ICAM_H

#include "Math\math.h"
#include "boost\shared_ptr.hpp"
namespace Oml
{
namespace Cam
{
class ICam
{
public:
	typedef boost::shared_ptr<ICam> P;

	virtual	const	Egg::Math::float3&		getEyePosition()=0;
	virtual	const	Egg::Math::float3&		getAhead()=0;
	virtual const	Egg::Math::float4x4&	getViewDirMatrix()=0;
	virtual	const	Egg::Math::float4x4&	getViewMatrix()=0;
	virtual	const	Egg::Math::float4x4&	getViewDirMatrixBack()=0;
	
	virtual	const	Egg::Math::float4x4&	getProjMatrix()=0;
	
	virtual			~ICam(){}
	virtual	void	animate(double dt){}
	virtual void	processMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam){}

	virtual void setAspect(float aspect)=0;

};

}
}



#endif