#ifndef INC_MESHIMPORTER_H
#define INC_MESHIMPORTER_H
#include "d3dx11effect.h"
#include "IndexedMesh.h"

struct aiMesh;
namespace Oml
{
namespace Mesh
{
class MeshImporter
{
public:


	static	IndexedMesh::P	fromAiMesh(ID3D11Device* device,aiMesh* assMesh);

};

}


}


#endif