#ifndef INC_BINDER_H
#define INC_BINDER_H
#include "d3dx11effect.h"
#include <vector>
#include "IndexedMesh.h"
#include "Bound.h"
#include "Material.h"
#include "ShadedMesh.h"

namespace Oml
{
namespace Mesh
{
class Binder
{
	class InputConfiguration
	{
	private:
		friend class Mesh::Binder;
		
		const	D3D11_INPUT_ELEMENT_DESC*	elements;
		unsigned int						nElements;
		const	D3DX11_PASS_DESC&			passDesc;

		ID3D11InputLayout*					inputLayout;

		InputConfiguration(const D3DX11_PASS_DESC& passDesc,Geometry::P geometry);

		bool isCompatible(const InputConfiguration& other) const;

		HRESULT createInputLayout(ID3D11Device* device);
	public:
		~InputConfiguration();
	};
	

public:
	typedef boost::shared_ptr<Binder> P;	
	static Binder::P create(ID3D11Device* device)	{return Binder::P(new Binder(device));}

					~Binder();
	Bound::P		bind(const D3DX11_PASS_DESC& passDesc, Geometry::P geometry);
	ShadedMesh::P	bindMaterial(Material::P material, Geometry::P geometry);
	void			replaceMaterial(Material::P material,	ShadedMesh::P& shaded);


private:
	typedef	std::vector<InputConfiguration*>	InputConfigurationList;
	InputConfigurationList	inputConfigurationList;

	ID3D11Device*	device;

	ID3D11InputLayout*	getCompatibleInputLayout(const D3DX11_PASS_DESC& passDesc, Geometry::P geometry) ;
	Binder(ID3D11Device* device);

};

}
}

#endif