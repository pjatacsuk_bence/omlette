#ifndef INC_INDEXEDMESH_H
#define INC_INDEXEDMESH_H
#include "d3dx11effect.h"
#include "Geometry.h"
#include "VertexStream.h"
namespace Oml
{
namespace Mesh
{
	struct IndexBufferDesc
	{
		static const unsigned short defaultData[6];


		DXGI_FORMAT					indexFormat;
		unsigned int				nPrimitives;
		D3D11_PRIMITIVE_TOPOLOGY	topology;	
		unsigned int				nIndices;
		void*						indexData;
		D3D11_USAGE					usage;
		unsigned int				bindFlags;
		unsigned int				cpuAccessFlags;
		unsigned int				miscFlags;

		unsigned int getIndexStride()
		{
			return (indexFormat==DXGI_FORMAT_R16_UINT) ? sizeof(unsigned short):sizeof(unsigned int);
		}
		IndexBufferDesc():
		indexFormat(DXGI_FORMAT_R16_UINT),
		nPrimitives(2),
		topology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST),
		nIndices(0),
		indexData((void*)defaultData),
		usage(D3D11_USAGE_IMMUTABLE),
		bindFlags(D3D11_BIND_INDEX_BUFFER),
		cpuAccessFlags(0),
		miscFlags(0)
		{}
	};

class VertexStream;

class IndexedMesh : public Geometry
{
public:
	typedef boost::shared_ptr<IndexedMesh> P;
	static IndexedMesh::P create(ID3D11Device* device, IndexBufferDesc& desc, VertexStream::P vertexStream)
	{
		VertexStream::A arr(1);
		arr.at(0) = vertexStream;
		return IndexedMesh::P(new IndexedMesh(device,desc,arr));
	}
	static IndexedMesh::P create(ID3D11Device* device, IndexBufferDesc& desc,VertexStream::A& vertexStreams)
	{
		return IndexedMesh::P(new IndexedMesh(device,desc,vertexStreams));
	}
	static IndexedMesh::P createQuad(ID3D11Device* device)
	{
		return IndexedMesh::create(device, IndexBufferDesc(), VertexStream::create(device, VertexStreamDesc()));
	}


									~IndexedMesh();
	void							getElements(const D3D11_INPUT_ELEMENT_DESC*& elements,unsigned int& nElements);

	void							draw(ID3D11DeviceContext* context);
	unsigned int					getVertexBufferCount() {return nVertexBuffers;}
	ID3D11Buffer**					getVertexBuffers()	{return vertexBuffers;}
	ID3D11Buffer*					getPrimaryBuffer()	{return vertexBuffers[0];}
	unsigned int*					getVertexStrides()	{return vertexStrides;}

	unsigned int					getPrimitivesCount(){return nPrimitives;}
	DXGI_FORMAT						getIndexFormat()	{return indexFormat;}
	D3D11_PRIMITIVE_TOPOLOGY		getTopology()		{return topology;}
	ID3D11Buffer*					getIndexBuffer()	{return indexBuffer;}	

	unsigned int					getIndexCount()		{return nIndices;}


private:	
	ID3D11Buffer**					vertexBuffers;
	unsigned int*					vertexStrides;
	unsigned int					nVertexBuffers;

	const D3D11_INPUT_ELEMENT_DESC* elements;
	unsigned int					nElements;

	ID3D11Buffer*					indexBuffer;

	DXGI_FORMAT						indexFormat;
	unsigned int					nPrimitives;
	unsigned int					nIndices;

	D3D11_PRIMITIVE_TOPOLOGY		topology;
	
	IndexedMesh(ID3D11Device* device,IndexBufferDesc& desc, VertexStream::A& vertexStreams);
};
}
	

}


#endif