#include "DXUT.h"
#include <Omlette\Mesh\FlipMesh.h>

using namespace Oml::Mesh;

FlipMesh::~FlipMesh()
{
	meshes.clear();
}

void FlipMesh::draw(RenderParameters renderParameters )
{
	auto it = meshes.find(*(renderParameters.mien));
	it->second->draw(renderParameters.context);
}

void FlipMesh::addMesh(Mien mien, ShadedMesh::P shadedMesh)
{
	auto it = meshes.find(mien);
	if (it != meshes.end())
	{
		meshes.erase(it);
	}
	meshes[mien] = shadedMesh;
}

ShadedMesh::P FlipMesh::getMesh(Mien mien)
{
	auto it = meshes.find(mien);
	if (it == meshes.end())
	{
		return NULL;
	}
	return meshes[mien];
}
