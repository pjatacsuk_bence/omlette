#ifndef INC_UNPICKABLESHADEDMESH_H
#define INC_UNPICKABLESHADEDMESH_H

#include <Omlette\Mesh\ShadedMesh.h>
#include <Omlette\Interfaces\IDrawable.h>
#include <Omlette\Interfaces\IPickable.h>
#include <Omlette\Framework\Utility\ResourceManager.h>

namespace Oml
{
namespace Mesh
{
class UnpickableShadedMesh : public IDrawable,public Unpickable
{
public:
	typedef boost::shared_ptr<UnpickableShadedMesh> P;
	static UnpickableShadedMesh::P create(ShadedMesh::P shadedMesh, IDrawable::DrawableType type)
	{
		return UnpickableShadedMesh::P(new UnpickableShadedMesh(shadedMesh, type));
	}
	void	draw(RenderParameters renderParameters)
	{
			shadedMesh->draw(renderParameters.context);
	}
private:
	UnpickableShadedMesh(ShadedMesh::P shadedMesh, IDrawable::DrawableType type = MESH):
	IDrawable(type),
	Unpickable(this)
	{
		this->shadedMesh = shadedMesh;
	}

	ShadedMesh::P shadedMesh;

};
}
}



#endif