#include "DXUT.h"
#include "ShadedMesh.h"

using namespace Oml::Mesh;

void ShadedMesh::draw(ID3D11DeviceContext* context)
{
	material->apply(context);
	bound->draw(context);
}