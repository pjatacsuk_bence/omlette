#ifndef INC_BOUND_H
#define INC_BOUND_H

#include "Geometry.h"

namespace Oml
{
namespace Mesh
{

class Geometry;

class Bound
{
public:
	typedef	boost::shared_ptr<Bound> P;
	static Bound::P create(Geometry::P geometry, ID3D11InputLayout* inputLayout)
	{
		return Bound::P(new Bound(geometry,inputLayout));
	}

	Geometry::P	getGeometry() {return geometry;}
	ID3D11InputLayout*	getInputLayout(){return inputLayout;}

	~Bound();

	void	draw(ID3D11DeviceContext* context);
private:
	Geometry::P			geometry;
	ID3D11InputLayout*	inputLayout;

	Bound(Geometry::P	geometry,ID3D11InputLayout*	inputLayout);
	
};

}
}


#endif