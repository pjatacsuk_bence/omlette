#ifndef INC_MIEN_H
#define INC_MIEN_H

namespace Oml
{
namespace Mesh
{
class Mien
{
public:
			Mien();

	bool	operator<(const Mien& mien) const;
	bool	operator==(const Mien& mien) const;
	
	inline bool isValid(){ return id != invalidId; }
	
private:
			unsigned int id;
	static	unsigned int nextId;
	static	unsigned int invalidId;
};
}
}


#endif // !INC_MIEN_H
