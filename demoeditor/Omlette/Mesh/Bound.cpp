#include "DXUT.h"
#include "Bound.h" 

using namespace Oml::Mesh;


Bound::Bound(Geometry::P geometry, ID3D11InputLayout* inputLayout):
geometry(geometry),
inputLayout(inputLayout)
{
	this->inputLayout->AddRef();		
}

Bound::~Bound()
{
	inputLayout->Release();	
}

void Bound::draw(ID3D11DeviceContext* context)
{
	context->IASetInputLayout(inputLayout);
	geometry->draw(context);
}