#include "DXUT.h"
#include "Binder.h"

using namespace Oml::Mesh;


Binder::Binder(ID3D11Device* device):
device(device)
{

}

Binder::~Binder()
{
	InputConfigurationList::iterator i = inputConfigurationList.begin();
	InputConfigurationList::iterator e = inputConfigurationList.end();
	
	while(i != e)
	{
		(*i)->inputLayout->Release();
		delete *i;
		i++;
	}

}

Bound::P Binder::bind(D3DX11_PASS_DESC const& passDesc, Geometry::P geometry)
{
	ID3D11InputLayout*	inputLayout = getCompatibleInputLayout(passDesc,geometry);
	return Bound::create(geometry,inputLayout);
}

ShadedMesh::P Binder::bindMaterial(Material::P material, Geometry::P geometry)
{
	D3DX11_PASS_DESC passDesc;
	material->getPassDesc(passDesc);
	return ShadedMesh::create(bind(passDesc,geometry),material);
}

void Binder::replaceMaterial(Material::P material, ShadedMesh::P& shaded)
{
	shaded = bindMaterial(material,shaded->getGeometry());
}

ID3D11InputLayout* Binder::getCompatibleInputLayout(D3DX11_PASS_DESC const& passDesc, Geometry::P geometry) 
{
	InputConfiguration* tentativeInputConfiguration = new InputConfiguration(passDesc,geometry);
	InputConfigurationList::iterator i = inputConfigurationList.begin();
	InputConfigurationList::iterator e = inputConfigurationList.end();

#ifdef DEBUG
	if(tentativeInputConfiguration->createInputLayout(device) != S_OK)
	{
		MessageBox(NULL,L"Faied to create an input layout. Invalid mesh-technique combination",L"Error!",0);
		DXUTShutdown(-1);
	}
	while(i != e)
	{
		if((*i)->isCompatible(*tentativeInputConfiguration))
		{
			tentativeInputConfiguration->inputLayout->Release();
			delete tentativeInputConfiguration;
			return (*i)->inputLayout;
		}
		i++;
	}

#else
	while(i != e)
	{
		if((*i)->isCompatible(*tentativeInputConfiguration))
		{
			return (*i)->inputLayout;
		}
		i++;
	}
	tentativeInputConfiguration->createInputLayout(device);
#endif
	inputConfigurationList.push_back(tentativeInputConfiguration);
	return tentativeInputConfiguration->inputLayout;

}

Binder::InputConfiguration::InputConfiguration(D3DX11_PASS_DESC const& passDesc, Geometry::P geometry):
passDesc(passDesc)
{
	const D3D11_INPUT_ELEMENT_DESC* delements;	
	unsigned int dnElements;

	geometry->getElements(delements,dnElements);

	nElements = dnElements;

	D3D11_INPUT_ELEMENT_DESC* elements = new D3D11_INPUT_ELEMENT_DESC[nElements];
	memcpy(elements,delements,nElements * sizeof(D3D11_INPUT_ELEMENT_DESC));

	for(int i=0;i<nElements;i++)
	{
		char* semanticName = new char[strlen(delements[i].SemanticName)+1];
		strcpy(semanticName,delements[i].SemanticName);
		elements[i].SemanticName = semanticName;
	}
	
	this->elements = elements;

}


Binder::InputConfiguration::~InputConfiguration()
{
	for(int i=0;i<nElements;i++)
	{
		if(elements[i].SemanticName)
		{
			delete elements[i].SemanticName;
		}
	}
	delete[] elements;
}

HRESULT	Binder::InputConfiguration::createInputLayout(ID3D11Device* device)
{
	return device->CreateInputLayout(elements,nElements,passDesc.pIAInputSignature,passDesc.IAInputSignatureSize,&inputLayout);
}

bool Binder::InputConfiguration::isCompatible(const Binder::InputConfiguration& other) const
{

	if(passDesc.IAInputSignatureSize != other.passDesc.IAInputSignatureSize)
	{
		return false;
	}
	if(0 != memcmp((const void*)passDesc.pIAInputSignature,(const void*)other.passDesc.pIAInputSignature,passDesc.IAInputSignatureSize))
	{
		return false;
	}

	for(unsigned int i=0; i<nElements; i++)
	{
		for(unsigned int u=0; u<other.nElements;u++)
		{
			bool found = false;
			if(elements[i].SemanticIndex == other.elements[u].SemanticIndex &&
				(0 == strcmp(elements[i].SemanticName,other.elements[u].SemanticName)))
			{
				found = true;
				if(elements[i].AlignedByteOffset != other.elements[u].AlignedByteOffset)
				{
					return false;
				}
				if(elements[i].Format != other.elements[u].Format)
				{
					return false;
				}
				if(elements[i].InputSlot != other.elements[u].InputSlot)
				{
					return false;
				}
				if(elements[i].InputSlotClass != other.elements[u].InputSlotClass)
				{
					return false;	
				}
				if(elements[i].InstanceDataStepRate != other.elements[u].InstanceDataStepRate)
				{
					return false;
				}
				
			}
			if(!found)
			{
				return false;
			}
		}
	}

	return true;
	
}


