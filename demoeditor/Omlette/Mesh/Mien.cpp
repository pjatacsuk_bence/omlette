#include "DXUT.h"

#include <Omlette\Mesh\Mien.h>

using namespace Oml::Mesh;

unsigned int Mien::nextId = 1;
unsigned int Mien::invalidId = 0;

Mien::Mien()
{
	id = nextId++;
}

bool Mien::operator<(const Mien& mien) const
{
	return id < mien.id;
}

bool Mien::operator==(const Mien& mien) const
{
	return id == mien.id;
}