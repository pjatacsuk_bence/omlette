#ifndef INC_FLIPMESH_H
#define INC_FLIPMESH_H
#include <Omlette\Interfaces\IDrawable.h>
#include <Omlette\Mesh\ShadedMesh.h>
#include <Omlette\Mesh\Mien.h>
#include <map>
namespace Oml
{
namespace Mesh 
{
class FlipMesh	: public IDrawable
{
public:
	typedef boost::shared_ptr<FlipMesh> P;
	static P create() { return P(new FlipMesh()); }

			virtual	~FlipMesh();
	void			draw(RenderParameters renderParameters);
	void			addMesh(Mien mien, ShadedMesh::P shadedMesh);
	ShadedMesh::P	getMesh(Mien mien);


private:
					FlipMesh(){}
	std::map<Mien, ShadedMesh::P> meshes;

};

}
}


#endif // !INC_FLIPMESH_H
