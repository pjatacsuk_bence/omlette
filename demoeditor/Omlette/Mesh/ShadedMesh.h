#ifndef INC_SHADEDMESH_H
#define INC_SHADEDMESH_H

#include <Omlette\Mesh\Bound.h>
#include <Omlette\Mesh\Material.h>
#include <Omlette\Components\Component.h>
namespace Oml
{
namespace Mesh
{
class ShadedMesh
{
public:
	typedef boost::shared_ptr<ShadedMesh> P;
	static ShadedMesh::P create(Bound::P bound, Material::P material)
	{
		return ShadedMesh::P(new ShadedMesh(bound,material));
	}

	Bound::P				getBound(){return bound;}
	Material::P				getMaterial(){return material;}
	Geometry::P				getGeometry() { return bound->getGeometry();}
	ID3D11InputLayout*		getInputLayout(){return bound->getInputLayout();}

	void	draw(ID3D11DeviceContext* context);
private:
	Bound::P bound;
	Material::P material;

	ShadedMesh(Bound::P bound,Material::P material):
	bound(bound),
	material(material)
	{}


};
}
}



#endif