#include <rasterizerStates.fx>
#include <blendStates.fx>
#include <depthStencilStates.fx>
float4x4 modelMatrix;
float4x4 modelMatrixInverse;
float4x4 modelViewProjMatrix;

Texture2D guiTexture;
SamplerState linearSampler
{
	Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Wrap;
    AddressV = Wrap;
	BorderColor = float4(1, 0, 1, 0);
};
struct IaosTrafo
{
    float4 pos	: POSITION;
    float3 normal 	: NORMAL;
    float2 tex 	: TEXCOORD;
};

struct VsosTrafo
{
    float4 pos 		: SV_POSITION;
    float4 worldPos 	: WORLDPOS;
    float3 normal 		: NORMAL;
    float2 tex 		: TEXCOORD;
};

struct IaosQuad
{
	float4  pos: POSITION;
	float2  tex: TEXCOORD0;
};

struct VsosQuad
{
	float4 pos: SV_POSITION;
	float2 tex: TEXCOORD0;
};

float4 color;

VsosQuad vsQuad(IaosQuad input)
{
	VsosQuad output = (VsosQuad)0;
	output.pos = input.pos;
	output.tex = input.tex;

//	hWorldPosMinusEye /= hWorldPosMinusEye.w;
	return output;
}


VsosTrafo vsIdle(IaosTrafo input)
{
	VsosTrafo output = (VsosTrafo)0;
	output.pos = mul(input.pos,
		modelViewProjMatrix);
	output.worldPos = mul(input.pos,
		modelMatrix);
	output.normal = mul(modelMatrixInverse,
		float4(input.normal.xyz, 0.0));
	output.tex = input.tex;
	return output;
}
float4 psQuad(VsosQuad input) : SV_TARGET
{
	return guiTexture.Sample(linearSampler,input.tex);
}
float4 psSolidColor(VsosTrafo input) : SV_Target
{
	return color;
}
float4 psIdle(VsosTrafo input) : SV_Target
{
	return saturate(input.normal).y;
}
float4 psPickerQuad(VsosQuad input) : SV_Target
{
	float4 texColor = guiTexture.Sample(linearSampler,input.tex);
	if(texColor.x == 0.0 && texColor.y == 0.0 && texColor.z == 0.0)
	{
		return 0;
	}
	else
	{
		return color;
	}
}
float4 psPicker(VsosTrafo input) : SV_Target
{
	return color; 
}

technique11 picker
{
	pass picker
	{
		SetVertexShader ( 
			CompileShader( vs_5_0, vsIdle() ) );
		SetGeometryShader ( NULL );
		SetRasterizerState( defaultRasterizer );
		
		SetPixelShader( 
			CompileShader( ps_5_0, psPicker() ) );
		SetDepthStencilState( defaultCompositor, 0 );
		SetBlendState( defaultBlender, 
			float4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xFFFFFFFF  ); 


	}
	pass pickerFullQuad
	{
		SetVertexShader ( 
			CompileShader( vs_5_0, vsQuad() ) );
		SetGeometryShader ( NULL );
		SetRasterizerState( defaultRasterizer );
		
		SetPixelShader( 
			CompileShader( ps_5_0, psPickerQuad() ) );
		SetDepthStencilState( noDepthTestCompositor, 0 );
		SetBlendState( alphaBlender, 
			float4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xFFFFFFFF  ); 
	}
}
technique11 fullQuad 
{
	pass fullQuad
	{
		SetVertexShader ( 
			CompileShader( vs_5_0, vsQuad() ) );

		SetGeometryShader ( NULL );
		SetRasterizerState( defaultRasterizer );
		SetPixelShader( 
			CompileShader( ps_5_0, psQuad() ) );
		SetDepthStencilState( noDepthTestCompositor, 0 );
		SetBlendState( alphaBlender, 
			float4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xFFFFFFFF  );
			
	}
}
technique11 basic
{
	pass SolidColor
	{
		SetVertexShader ( 
			CompileShader( vs_5_0, vsIdle() ) );
		SetGeometryShader ( NULL );
		SetRasterizerState( defaultRasterizer );
		
		SetPixelShader( 
			CompileShader( ps_5_0, psSolidColor() ) );
		SetDepthStencilState( defaultCompositor, 0 );
		SetBlendState( defaultBlender, 
			float4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xFFFFFFFF  ); 
	}
}
technique11 idle
{
	pass idle
	{
		SetVertexShader ( 
			CompileShader( vs_5_0, vsIdle() ) );
		SetGeometryShader ( NULL );
		SetRasterizerState( defaultRasterizer );
		
		SetPixelShader( 
			CompileShader( ps_5_0, psIdle() ) );
		SetDepthStencilState( defaultCompositor, 0 );
		SetBlendState( defaultBlender, 
			float4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xFFFFFFFF  ); 

	}
}
